﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace P2PJeffConnect
{
    public enum P2PConnectionType
    {
        Established_NatTraversing,
        Established_LocalDirect,
    }
    public enum P2PConnectionState
    {
        Connected,
        NotResponding
    }
    public enum P2PConnectionMode
    {
        Messenger,
        Synchronizator
    }

    public class P2PMessageEventArgs : EventArgs
    {
        public P2PTinyMessage Message { get; }
        internal P2PMessageEventArgs(P2PTinyMessage m)
        { Message = m; }
    }
    public delegate void P2PMessageEventHandler(P2PConnection sender, P2PMessageEventArgs e);
    public delegate void P2PConnectionEventHandler(P2PConnection sender, EventArgs e);

    /// <summary>
    ///  Класс обеспечивающий поддержку и обслуживание постоянного фактического соединения между пирами.
    ///  Реализует протокол P2PJeffConnect (передача сообщений или обработка и подтверждение входящих сообщений)
    /// </summary>
    public class P2PConnection
    {
        private const int WAIT_MSG_CONFR_SLEEP = 0;
        private const int WAIT_MSG_CONFR_RESEND_INTERVAL = 200;
        private const int WAIT_MSG_CONFR_RESEND_MAX_COUNT = 10;

        private const int KEEP_ALIVE_SEND_INTERVAL = 1500;
        private const int KEEP_ALIVE_RECEIVE_WORRY = 6000;
        private const int KEEP_ALIVE_RECEIVE_ERROR = 10000;

        public event P2PMessageEventHandler MessageReceived;
        public event P2PMessageEventHandler MessageNotDelivered;
        public event Action NotRespondingWarning;
        public event P2PConnectionEventHandler Disconnected;

        private List<P2PTinyMessage> MsgWaitingConfirmation { get; } = new List<P2PTinyMessage>();
        private Dictionary<int, DateTime> MsgReceivedJournal { get; } = new Dictionary<int, DateTime>();

        internal P2PConnection(P2PThisPeer @this, IPEndPoint remoteActiveEP,
            P2PConnectionType state, string peerName)
        {
            Type = state;
            ActiveRemoteAddress = remoteActiveEP;
            ThisPeer = @this;
            RemotePeerName = peerName;
            SupportedModes.Add(mode);
        }

        public P2PConnectionType Type { get; private set; }

        public P2PConnectionState State { get; private set; }

        private P2PConnectionMode mode = P2PConnectionMode.Messenger;
        public List<P2PConnectionMode> SupportedModes { get; } = new List<P2PConnectionMode>();
        public event P2PConnectionEventHandler ModeChanged;
        public P2PConnectionMode Mode
        {
            get { return mode; }
        }
        public async void ChangeMode(P2PConnectionMode newMode)
        {
            _p2ptrace.TraceMsg(this, $"Change connection mode to {newMode}");
            // если режим не меняется по факту то ничего не делаем
            if (mode == newMode) return;
            // создаем запрос на изменение режима
            var req = P2PTinyMessage.CreateChangeModeRequest(newMode);
            // Отправляем запрос и ждем подтверждения о принятии
            // Если пир поддерживает изменение режима:
            // ===) то он продублирует запрос на изменение,
            // ===) и только потом отправит подтверждение о принятии.
            // Если нет, то он отправит только подтверждение о принятиию
            bool conf = await SendMessageAsync(req);
            if (!conf) throw new InvalidOperationException("No answer to change mode request.");
            // итого, если после прихода подтверждения 
            // наш режим изменился на желаемый, то все ок,
            // а если нет, то ошибка!
            if (mode != newMode) throw new NotSupportedException("Remote peer does not support change mode now.");
            if (ModeChanged != null) ModeChanged(this, new EventArgs());
        }

        public string RemotePeerName { get; }
        public P2PThisPeer ThisPeer { get; }
        public IPEndPoint ActiveRemoteAddress { get; }
        public bool IsConnected => Type == P2PConnectionType.Established_LocalDirect ||
            Type == P2PConnectionType.Established_NatTraversing;

        internal void _MessageProc(P2PTinyMessage msg)
        {
            switch (msg.Type)
            {
                case MessageType.KeepAlivePulse:
                    _lastKeptAlive = DateTime.Now;
                    Debug.WriteLine($"keep alive {msg.ID}");
                    break;
                case MessageType.ReceiveConfirm:
                    if (MsgWaitingConfirmation.Exists(m => m.ID == msg.ID))
                    {
                        MsgWaitingConfirmation.RemoveAt
                            (MsgWaitingConfirmation.FindIndex(m => m.ID == msg.ID));
                    }
                    break;
                case MessageType.ChangeMode:
                    // Если id есть в списке, значит это ответный запрос, пересылка не нужна
                    if (MsgWaitingConfirmation.Exists(m => m.ID == msg.ID))
                    {
                        var m = (P2PConnectionMode)msg.GetContentsAsInt();
                        if (SupportedModes.Contains(m))
                        {
                            mode = m;
                        }
                        else {/*Уведомление о запросе изменения на неподдерживаемый режим*/}
                    }
                    else // Иначе это самостоятельный запрос, нужна пересылка
                    {
                        msg.UpdateTime();
                        var m = (P2PConnectionMode)msg.GetContentsAsInt();
                        _p2ptrace.TraceMsg(this, $"Change connection mode request: {m}");
                        if (SupportedModes.Contains(m))
                        {
                            mode = m;
                            var tsk = SendMessageAsync(msg);
                        }
                        else {/*Уведомление о запросе изменения на неподдерживаемый режим*/}
                    }
                    goto default;
                default:
                    // Обрабатываем только если сообщение еще не было принято
                    if (!MsgReceivedJournal.ContainsKey(msg.ID))
                    {
                        if (MessageReceived != null)
                            MessageReceived(this, new P2PMessageEventArgs(msg));
                        MsgReceivedJournal[msg.ID] = DateTime.Now;
                    }
                    else
                        _p2ptrace.TraceMsg(this, $"Msg duplicate {msg.Type}-{msg.ID}");
                    // Но подтверждение отправляем всегда
                    var task = SendMessageAsync(P2PTinyMessage.CreateReceiveConfirm(msg));
                    break;
            }
        }

        private DateTime _lastKeptAlive;
        private Thread _working_thread;

        internal void StartServeConnection()
        {
            _working_thread = new Thread(() =>
            {
                try
                {
                    while (true)
                        switch (Mode)
                        {
                            case P2PConnectionMode.Messenger:
                                _MessengerIteration();
                                break;
                            case P2PConnectionMode.Synchronizator:
                                break;
                        }
                }
                catch (ThreadAbortException)
                { }
            });
            _working_thread.IsBackground = true;
            _working_thread.Start();
            _lastKeptAlive = DateTime.Now;
        }
        private void _MessengerIteration()
        {
            List<int> expired = new List<int>();
            foreach (var m in MsgReceivedJournal)
                if ((m.Value - DateTime.Now) >
                TimeSpan.FromMilliseconds(WAIT_MSG_CONFR_RESEND_INTERVAL
                * WAIT_MSG_CONFR_RESEND_MAX_COUNT))
                    expired.Add(m.Key);
            foreach (var exp_msg in expired)
                MsgReceivedJournal.Remove(exp_msg);
            expired.Clear();
            // Сообщенька keep-alive
            var msg = P2PTinyMessage.CreateKeepAlivePulse();
            // Шлем сообщение
            var taskSend = SendMessageAsync(msg);
            // Проверяем последнее состояние keep-alive пульсов пира.
            if (DateTime.Now - _lastKeptAlive > TimeSpan.FromMilliseconds(KEEP_ALIVE_RECEIVE_ERROR))
            { var taskAbort = SafeDisconnectAsync(false); }// Очень давно отвечал, ошибка
            if (DateTime.Now - _lastKeptAlive > TimeSpan.FromMilliseconds(KEEP_ALIVE_RECEIVE_WORRY))
            {
                _p2ptrace.TraceMsg(this, $"Not responding warning!");
                if (NotRespondingWarning != null) NotRespondingWarning();
            }// Давно отвечал, 
             // Спим
            Thread.Sleep(KEEP_ALIVE_SEND_INTERVAL);
        }

        public async Task<bool> SendMessageAsync(P2PTinyMessage message)
        {
            if (message == null)
                throw new ArgumentNullException(nameof(message));
            return await Task.Factory.StartNew(() =>
            {
                return SendMessage(message);
            });

        }
        private bool SendMessage(P2PTinyMessage message)
        {
            // Посылаем сообщение пиру.
            P2PRecHub.Socket.SendTo(message.ToBinary(), ActiveRemoteAddress);
            // Если нужно подтверждение,
            if (message.RequireConfirmation)
            {
                // то запихиваем сообщеньку в список ожидающих подтверждения.
                MsgWaitingConfirmation.Add(message);
                // Цикл пересылки (какое то кол-во раз).
                for (int i = 0; i < WAIT_MSG_CONFR_RESEND_MAX_COUNT; i++)
                {
                    // Ждем таймаут пересылки.
                    DateTime resendTime = DateTime.Now.AddMilliseconds(WAIT_MSG_CONFR_RESEND_INTERVAL);
                    while (DateTime.Now < resendTime)
                    {
                        Thread.Sleep(WAIT_MSG_CONFR_SLEEP);
                        // после каждого микротаймаута ожидания проверяем было ли получено подтверждение.
                        if (!MsgWaitingConfirmation.Contains(message))
                            return true; // Если получено подтверждение, то все ок, возвращаемся.
                        else // Если нет, ждем еще
                        {
                        }
                    }
                    // Пересылка.
                    _p2ptrace.TraceMsg(this, $"No msg confirm. Resend {i}");
                    P2PRecHub.Socket.SendTo(message.ToBinary(), ActiveRemoteAddress);
                }
                if (MessageNotDelivered != null)
                    MessageNotDelivered(this, new P2PMessageEventArgs(message));
                return false;
            }
            else
                return true;
        }

        public async Task SafeDisconnectAsync(bool friendly = true)
        {
            var task = SendMessageAsync(P2PTinyMessage.CreateDisconnectNotification());
            if (friendly)
                await task;
            if (Disconnected != null)
                Disconnected(this, new EventArgs());
            _working_thread.Abort();
        }

        public override string ToString()
        {
            return $"[{ThisPeer.PeerName}<=>{RemotePeerName}]";
        }
    }
}