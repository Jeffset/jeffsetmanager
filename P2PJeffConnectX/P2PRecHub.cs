﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace P2PJeffConnect
{
    internal class P2PRecHub
    {
        private const int LOCAL_PORT = 22828;
        private const int REC_BUFFER_SIZE = 1024 * 16;
        private const int PUNCH_PULSE_INTERVAL = 200;
        private const int PUNCH_TOTAL_TIMEOUT = 10000;

        static P2PRecHub()
        {
            Socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            Socket.Bind(new IPEndPoint(IPAddress.Any, LOCAL_PORT));
        }

        byte[] receiveBuffer { get; } = new byte[REC_BUFFER_SIZE];
        private Thread _hubThread = null;
        public bool IsActive => (_hubThread != null && _hubThread.ThreadState == System.Threading.ThreadState.Running);
        private IPEndPoint ThisPrivateAddress { get; set; }
        private IPEndPoint ThisPublicAddress { get; set; }
        public P2PThisPeer ThisPeer { get; }
        public static Socket Socket { get; } 
        internal Dictionary<string, P2PConnection> ActiveConnections { get; } = new Dictionary<string, P2PConnection>();
        private IPEndPoint _newConnectionRemoteEp = null;
        //
        public P2PRecHub(P2PThisPeer @this)
        {
            ThisPeer = @this;
        }
        public void StartHub()
        {
            if (_hubThread != null)
                return;
            _hubThread = new Thread(_WorkingListenHubThread);
            _hubThread.IsBackground = true;
            _hubThread.Start();
        }
        public void StopHub()
        {
            List<string> str = ActiveConnections.Keys.ToList();
            foreach (var peer in str)
            {
                var task = ActiveConnections[peer].SafeDisconnectAsync();
            }
            ActiveConnections.Clear();
            _hubThread.Abort();
            _hubThread = null;
        }
        //
        public async Task<P2PConnection> ConnectTo(string peerName, IPEndPoint remotePrivate, IPEndPoint remotePublic)
        {
            // Если подключение уже есть
            if (ActiveConnections.ContainsKey(peerName))
                return ActiveConnections[peerName];
            // Запускаем и ждем задачу-панчер. Если ответ придет, то его словит главный хаб и засетит нам адресок.
            await Task.Factory.StartNew(() =>
            {
                DateTime tillTime = DateTime.Now.AddMilliseconds(PUNCH_TOTAL_TIMEOUT);
                var punchMessage = P2PTinyMessage.CreatePunchPulse();
                byte[] sendBuffer = punchMessage.ToBinary();
                do
                {
                    Socket.SendTo(sendBuffer, remotePrivate);
                    Socket.SendTo(sendBuffer, remotePublic);
                    Thread.Sleep(PUNCH_PULSE_INTERVAL);
                } while ((_newConnectionRemoteEp == null && DateTime.Now < tillTime));
                tillTime = DateTime.Now.AddSeconds(1);
                do
                {
                    Socket.SendTo(sendBuffer, remotePrivate);
                    Socket.SendTo(sendBuffer, remotePublic);
                    Thread.Sleep(PUNCH_PULSE_INTERVAL);
                } while (DateTime.Now < tillTime);
            });
            // Если адрес ответа ненулевой, значит ответ пришел, все ОК, инициализируем объект подключения
            if (_newConnectionRemoteEp != null)
            {
                // Как подключилося: в локальном адресом пространстве или через нат-траверсинг
                P2PConnectionType state = _newConnectionRemoteEp.ToString() == remotePrivate.ToString() ?
                    P2PConnectionType.Established_LocalDirect : P2PConnectionType.Established_NatTraversing;
                // Создаем объект подключения.
                var Connection = new P2PConnection(ThisPeer, _newConnectionRemoteEp, state, peerName);
                Connection.Disconnected += (s, e) => { ActiveConnections.Remove(s.RemotePeerName); };
                // Сохраняем его в активные подключения.
                ActiveConnections[peerName] = Connection;
                // Обнуляем адресок.
                _newConnectionRemoteEp = null;
                // Активируем подключение
                Connection.StartServeConnection();
                // Возвращаем подключение.
                return Connection;
            }
            // Иначе не подключилося (истек таймаут), вздыхаем, нолик возвращаем
            else
                return null;
        }
        public async Task<Tuple<IPEndPoint, IPEndPoint>> Resolve()
        {
            ThisPrivateAddress = new IPEndPoint(Dns.GetHostEntry(Dns.GetHostName()).AddressList.First
                (ip => ip.AddressFamily == AddressFamily.InterNetwork),
                LOCAL_PORT);
            ThisPublicAddress = await Task<IPEndPoint>.Factory.StartNew(
                () => STUN.STUNResolver.RetrieveExternalContext(Socket));
            return Tuple.Create(ThisPrivateAddress, ThisPublicAddress);
        }
        private bool _IsConnectedIP(string addr)
        {
            foreach (var conn in ActiveConnections.Values)
                if (conn.ActiveRemoteAddress.ToString() == addr)
                    return true;
            return false;
        }
        private void _IsConnectedIP(string addr, out P2PConnection connection)
        {
            foreach (var conn in ActiveConnections.Values)
                if (conn.ActiveRemoteAddress.ToString() == addr)
                {
                    connection = conn;
                    return;
                }
            connection = null;
        }
        /// <summary>
        /// Главный поток слушающего хаба.
        /// </summary>
        private void _WorkingListenHubThread()
        {
            try
            {
                while (true)
                {
                    try
                    {
                        EndPoint ep = new IPEndPoint(IPAddress.Any, 0);
                        Socket.ReceiveFrom(receiveBuffer, ref ep);
                        string whereItCameFrom = (ep as IPEndPoint).ToString();

                        // Пробуем распознать сообщение протокола.
                        P2PTinyMessage ans = P2PTinyMessage.ParseResponse(receiveBuffer);

                        // Это сообщение не распознано?
                        if (ans == null)
                            // Проверяем наличие уже открытого подключения к этому отправителю,
                            if (!_IsConnectedIP(whereItCameFrom))
                            {
                                Debug.WriteLine($"HUB -> Rubbish from {whereItCameFrom}");
                                continue;
                            }// => Тогда это просто мусор (мб от STUN). Игнорим этот мусор.
                            else // => Иначе это поврежденное сообщение, вздыхаем, игнорим, ждем пересылки.
                            {
                                Debug.WriteLine($"HUB -> Corrupted msg from {whereItCameFrom}");
                                continue;
                            }

                        // Если это пробивной импульс
                        if (ans.Type == MessageType.PunchPulse)
                        {
                            Debug.WriteLine($"HUB -> Punch pulse from {whereItCameFrom}");
                            // Запоздалый пробивной импульс 
                            if (_IsConnectedIP(whereItCameFrom))
                            {
                                Debug.WriteLine($"HUB -> Lated punch pulse from {whereItCameFrom}");
                                continue;
                            } // Всё уже пробито, игнорим.
                            // Поздравляем, у нас установлено новое подключение!
                            // Задаем адрес с которого пришел импульс, поток-панчер увидит, обрадуется)
                            _newConnectionRemoteEp = ep as IPEndPoint;
                        }
                        // Или в конце концов это просто рабочее сообщение или keep_alive запрос.
                        else
                        {
                            P2PConnection targetTunnel;
                            _IsConnectedIP(whereItCameFrom, out targetTunnel);
                            if (targetTunnel == null)
                            { continue; }
                            // Направим корректное сообщение на обработку в соответствующее подключение.
                            // (Подтверждение об успешном приеме подключенчик отправит сам если требуется)
                            targetTunnel._MessageProc(ans);
                        }
                    }
                    catch (SocketException e) when (e.SocketErrorCode == SocketError.TimedOut)
                    { }
                }
            }
            catch (ThreadAbortException)
            { }
        }
    }
}
