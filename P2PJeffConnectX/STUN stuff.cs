﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace P2PJeffConnect.STUN
{
    enum STUN_Msg_Type
    {
        BIND_REQUEST = 0x0001,
        BIND_RESPONSE = 0x0101,
        BIND_ERROR_RESPONSE = 0x0111
    }

    enum STUN_Msg_Attrib_Type
    {
        MAPPED_ADDRESS = 0x0001,
        RESPONSE_ADDRESS = 0x0002,
        CHANGE_REQUEST = 0x0003,
        SOURCE_ADDRESS = 0x0004,
        CHANGED_ADDRESS = 0x0005,
        USERNAME = 0x0006,
        PASSWORD = 0x0007,
        MESSAGE_INTEGRITY = 0x0008,
        ERROR_CODE = 0x0009,
        UNKNOWN_ATTRIBUTES = 0x000a,
        REFLECTED_FROM = 0x000b,
        //
        SOFTWARE = 0x8022,
        ALTERNATE_SERVER = 0x8023,
        FINGERPRINT = 0x8028
    }
    struct STUN_Msg_Attrib
    {
        public ushort TYPE;
        public ushort LENGTH;
        public byte[] VALUE;

        public override string ToString()
        {
            return Type.ToString();
        }

        public STUN_Msg_Attrib_Type Type
        { get { return (STUN_Msg_Attrib_Type)TYPE; } }

        public struct STUN_ADDRESS
        {
            byte IGNORED;
            public byte FAMILY;
            public ushort PORT;
            public byte[] ADDRESS;

            public IPEndPoint EndPoint
            {
                get
                {
                    string tmp = string.Format("{0}.{1}.{2}.{3}", ADDRESS[0], ADDRESS[1], ADDRESS[2], ADDRESS[3]);
                    return new IPEndPoint(IPAddress.Parse(tmp), PORT);
                }
            }

            public static STUN_ADDRESS ParseMappedAddress(byte[] data)
            {
                STUN_ADDRESS addr;
                MemoryStream ms = new MemoryStream(data);
                BinaryReader br = new BinaryReader(ms);
                addr.IGNORED = br.ReadByte();
                addr.FAMILY = br.ReadByte();
                addr.PORT = STUN_Msg.ConvertEndian(br.ReadUInt16());
                addr.ADDRESS = br.ReadBytes(4);
                return addr;
            }
        }
    }
    struct STUN_Msg
    {
        const uint MAGIC_COOKIE = 0x2112A442;

        public ushort STUN_2zero_Message_Type;// : 16;
        public ushort STUN_Message_Length;// : 16;
        public uint STUN_MagicCookie;// : 32;
        public uint STUN_Transaction_Id_H;// : 32;
        public uint STUN_Transaction_Id_M;// : 32;
        public uint STUN_Transaction_Id_L;// : 32;

        public STUN_Msg_Type Type
        { get { return (STUN_Msg_Type)STUN_2zero_Message_Type; } }

        public List<STUN_Msg_Attrib> attribs;

        public static ushort ConvertEndian(ushort val)
        {
            IEnumerable<byte> d = BitConverter.GetBytes(val);
            d = d.Reverse();
            return BitConverter.ToUInt16(d.ToArray(), 0);
        }
        public static uint ConvertEndian(uint val)
        {
            IEnumerable<byte> d = BitConverter.GetBytes(val);
            d = d.Reverse();
            return BitConverter.ToUInt32(d.ToArray(), 0);
        }

        public static byte[] GenBindRequest()
        {
            IEnumerable<byte> data = new byte[0];
            STUN_Msg msg;
            msg.STUN_2zero_Message_Type = (ushort)STUN_Msg_Type.BIND_REQUEST;
            //msg.STUN_MagicCookie = MAGIC_COOKIE;
            msg.STUN_Message_Length = 0;
            Random r = new Random(Environment.TickCount);
            msg.STUN_Transaction_Id_L = (uint)r.Next();
            msg.STUN_MagicCookie = MAGIC_COOKIE;//(uint)r.Next();
            msg.STUN_Transaction_Id_M = (uint)r.Next();
            msg.STUN_Transaction_Id_H = (uint)r.Next();

            data = data.Concat(BitConverter.GetBytes(msg.STUN_2zero_Message_Type).Reverse()).ToArray();
            data = data.Concat(BitConverter.GetBytes(msg.STUN_Message_Length).Reverse());
            data = data.Concat(BitConverter.GetBytes(msg.STUN_MagicCookie).Reverse());
            data = data.Concat(BitConverter.GetBytes(msg.STUN_Transaction_Id_L).Reverse());
            data = data.Concat(BitConverter.GetBytes(msg.STUN_Transaction_Id_M).Reverse());
            data = data.Concat(BitConverter.GetBytes(msg.STUN_Transaction_Id_H).Reverse());
            return data.ToArray();
        }

        public static STUN_Msg ParseBin(byte[] resp)
        {
            try
            {
                STUN_Msg msg;
                int index = 0;
                msg.STUN_2zero_Message_Type = ConvertEndian(BitConverter.ToUInt16(resp, index)); index += 2;
                msg.STUN_Message_Length = ConvertEndian(BitConverter.ToUInt16(resp, index)); index += 2;
                msg.STUN_MagicCookie = ConvertEndian(BitConverter.ToUInt32(resp, index)); index += 4;
                msg.STUN_Transaction_Id_L = ConvertEndian(BitConverter.ToUInt32(resp, index)); index += 4;
                msg.STUN_Transaction_Id_M = ConvertEndian(BitConverter.ToUInt32(resp, index)); index += 4;
                msg.STUN_Transaction_Id_H = ConvertEndian(BitConverter.ToUInt32(resp, index)); index += 4;
                msg.attribs = new List<STUN_Msg_Attrib>();
                int end = index + msg.STUN_Message_Length;
                while (index < end)
                {
                    STUN_Msg_Attrib attrib;
                    attrib.TYPE = ConvertEndian(BitConverter.ToUInt16(resp, index)); index += 2;
                    attrib.LENGTH = ConvertEndian(BitConverter.ToUInt16(resp, index)); index += 2;
                    attrib.VALUE = new byte[attrib.LENGTH];
                    Array.Copy(resp, index, attrib.VALUE, 0, attrib.LENGTH);
                    index += attrib.LENGTH;
                    msg.attribs.Add(attrib);
                }
                return msg;
            }
            catch (IndexOutOfRangeException)
            {
                throw;
            }
        }

        public bool CheckTransactionID(STUN_Msg msg)
        {
            return
                STUN_Transaction_Id_L == msg.STUN_Transaction_Id_L &&
                STUN_Transaction_Id_M == msg.STUN_Transaction_Id_M &&
                STUN_Transaction_Id_H == msg.STUN_Transaction_Id_H;
        }
    }
}
