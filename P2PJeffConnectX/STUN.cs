﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace P2PJeffConnect.STUN
{
    internal enum NATPortMappingBehavior
    {
        Cone, Symmetric
    }

    internal static class STUNResolver
    {
        private static readonly string[] _stunServers = { "stun.ekiga.net", "stun.counterpath.net", "stun.gmx.net" };
        private static readonly int STUNPort = 3478;
        private static IPEndPoint _RetrieveExternalAddress(Socket socket, string stunServer)
        {
            IPEndPoint stunEndPoint = new IPEndPoint(Dns.GetHostAddresses(stunServer)[0], STUNPort);
            //socket.ReceiveTimeout = Timeout;
            //socket.SendTimeout = Timeout;

            byte[] message = STUN_Msg.GenBindRequest();
            STUN_Msg req = STUN_Msg.ParseBin(message);

            //DateTime time = DateTime.Now;
            //while (time.AddMilliseconds(Timeout) > DateTime.Now)
            //{
            //    try
            //    {
            socket.SendTo(message, stunEndPoint);
            new Action(() =>
            {
                const int INTERVAL = 1000, COUNT = 3;
                for (int i = 0; i < COUNT; i++)
                {
                    Thread.Sleep(INTERVAL);
                    socket.SendTo(message, stunEndPoint);
                }
            }).BeginInvoke(null, null);
            //if (socket.Poll(100, SelectMode.SelectRead))
            //{
            byte[] receiveBuffer = new byte[512];
            while (true)
            {
                EndPoint ep = new IPEndPoint(IPAddress.Any, 0);
                socket.ReceiveFrom(receiveBuffer, ref ep);
                if ((ep as IPEndPoint).Address.Equals(stunEndPoint.Address))
                    break; 
            }
            var msg = STUN_Msg.ParseBin(receiveBuffer);
            //if (!msg.CheckTransactionID(req))
            //continue;
            foreach (var attrib in msg.attribs)
            {
                if (attrib.Type == STUN_Msg_Attrib_Type.MAPPED_ADDRESS)
                {
                    var mappedAddress = STUN_Msg_Attrib.STUN_ADDRESS.ParseMappedAddress(attrib.VALUE);
                    return mappedAddress.EndPoint;
                }
            }
            socket.Close();
            //            break;
            //        }
            //    }
            //    catch
            //    {
            //        return null;
            //    }
            //}
            return null;
        }
        internal static IPEndPoint RetrieveExternalContext(Socket socket)
        {
            IPEndPoint ext1 = _RetrieveExternalAddress(socket, _stunServers[0]);
            Thread.Sleep(100);

            //IPEndPoint ext3 = _RetrieveExternalAddress(socket, _stunServers[2]);

            //if (ext1.Address.Equals(ext2.Address))
            //    throw new WebException("Addresses are not equal!", WebExceptionStatus.UnknownError);

            //if (ext1.Port != ext2.Port)
            //    Debug.WriteLine("YOU HAVE SYMMETRIC NAT FUCKING SHIT!");
            //else
            return ext1;
        }
    }
}
