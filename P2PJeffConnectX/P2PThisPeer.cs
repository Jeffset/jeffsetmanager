﻿#define _TRACE_

using CloudOperations;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace P2PJeffConnect
{
    public static class _p2ptrace
    {
        public static event Action<string> Print;

        [Conditional("_TRACE_")]
        public static void TraceMsg(object s, string msg)
        {
            string msg_full = $"{s}->{msg}";
            Print?.Invoke(msg_full);
            Debug.WriteLine(msg_full);
        }
    }

    [Serializable]
    internal class ConnectionRequestData
    {
        public IPEndPoint PublicAddress { get; }
        public IPEndPoint PrivateAddress { get; }
        public string PeerName { get; }
        public ConnectionRequestData(IPEndPoint @public, IPEndPoint @private, string peerName)
        {
            PublicAddress = @public;
            PrivateAddress = @private;
            PeerName = peerName;
        }
        public ConnectionRequestData(byte[] data)
        {
            using (MemoryStream ms = new MemoryStream(data))
            using (BinaryReader br = new BinaryReader(ms))
            {
                IPAddress tmp; int port;
                tmp = new IPAddress(br.ReadBytes(4));
                port = br.ReadUInt16();
                PublicAddress = new IPEndPoint(tmp, port);
                tmp = new IPAddress(br.ReadBytes(4));
                port = br.ReadUInt16();
                PrivateAddress = new IPEndPoint(tmp, port);
                PeerName = br.ReadString();
            }
        }
        public byte[] GetBinary()
        {
            byte[] data = null;
            using (MemoryStream ms = new MemoryStream())
            using (BinaryWriter bw = new BinaryWriter(ms))
            {
                bw.Write(PublicAddress.Address.GetAddressBytes());
                bw.Write((ushort)PublicAddress.Port);
                bw.Write(PrivateAddress.Address.GetAddressBytes());
                bw.Write((ushort)PrivateAddress.Port);
                bw.Write(PeerName);
                data = ms.GetBuffer();
            }
            return data;
        }
    }

    public enum ThisPeerState
    {
        Offline = 0, Online = 8, Busy = 1
    }

    public class P2PThisPeer : IDisposable
    {
        private bool _cancel = false;
        private bool OpCanceled() { if (_cancel) _p2ptrace.TraceMsg(this, "--Operation canceled-- -> INT"); return _cancel; }
        private ThisPeerState _state = ThisPeerState.Offline;
        private Task _requestWatcherTask;
        private string _GenIncConnectStrFor(string peerName) => $"{peerName}/+Connection";
        private P2PRecHub Hub { get; }
        //
        public bool RequestWatcherRunning { get; private set; }
        public int PassiveWatchInterval { get; set; } = 1000;
        public int ActiveWatchInterval { get; set; } = 250;
        public ThisPeerState State
        {
            get { return _state; }
            set
            {
                if (_state == ThisPeerState.Offline && value == ThisPeerState.Busy)
                    throw new InvalidOperationException("Can not perform p2p operations while offline.");
                _state = value;
                _cancel = false;
            }
        }

        public string PeerName { get; }
        public CloudClient Tracker { get; private set; } = new CloudClient(workingPath: "/SystemStorage/@P2PJeffConnect/");
        public bool Online { get; private set; }
        //
        public event Action IncomingConnection;
        public event P2PConnectionEventHandler ConnectionEstablished;

        public bool IsConnectedTo(string peerName) => Hub.ActiveConnections.ContainsKey(peerName);
        public P2PConnection GetConnection(string peerName) => IsConnectedTo(peerName) ? Hub.ActiveConnections[peerName] : null;
        //
        private void ___StopIfCanceled()
        { if (OpCanceled()) throw new OperationCanceledException(); }
        //
        public void CancelActiveOperation()
        {
            if (State != ThisPeerState.Busy)
                return;
            _cancel = true;
        }
        public async Task<IEnumerable<string>> RefreshOnlinePeersAsync()
        {
            var peers = await Tracker.GetInfoAsync("");
            _p2ptrace.TraceMsg(this, "USER-> Online Peers refreshed.");
            return from peerDir in peers select peerDir.Name;
        }
        public async Task<P2PConnection> ConnectToAsync(string remotePeer)
        {
            if (IsConnectedTo(remotePeer))
                return GetConnection(remotePeer);
            try
            {
                State = ThisPeerState.Busy;
                // Если этого пира нет в онлайне
                if (!await Tracker.CheckExistsAsync(remotePeer))
                    throw new InvalidOperationException($"Peer with name {remotePeer} does not exist or not online at the moment.");
                _p2ptrace.TraceMsg(this, $"{remotePeer} is online -> OK.");
                // Если к пиру уже кто-то подключается
                if (await Tracker.CheckExistsAsync(_GenIncConnectStrFor(remotePeer)))
                    throw new InvalidOperationException($"Peer with name {remotePeer} is busy with another connection. Wait.");
                _p2ptrace.TraceMsg(this, $"{remotePeer} is free -> OK.");
                ___StopIfCanceled();
                // Пока суд да дело, начнем остановку RequestWatcher'а
                var stopTask = RunRequestWatcherService(false);
                // Само подключение, создаем новое подключение и открываем дырку
                //----------------------------P2PConnection hole = await P2PConnection.CreateAndResolve(this);
                var ipInfo = await Hub.Resolve();
                IPEndPoint thisPrivateAddress = ipInfo.Item1, thisPublicAddress = ipInfo.Item2;
                //----------------------------hole.Report += (msg) => _p2ptrace.TraceMsg(this, msg);
                _p2ptrace.TraceMsg(this, $"Hole from {PeerName} is opened. -> OK\r\n Address mapping: ({thisPrivateAddress} => {thisPublicAddress}).");
                ___StopIfCanceled();
                // ЗАПРОС: Публикуем свой полученный внешний адрес и имя пира в подключении к пиру
                var publicInfo = new ConnectionRequestData(thisPublicAddress, thisPrivateAddress, PeerName);
                await Tracker.UploadSerializedInfoAsync(publicInfo.GetBinary(), _GenIncConnectStrFor(remotePeer));
                _p2ptrace.TraceMsg(this, $"This public EP was published -> OK.");
                ___StopIfCanceled();
                // К этому времени RequestWatcher должен быть остановлен
                await stopTask;
                _p2ptrace.TraceMsg(this, $"RequestWatcher was stopped - > OK.");
                ___StopIfCanceled();
                // Теперь спокойно ждем пока пир ответит нам своим адресом
                while (!await Tracker.CheckExistsAsync(_GenIncConnectStrFor(PeerName)))
                    Thread.Sleep(ActiveWatchInterval);
                _p2ptrace.TraceMsg(this, $"Answer request from {remotePeer} was detected -> OK.");
                ___StopIfCanceled();
                // ОТВЕТ: Скачиваем внешний адрес пира
                var ansInfo = new ConnectionRequestData(await Tracker.DownloadSerializedInfoAsync<byte[]>(_GenIncConnectStrFor(PeerName)));
                _p2ptrace.TraceMsg(this, $"info: Remote address mapping: {ansInfo.PrivateAddress}=>{ansInfo.PublicAddress}");
                ___StopIfCanceled();
                // Чистим запросы
                var task1 = Tracker.DeleteAsync(_GenIncConnectStrFor(PeerName));
                var task2 = Tracker.DeleteAsync(_GenIncConnectStrFor(remotePeer));
                // И, в ответ на его попытки пробить нашу дырку, пробиваем его.
                _p2ptrace.TraceMsg(this, $"[{thisPrivateAddress}] <--dbl punching--> [{ansInfo.PrivateAddress}]");
                _p2ptrace.TraceMsg(this, $"[{thisPublicAddress}] <--dbl punching--> [{ansInfo.PublicAddress}]");
                var tunnel = await Hub.ConnectTo(ansInfo.PeerName, ansInfo.PrivateAddress, ansInfo.PublicAddress);
                if (tunnel == null)
                    throw new TimeoutException("Hole punching failed.");
                else
                {
                    _OnConnectionEstablished(tunnel);
                    return tunnel;
                }
            }
            catch (OperationCanceledException)
            { return null; }
            finally
            {
                var task1 = Tracker.DeleteAsync(_GenIncConnectStrFor(PeerName));
                var task2 = Tracker.DeleteAsync(_GenIncConnectStrFor(remotePeer));
                var task3 = RunRequestWatcherService(true);
                State = ThisPeerState.Online;
            }
        }

        private void _OnConnectionEstablished(P2PConnection tunnel)
        {
            if (tunnel == null)
            { _p2ptrace.TraceMsg(this, $"Failed to connect to {tunnel.RemotePeerName} -> FAIL."); }
            else
            {
                _p2ptrace.TraceMsg(this, $"Connection [{PeerName} <-> {tunnel.RemotePeerName}] established");
                _p2ptrace.TraceMsg(this, string.Format("Using {0}",
                    tunnel.Type == P2PConnectionType.Established_LocalDirect ?
                    "direct connection via private Ip's (local network)" : "NAT traversal via public Ip's (different networks)"));
                if (ConnectionEstablished != null)
                    ConnectionEstablished(tunnel, new EventArgs());
            }
        }

        private async Task _OnIncomingConnection()
        {
            try
            {
                State = ThisPeerState.Busy;
                // Получаем имя запрашивающего пира и его адрес 
                var reqInfo = new ConnectionRequestData(await Tracker.DownloadSerializedInfoAsync<byte[]>(_GenIncConnectStrFor(PeerName)));
                _p2ptrace.TraceMsg(this, $"Incoming connection from {reqInfo.PeerName} -> OK.");
                _p2ptrace.TraceMsg(this, $"Remote address mapping: {reqInfo.PrivateAddress}=>{reqInfo.PublicAddress} - > OK.");
                // Открываем ответную дырку
                var ipInfo = await Hub.Resolve();
                var ThisPrivateAddress = ipInfo.Item1; var ThisPublicAddress = ipInfo.Item2;
                //-------------------------------P2PConnection hole = await P2PConnection.CreateAndResolve(this);
                //-------------------------------hole.Report += (msg) => _p2ptrace.TraceMsg(this, msg);
                _p2ptrace.TraceMsg(this, $"Hole from {PeerName} is opened, public EP is {ThisPublicAddress} -> OK.");
                // Формируем и посылаем ответный запрос-подтверждение подключения
                var ansInfo = new ConnectionRequestData(ThisPublicAddress, ThisPrivateAddress, PeerName);
                await Tracker.UploadSerializedInfoAsync(ansInfo.GetBinary(), _GenIncConnectStrFor(reqInfo.PeerName));
                _p2ptrace.TraceMsg(this, $"This public EP was published -> OK.");
                // После посылки ответа начинаем пробивать его дырку, он получит ответ и пробьет нашу.
                _p2ptrace.TraceMsg(this, $"[{ThisPrivateAddress}] <--punching--> [{reqInfo.PrivateAddress}]");
                _p2ptrace.TraceMsg(this, $"[{ThisPublicAddress}] <--punching--> [{reqInfo.PublicAddress}]");
                var tunnel = await Hub.ConnectTo(reqInfo.PeerName, reqInfo.PrivateAddress, reqInfo.PublicAddress);
                _OnConnectionEstablished(tunnel);
            }
            finally
            {
                State = ThisPeerState.Online;
            }
        }

        public async Task RunRequestWatcherService(bool run)
        {
            // Если значение не меняется, то ничего не делаем.
            if (RequestWatcherRunning == run)
                return;
            if (run)
            {
                RequestWatcherRunning = true;
                // Запустить задачу RequestWatcher
                _requestWatcherTask = Task.Factory.StartNew(_RequestWatcher);
            }
            else
            {
                RequestWatcherRunning = false;
                // Подождать пока RequestWatcher завершит работу
                await _requestWatcherTask;
                _requestWatcherTask = null;
            }
        }
        public async Task SetOnlineAsync(bool online)
        {
            if (State != ThisPeerState.Offline && State != ThisPeerState.Online)
                throw new InvalidOperationException("Can not change online state when peer is busy");
            // Если значение не меняется, то ничего не делаем.
            if (Online == online)
                return;
            if (online)// Теперь мы онлайн ->
            {
                // Создать на трекере папку "{UserName}".
                await Tracker.CreateCatalogAsync(PeerName);
                Online = true;
                State = ThisPeerState.Online;
                Hub.StartHub();
            }
            else // Теперь мы оффлайн ->
            {
                // Останавливаем службу приема входящих соединений.
                var task = RunRequestWatcherService(false);
                // Удалить на трекере папку "{UserName}".
                await Tracker.DeleteAsync(PeerName);
                await task;
                Online = false;
                State = ThisPeerState.Offline;
                Hub.StopHub();
            }
        }
        //
        private async void _RequestWatcher()
        {
            _p2ptrace.TraceMsg(this, "ReqWatcher: started");
            // Работаем пока онлайн
            while (RequestWatcherRunning)
            {
                // Есть ли входящее соединение
                bool incomConnect = Tracker.CheckExists(_GenIncConnectStrFor(PeerName));
                // Обработать входящее подключение
                if (incomConnect)
                {
                    _p2ptrace.TraceMsg(this, "ReqWatcher: Incoming connection detected! -> CONNECT");
                    IncomingConnection?.Invoke();
                    await _OnIncomingConnection();
                    Thread.Sleep(PassiveWatchInterval * 3);
                }
                //else
                //_p2ptrace.TraceMsg(this, "SERVICE-> No incoming connections.");
                // Ждем интервал проверки запроса
                Thread.Sleep(ActiveWatchInterval);
            }
            _p2ptrace.TraceMsg(this, "ReqWatcher: Finished");
        }
        //
        public P2PThisPeer(string name)
        {
            Hub = new P2PRecHub(this);
            PeerName = name;
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                }
                var task2 = SetOnlineAsync(false);
                disposedValue = true;
                Hub.StopHub();
            }
        }

        ~P2PThisPeer()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion

        public override string ToString()
        {
            return $"This peer {PeerName}";
        }
    }
}
