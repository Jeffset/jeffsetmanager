﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P2PJeffConnect
{
    public enum MessageType
    {
        // Punch
        PunchPulse = -2,

        // Service
        DisconnectNotif = -3,
        KeepAlivePulse = 1,
        ReceiveConfirm = 2,
        ChangeMode = 3,

        //UserService
        UserService = 33,

        // Content type
        TextWin1251 = 10,
        TextUnicode = 11,
        Bytepack = 12,
    }

    /// <summary>
    /// Класс, коий предоставляет методы для построения и расшифровки сообщений протокола P2PJeffConnect
    /// </summary>
    public class P2PTinyMessage
    {
        private static Random _idGen = new Random(Environment.TickCount);
        private const ushort MAGIC_CONSTANT = 0x228;
        //
        private byte[] _content = null;
        //
        // Открытые интерфейсные свойства для исследования сообщения
        //
        public int ID { get; private set; }
        public MessageType Type { get; private set; }
        public ushort ContentLength => (ushort)(_content == null ? 0 : _content.Length);
        public DateTime TimeSent { get; private set; }
        public bool RequireConfirmation =>
            Type != MessageType.KeepAlivePulse &&
            Type != MessageType.DisconnectNotif &&
            Type != MessageType.ReceiveConfirm;
        //
        //
        public string GetContentAsString()
        {
            switch (Type)
            {
                case MessageType.TextWin1251:
                    return Encoding.GetEncoding(1251).GetString(_content);
                case MessageType.TextUnicode:
                    return Encoding.Unicode.GetString(_content);
                default:
                    return Encoding.Default.GetString(_content);
            }
        }
        public Int32 GetContentsAsInt()
        {
            return BitConverter.ToInt32(_content, 0);
        }
        public byte[] GetBytepack() => _content;
        //
        private P2PTinyMessage()
        {

        }

        /*------- P2PJeffConnect ---------
        ----------------------------------
        __ parameter ____ size ___________

        MagicConstant  |  16 bits
        MessageId      |  32 bits
        TimeSent       |  64 bits
        MessageType    |  8 bits
        ContentLength  |  16 bits
        [Content data  |  <ContentLength>]
        MagicConstant  |  16 bits
        */
        public byte[] ToBinary()
        {
            using (var ms = new MemoryStream())
            {
                using (var stream = new BinaryWriter(ms))
                {
                    stream.Write((UInt16)MAGIC_CONSTANT);
                    stream.Write((Int32)ID);
                    stream.Write((Int64)TimeSent.Ticks);
                    stream.Write((SByte)Type);
                    stream.Write((Int16)ContentLength);
                    if (ContentLength != 0)
                        stream.Write(_content);
                    stream.Write((UInt16)MAGIC_CONSTANT);
                }
                return ms.GetBuffer();
            }
        }

        internal void UpdateTime()
        {
            TimeSent = DateTime.Now;
        }

        //
        public static P2PTinyMessage ParseResponse(byte[] buffer)
        {
            var msg = new P2PTinyMessage();
            using (var ms = new MemoryStream(buffer))
            {
                using (var stream = new BinaryReader(ms))
                {
                    if (stream.ReadUInt16() != MAGIC_CONSTANT)
                        return null;
                    msg.ID = stream.ReadInt32();
                    msg.TimeSent = new DateTime(stream.ReadInt64());
                    msg.Type = (MessageType)stream.ReadSByte();
                    Int16 length = stream.ReadInt16();
                    msg._content = stream.ReadBytes(length);
                    if (stream.ReadUInt16() != MAGIC_CONSTANT)
                        return null;
                }
            }
            return msg;
        }
        //
        internal static P2PTinyMessage CreateKeepAlivePulse()
        {
            var msg = new P2PTinyMessage();
            msg.Type = MessageType.KeepAlivePulse;
            msg.TimeSent = DateTime.Now;
            msg.ID = _idGen.Next();
            return msg;
        }

        internal static P2PTinyMessage CreateDisconnectNotification()
        {
            var msg = new P2PTinyMessage();
            msg.Type = MessageType.DisconnectNotif;
            msg.TimeSent = DateTime.Now;
            msg.ID = _idGen.Next();
            return msg;
        }

        public static P2PTinyMessage CreateTextMessage(string text, MessageType encoding = MessageType.TextWin1251)
        {
            if (encoding != MessageType.TextUnicode &&
                encoding != MessageType.TextWin1251)
                throw new ArgumentException("Invalid encoding!", nameof(encoding));
            var msg = new P2PTinyMessage();
            msg.Type = encoding;
            msg._content = (encoding == MessageType.TextWin1251 ? Encoding.GetEncoding(1251)
                : Encoding.Unicode).GetBytes(text);
            msg.TimeSent = DateTime.Now;
            msg.ID = _idGen.Next();
            return msg;
        }

        public static P2PTinyMessage CreateControlMessage(int comandId)
        {
            var msg = new P2PTinyMessage();
            msg.Type = MessageType.UserService;
            msg._content = BitConverter.GetBytes(comandId);
            msg.TimeSent = DateTime.Now;
            msg.ID = _idGen.Next();
            return msg;
        }

        internal static P2PTinyMessage CreateChangeModeRequest(P2PConnectionMode mode)
        {
            var msg = new P2PTinyMessage();
            msg.Type = MessageType.ChangeMode;
            msg._content = BitConverter.GetBytes((Int32)mode);
            msg.TimeSent = DateTime.Now;
            msg.ID = _idGen.Next();
            return msg;
        }

        public static P2PTinyMessage CreateBytepack(byte[] data)
        {
            var msg = new P2PTinyMessage();
            msg.Type = MessageType.Bytepack;
            msg._content = data;
            msg.TimeSent = DateTime.Now;
            msg.ID = _idGen.Next();
            return msg;
        }

        internal static P2PTinyMessage CreateReceiveConfirm(P2PTinyMessage msg)
        {
            var confr = new P2PTinyMessage();
            confr.Type = MessageType.ReceiveConfirm;
            msg.TimeSent = DateTime.Now;
            confr.ID = msg.ID;
            return confr;
        }

        internal static P2PTinyMessage CreatePunchPulse()
        {
            var msg = new P2PTinyMessage();
            msg.Type = MessageType.PunchPulse;
            msg.TimeSent = DateTime.Now;
            msg.ID = _idGen.Next();
            return msg;
        }

    }
}
