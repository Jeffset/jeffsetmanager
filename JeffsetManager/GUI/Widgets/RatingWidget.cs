﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Imaging;

namespace JeffsetManager {
    public partial class RatingWidget : UserControl {

        [Category("Behavior")]
        public bool ReadOnly { get; set; }

        private float rating;

        [Category("Appearance")]
        public float Rating {
            get { return rating; }
            set {
                rating = (value < 0.0f) ? 0.0f : (value > 5.0f ? 5.0f : value);
                rating = (float)Math.Round(rating * 2.0) / 2.0f;
                lblRating.Text = rating.ToString();
                panelStars.Invalidate();
            }
        }

        public RatingWidget() {
            InitializeComponent();
            Rating = 4.5f;
        }
        private SolidBrush sb;

        protected override void OnBackColorChanged(EventArgs e) {
            base.OnBackColorChanged(e);
            sb = new SolidBrush(BackColor);
        }

        private void panelStars_Paint(object sender, PaintEventArgs e) {
            var cont = sender as Control;
            var rect = new Rectangle(Point.Empty, cont.Size);
            var offset = (int)(cont.Width * Rating / 5.0f);
            rect.X += offset; rect.Width -= offset;
            e.Graphics.FillRectangle(sb, rect);
        }

        private void panelStars_MouseClick(object sender, MouseEventArgs e) {
            if(ReadOnly) return;
            var cont = sender as Control;
            Rating = (float)e.X / cont.Width * 5.0f;
        }
    }
}
