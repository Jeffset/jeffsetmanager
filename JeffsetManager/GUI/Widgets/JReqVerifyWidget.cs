﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ManagerCore;

namespace JeffsetManager {
    public partial class JReqVerifyWidget : UserControl {
        #region Graphics
        private const int offset = 4;
        private const float width = 2.0f;
        private readonly Color color = Color.Gray;
        private Pen border;
        #endregion

        private JDRequest req;

        //[Category("Appearance")]
        //public bool ReadOnly {
        //    get { return !cb_UserLicense.Enabled; }
        //    set { cb_UserLicense.Enabled = chb_Tester.Enabled = !value; }
        //}

        [Category("Behavior")]
        public bool Fake { get; set; }

        public JDRequest Request {
            get { return req; }
            set {
                req = value;
                if(value == null) return;
                string m = string.Format("[{0}]\n\r{1} ({2}) {3}",
                    Fake ? "[Управление]" : value.When.ToString(), value.Who.Credentials.UserName, value.Who.Name, value.Who.License);
                lbl_Msg.Text = m;
                var lic = (JeffsetLicence)value.Data["LICENSE"];
                cb_UserLicense.Text = lic.UserLicense.ToString();
                chb_Tester.Checked = lic.TesterLicense;
                switch(req.Data["ACTION"] as string)
                {
                    case "регистрация":
                        btnVerify.Text = "Verify";
                        btnVerify.ForeColor = Color.MediumSpringGreen;
                        btnDecline.Text = "Delete";
                        btnDecline.ForeColor = Color.Gray;
                        BackColor = Color.FromArgb(40, 40, 30);
                        break;
                    case "повышение":
                        btnVerify.Text = "Extend";
                        btnVerify.ForeColor = Color.Cyan;
                        btnDecline.Text = "Deny";
                        btnDecline.ForeColor = Color.Orange;
                        BackColor = Color.FromArgb(10, 30, 50);
                        break;
                    default:
                        break;
                }

            }
        }

        public JReqVerifyWidget() {
            InitializeComponent();
            border = new Pen(color);
            border.DashStyle = System.Drawing.Drawing2D.DashStyle.Dash;
            border.Width = width;
            cb_UserLicense.Text = "Extended";
        }

        bool AvaLoaded = false;
        protected override void OnPaint(PaintEventArgs e) {
            if(req != null && !AvaLoaded)
            {
                var backTask = JM.Core.DownloadAvatarAsync(req.Who, (s) =>
                {
                    if(s == null) return;
                    pbAva.Image = Image.FromStream(s);
                });
                AvaLoaded = true;
            }
            base.OnPaint(e);
            e.Graphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            e.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            // Draw dashed border;
            Rectangle r = ClientRectangle;
            r.Inflate(-offset, -offset);
            e.Graphics.DrawRectangle(border, r);
        }

        private async void btnVerify_Click(object sender, EventArgs e) {
            if(Arh_Dialog.BUSY_CONTROL(sender)) return;
            //<--------BUSY--CONTROL--------->
            req.Who.License = new JeffsetLicence() {
                TesterLicense = chb_Tester.Checked,
                UserLicense = (JeffsetUserType)Enum.Parse(typeof(JeffsetUserType), cb_UserLicense.Text)
            };
            var oldTxt = btnVerify.Text;
            btnVerify.Text = "Wait..";
            await JM.Core.ChangeAccountSettingsAsync(req.Who);
            if(!Fake)
            {
                await JM.Core.RequestHasBeenHandledAsync(req, JDevRequestState.Accepted);
                Parent.Controls.Remove(this);
            }
            else
            {
                btnVerify.Text = oldTxt;
                string m = string.Format("[{0}]\n\r{1} ({2}) {3}",
                    "[Управление]", req.Who.Credentials.UserName, req.Who.Name, req.Who.License);
                lbl_Msg.Text = m;
            }
        }

        private async void btnDecline_Click(object sender, EventArgs e) {
            if(Arh_Dialog.BUSY_CONTROL(sender)) return;
            //<--------BUSY--CONTROL--------->
            if(req.Data["ACTION"] as string == "регистрация")
            {
                var a = Arh_MessageBox.ShowMB(string.Format("Удалить запрошенный аккаунт для {0}?", req.Who.Name),
                     "Отклонить запрос", Arh_MessageBox.AMBType.Warning);
                if(a == DialogResult.Cancel) return;
                await JM.Core.WipeAccountAsync(req.Who);
            }
            btnDecline.Text = "Wait..";
            if(!Fake)
                await JM.Core.RequestHasBeenHandledAsync(req, JDevRequestState.Declined);
            Parent.Controls.Remove(this);
        }
    }
}
