﻿namespace JeffsetManager
{
    partial class JeffsetSoftwareWidget
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_Title = new System.Windows.Forms.Label();
            this.chooseInstallPath = new System.Windows.Forms.FolderBrowserDialog();
            this.pb_Icon = new System.Windows.Forms.PictureBox();
            this.lbl_Version = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Icon)).BeginInit();
            this.SuspendLayout();
            // 
            // lbl_Title
            // 
            this.lbl_Title.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lbl_Title.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lbl_Title.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_Title.Font = new System.Drawing.Font("Courier New", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Title.ForeColor = System.Drawing.Color.White;
            this.lbl_Title.Location = new System.Drawing.Point(80, 0);
            this.lbl_Title.Name = "lbl_Title";
            this.lbl_Title.Size = new System.Drawing.Size(194, 54);
            this.lbl_Title.TabIndex = 3;
            this.lbl_Title.Text = "Program Name";
            this.lbl_Title.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl_Title.Click += new System.EventHandler(this.Activate);
            this.lbl_Title.Paint += new System.Windows.Forms.PaintEventHandler(this.pb_Banner_Paint);
            // 
            // pb_Icon
            // 
            this.pb_Icon.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pb_Icon.Dock = System.Windows.Forms.DockStyle.Left;
            this.pb_Icon.Image = global::JeffsetManager.Properties.Resources.defaultAva;
            this.pb_Icon.Location = new System.Drawing.Point(0, 0);
            this.pb_Icon.Name = "pb_Icon";
            this.pb_Icon.Size = new System.Drawing.Size(80, 78);
            this.pb_Icon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pb_Icon.TabIndex = 3;
            this.pb_Icon.TabStop = false;
            // 
            // lbl_Version
            // 
            this.lbl_Version.AutoSize = true;
            this.lbl_Version.BackColor = System.Drawing.Color.Transparent;
            this.lbl_Version.ForeColor = System.Drawing.Color.MediumSpringGreen;
            this.lbl_Version.Location = new System.Drawing.Point(86, 54);
            this.lbl_Version.Name = "lbl_Version";
            this.lbl_Version.Size = new System.Drawing.Size(95, 13);
            this.lbl_Version.TabIndex = 4;
            this.lbl_Version.Text = "Версия: 2.0.3.155";
            // 
            // JeffsetSoftwareWidget
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(34)))), ((int)(((byte)(34)))));
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.lbl_Version);
            this.Controls.Add(this.lbl_Title);
            this.Controls.Add(this.pb_Icon);
            this.MinimumSize = new System.Drawing.Size(0, 80);
            this.Name = "JeffsetSoftwareWidget";
            this.Size = new System.Drawing.Size(274, 78);
            ((System.ComponentModel.ISupportInitialize)(this.pb_Icon)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lbl_Title;
        private System.Windows.Forms.FolderBrowserDialog chooseInstallPath;
        private System.Windows.Forms.PictureBox pb_Icon;
        private System.Windows.Forms.Label lbl_Version;
    }
}
