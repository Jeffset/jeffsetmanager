﻿namespace JeffsetManager {
    partial class RatingWidget {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if(disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.panelStars = new System.Windows.Forms.Panel();
            this.lblRating = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // panelStars
            // 
            this.panelStars.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelStars.BackgroundImage = global::JeffsetManager.Properties.Resources.ratings;
            this.panelStars.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panelStars.Location = new System.Drawing.Point(3, 3);
            this.panelStars.Name = "panelStars";
            this.panelStars.Size = new System.Drawing.Size(241, 49);
            this.panelStars.TabIndex = 0;
            this.panelStars.Paint += new System.Windows.Forms.PaintEventHandler(this.panelStars_Paint);
            this.panelStars.MouseClick += new System.Windows.Forms.MouseEventHandler(this.panelStars_MouseClick);
            // 
            // lblRating
            // 
            this.lblRating.BackColor = System.Drawing.Color.DarkSlateGray;
            this.lblRating.Dock = System.Windows.Forms.DockStyle.Right;
            this.lblRating.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblRating.ForeColor = System.Drawing.Color.Orange;
            this.lblRating.Location = new System.Drawing.Point(246, 0);
            this.lblRating.Name = "lblRating";
            this.lblRating.Size = new System.Drawing.Size(44, 55);
            this.lblRating.TabIndex = 1;
            this.lblRating.Text = "3,5";
            this.lblRating.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // RatingWidget
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.Black;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.lblRating);
            this.Controls.Add(this.panelStars);
            this.DoubleBuffered = true;
            this.Name = "RatingWidget";
            this.Size = new System.Drawing.Size(290, 55);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelStars;
        private System.Windows.Forms.Label lblRating;
    }
}
