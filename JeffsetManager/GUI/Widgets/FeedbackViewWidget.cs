﻿using System;
using System.Drawing;
using System.Windows.Forms;
using ManagerCore;

namespace JeffsetManager
{
    public partial class FeedbackViewWidget : UserControl {
        #region Graphics
        private const int offset = 4;
        private const float width = 2.0f;
        private readonly Color color = Color.DarkOrange;
        private Pen border;
        #endregion

        private JDRequest req;
        public JDRequest Request {
            get { return req; }
            set {
                req = value;
                if(value == null) return;
                lblUserName.Text = $"Отзыв от {value.Who.Name} ({value.Who.Credentials.UserName})";
                btnExpand.Text = value.Data["TITLE"] as string;
                lblText.Text = value.Data["TEXT"] as string;
                ratingWidget.Rating = (float)value.Data["RATING"];
                lblWhen.Text = value.When.ToShortDateString() + ' ' + value.When.ToLongTimeString();
            }
        }

        public FeedbackViewWidget() {
            InitializeComponent();
            border = new Pen(color);
            border.DashStyle = System.Drawing.Drawing2D.DashStyle.Dash;
            border.Width = width;
            #region FormText
            textForm = new Form();
            textForm.FormBorderStyle = FormBorderStyle.FixedSingle;
            textForm.BackColor = Color.FromArgb(40, 40, 40);
            textForm.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            textForm.AutoSize = true;
            textForm.StartPosition = FormStartPosition.Manual;
            textForm.ShowInTaskbar = false;
            textForm.ForeColor = Color.MediumSpringGreen;
            textForm.Font = new Font("Courier New", 15);
            textForm.ControlBox = false;
            textForm.AutoScroll = true;
            lblText = new Label();
            lblText.AutoSize = true;
            lblText.Click += (s, e) => textForm.Close();
            textForm.Controls.Add(lblText); 
            #endregion
        }
        bool AvaLoaded = false;
        protected override void OnPaint(PaintEventArgs e) {
            if(req != null && !AvaLoaded) {
                var backTask = JM.Core.DownloadAvatarAsync(req.Who, (s) => {
                    if(s == null) return;
                    pbAva.Image = Image.FromStream(s);
                });
                AvaLoaded = true;
            }
            base.OnPaint(e);
            e.Graphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            e.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            // Draw dashed border;
            Rectangle r = ClientRectangle;
            r.Inflate(-offset, -offset);
            e.Graphics.DrawRectangle(border, r);
        }

        Form textForm;
        Label lblText;
        private void btnExpand_Click(object sender, EventArgs e) {
            btnExpand.ForeColor = Color.MediumSpringGreen;
            lblText.MaximumSize = new Size(Width, 15000);
            textForm.MaximumSize = new Size(Width, 500);
            textForm.Location = PointToScreen(Point.Empty);
            textForm.ShowDialog(btnExpand);
            btnExpand.ForeColor = ForeColor;
        }

        private async void btnDelete_Click(object sender, EventArgs e) {
            if (Arh_Dialog.BUSY_CONTROL(sender)) return;
            //<--------BUSY--CONTROL--------->
            await JM.Core.RemoveRequestAsync(req);
            Parent.Controls.Remove(this);
        }
    }
}
