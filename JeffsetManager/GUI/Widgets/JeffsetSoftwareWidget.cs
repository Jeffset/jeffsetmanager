﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ManagerCore;
using System.IO;
using System.Diagnostics;
using CloudOperations;
using System.Net;

namespace JeffsetManager {
    public partial class JeffsetSoftwareWidget : UserControl {
        public FlowLayoutPanel ActionPanel { get; set; }

        private static JeffsetSoftwareWidget _selected;
        public static JeffsetSoftwareWidget SelectedWidget {
            get { return _selected; }
            set {
                if(_selected == value)
                    return;
                if(_selected != null)
                    _selected.SelectW(false);
                _selected = value;
                if(_selected != null)
                    _selected.SelectW(true);
            }
        }

        #region Graphics
        private const int offset = 4;
        private const float width = 2.0f;
        private Pen border;
        //
        #endregion

        private JeffsetSoftware _software;

        public JeffsetSoftware RepresentedSoftware {
            get { return _software; }
            set {
                _software = value;
                if(value == null) return;
                lbl_Title.Text = value.ProductFriendlyName;
                if(value.Installation != null) {
                    if(value.CheckUpdates()) {
                        if(value.CheckCriticalUpdates()) {
                            lbl_Version.Text = string.Format("v. {0} [new {1}]",
                                value.Installation.InstalledVersion, value.CurrentVersion);
                            lbl_Version.ForeColor = Color.Red;
                        }
                        else {
                            lbl_Version.Text = string.Format("v. {0} [new {1}]",
                                value.Installation.InstalledVersion, value.CurrentVersion);
                            lbl_Version.ForeColor = Color.Orange;
                        }
                    }
                    else {
                        lbl_Version.Text = string.Format("v. {0} [установлено]",
                            value.CurrentVersion);
                        lbl_Version.ForeColor = Color.Cyan;
                    }
                }
                else {
                    lbl_Version.Text = string.Format("v. {0}",
                            value.CurrentVersion);
                    lbl_Version.ForeColor = Color.MediumSpringGreen;
                }
            }
        }

        public JeffsetSoftwareWidget() {
            InitializeComponent();
            border = new Pen(Color.Black);
            border.DashStyle = System.Drawing.Drawing2D.DashStyle.Dash;
            border.Width = width;
        }

        bool imageUpdated = false;
        protected override void OnPaint(PaintEventArgs e) {
            if(!imageUpdated && RepresentedSoftware != null)
                RepresentedSoftware.AdditInfo.IconBitmap.UpdateImage((s) => {
                    Image img = Image.FromStream(s);
                    pb_Icon.Image = img;
                });
            imageUpdated = true;
            base.OnPaint(e);
            e.Graphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            e.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            // Draw dashed border;
            Rectangle r = ClientRectangle;
            r.Inflate(-offset, -offset);
            e.Graphics.DrawRectangle(border, r);
        }

        private void pb_Banner_Paint(object sender, PaintEventArgs e) {
            base.OnPaint(e);
            e.Graphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            e.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            // Draw dashed border;
            Rectangle r = (sender as Control).ClientRectangle;
            r.Inflate(0, -offset);
            r.X -= offset;
            e.Graphics.DrawRectangle(border, r);
            // Draw banner
            //r.Width -= flowLayoutPanel1.Width;
            //r.Height -= flowLayoutPanel1.Height;
            //r.Inflate(-offset, -offset);
        }

        private void CreateMenuButton(string text, Color color, EventHandler click) {
            const int D = 30, H = 30;
            Button btnUpdate = new Button();
            btnUpdate.FlatStyle = FlatStyle.Flat;
            btnUpdate.Height = H;
            btnUpdate.Width = ActionPanel.Width - D;
            btnUpdate.Text = text;
            btnUpdate.ForeColor = color;
            btnUpdate.Click += click;
            ActionPanel.Controls.Add(btnUpdate);
        }

        #region MenuActions
        private void _RefreshContents() {
            SelectW(true);
            RepresentedSoftware = RepresentedSoftware;
        }
        private async void UpdateAction(object s, EventArgs e) {
            if(Arh_Dialog.BUSY_CONTROL(s)) return;
            //<--------BUSY--CONTROL--------->
            var sender = s as Control;
            var last = sender.Text; sender.Text = "Обновление...";
            try {
                await JM.Core.UpdateInstalledProductAsync(RepresentedSoftware);
            } catch(InvalidOperationException) {
                Arh_MessageBox.ShowMB($"Ошибка получения/обновления приложения {RepresentedSoftware.ProductFriendlyName}!",
                "Arahon: Ошибка обновления", type: Arh_MessageBox.AMBType.Error);
                sender.Text = last;
                return;
            }
            sender.Text = last;
            Arh_MessageBox.ShowMB(string.Format("Приложение {0} было успешно установлено.",
                RepresentedSoftware.ProductFriendlyName), type: Arh_MessageBox.AMBType.Success);
            _RefreshContents();
        }
        private void LaunchAction(object s, EventArgs e) {
            if(Arh_Dialog.BUSY_CONTROL(s)) return;
            //<--------BUSY--CONTROL--------->
            string launchPath = RepresentedSoftware.Installation.InstallationCatalog;
            var files = Directory.GetFiles(launchPath);
            string exe;
            try {
                exe = files.First(f => Path.GetExtension(f) == ".exe" || Path.GetExtension(f) == ".sln");
            } catch(InvalidOperationException) {
                Arh_MessageBox.ShowMB("Невозможно найти исполняемый файл программы или файл решения VS!",
                    type: Arh_MessageBox.AMBType.Error);
                return;
            }
            var tmp = Environment.CurrentDirectory;
            Environment.CurrentDirectory = Path.GetDirectoryName(exe);
            Process.Start(exe);
            Environment.CurrentDirectory = tmp;
        }
        private void UninstallAction(object s, EventArgs e) {
            if(Arh_Dialog.BUSY_CONTROL(s)) return;
            //<--------BUSY--CONTROL--------->
            var res = Arh_MessageBox.ShowMB(
                string.Format("Удалить \"{0}\" с вашего комьютера?\n\r(ОК/(X)Escape)", RepresentedSoftware.ProductFriendlyName),
                "Прекращение публикации", Arh_MessageBox.AMBType.Info);
            if(res != DialogResult.OK)
                return;
            try {
                JM.Core.UninstallProduct(RepresentedSoftware);
            } catch(DirectoryNotFoundException) {
                Arh_MessageBox.ShowMB(
                    "Каталог программы не найден!\r\nВозможно, программа уже была удалена либо перемещена.",
                    type: Arh_MessageBox.AMBType.Warning);
            } catch(IOException ex) {
                Arh_MessageBox.ShowMB(
                    $"Ошибка удаления программы [{ex.Message}]!\r\n",
                    type: Arh_MessageBox.AMBType.Error);
                return;
            } catch(Exception ex) {
                Arh_MessageBox.ShowMB(
                    $"Неожиданная ошибка удаления программы [{ex.Message}]!\r\n",
                    type: Arh_MessageBox.AMBType.Error);
                return;
            }
            _RefreshContents();
        }
        private async void InstallAction(object s, EventArgs e) {
            if(Arh_Dialog.BUSY_CONTROL(s)) return;
            //<--------BUSY--CONTROL--------->
            if(chooseInstallPath.ShowDialog() != DialogResult.OK) return;
            var sender = s as Control;
            var last = sender.Text; sender.Text = "Установка...";
            try {
                await JM.Core.InstallNewProductAsync(RepresentedSoftware, chooseInstallPath.SelectedPath);
            }
            #region EXCEPT
            catch(CloudNotFoundException) {
                Arh_MessageBox.ShowMB($"Ошибка скачивания приложения {RepresentedSoftware.ProductFriendlyName}. Возможно пакет отсутствует в облаке!",
                "Arahon: Ошибка скачивания", type: Arh_MessageBox.AMBType.Error);
                sender.Text = last;
                return;
            } catch(IOException) {
                Arh_MessageBox.ShowMB($"Ошибка установки приложения {RepresentedSoftware.ProductFriendlyName}!",
                "Arahon: Ошибка установки", type: Arh_MessageBox.AMBType.Error);
                sender.Text = last;
                return;
            } catch(InvalidOperationException) {
                Arh_MessageBox.ShowMB($"Неожиданная ошибка получения/установки приложения {RepresentedSoftware.ProductFriendlyName}!",
                "Arahon: Ошибка установки", type: Arh_MessageBox.AMBType.Error);
                sender.Text = last;
                return;
            }
            #endregion
            Arh_MessageBox.ShowMB(string.Format("Приложение {0} было успешно установлено.",
                RepresentedSoftware.ProductFriendlyName), type: Arh_MessageBox.AMBType.Success);
            sender.Text = last;
            _RefreshContents();
        }
        private async void DeleteProduct(object s, EventArgs e) {
            if(Arh_Dialog.BUSY_CONTROL(s)) return;
            //<--------BUSY--CONTROL--------->
            var res = Arh_MessageBox.ShowMB(
                string.Format("Полное удаление продукта \"{0}\" из облака и прекращение его поддержки. Продолжить?\n\r(ОК/(X)Escape)",
                RepresentedSoftware.ProductInternalName),
                "Деинсталляция", Arh_MessageBox.AMBType.Info);
            if(res != DialogResult.OK)
                return;
            await JM.Core.DeletePublishedProductAsync(RepresentedSoftware);
            //await JM.Core.RefreshAllSoftwareListAsync();
        }
        private void OpenFolderAction(object s, EventArgs e) {
            try {
                Process.Start(RepresentedSoftware.Installation.InstallationCatalog);
            } catch(DirectoryNotFoundException) {
                Arh_MessageBox.ShowMB(
                    "Каталог программы не найден!\r\nВозможно, программа была удалена либо перемещена.",
                    type: Arh_MessageBox.AMBType.Warning);
            } catch(Exception ex) {
                Arh_MessageBox.ShowMB(
                    $"Неожиданная ошибка открытия каталога программы [{ex.Message}]!\r\n",
                    type: Arh_MessageBox.AMBType.Error);
                return;
            }
        }
        private void ReviewAction(object s, EventArgs e) {
            if(Arh_Dialog.BUSY_CONTROL(s)) return;
            //<--------BUSY--CONTROL--------->
            var res = new Arh_WriteReviewDialog().ShowDialog();
        }
        #endregion

        private void InitProductPresentation() {
            ActionPanel.HorizontalScroll.Enabled = false;
            ActionPanel.HorizontalScroll.Visible = false;
            const int D = 30;
            if(RepresentedSoftware.AdditInfo.BannerBitmap != null) {
                PictureBox pbBanner = new PictureBox();
                pbBanner.SizeMode = PictureBoxSizeMode.Zoom;
                pbBanner.Size = new Size(ActionPanel.Width - D, 120);
                pbBanner.BorderStyle = BorderStyle.FixedSingle;
                //pbBanner.Image = Image.FromStream(RepresentedSoftware.AdditInfo.BannerBitmap);
                RepresentedSoftware.AdditInfo.BannerBitmap.UpdateImage((s) => {
                    if(s == null) return;
                    if(Tag != null) (Tag as Image).Dispose();
                    pbBanner.Image = Image.FromStream(s);
                    Tag = pbBanner.Image;
                });
                ActionPanel.Controls.Add(pbBanner);
            }
            //
            Label lblDescr = new Label();
            lblDescr.AutoSize = true;
            //lblDescr.Size = new Size(ActionPanel.Width - D, h + D);
            lblDescr.ForeColor = Color.LightGray;
            lblDescr.Text = RepresentedSoftware.PublicDescription;
            ActionPanel.Controls.Add(lblDescr);
        }

        private void SelectW(bool select) {
            // Очищаем панель приложения
            ActionPanel.Controls.Clear();
            // Подсветка если выделен
            if(select)
                lbl_Title.BackColor = Color.DarkCyan;
            else {
                lbl_Title.BackColor = Color.FromArgb(64, 64, 64);
                return;
            }
            // Презентация
            InitProductPresentation();
            // Если установлено
            if(RepresentedSoftware.Installation != null) {
                if(RepresentedSoftware.CheckUpdates()) // Если есть обновления
                    CreateMenuButton("Обновить", Color.Yellow, UpdateAction); // (ОБНОВИТЬ)
                if(!RepresentedSoftware.CheckCriticalUpdates()) // Если нет критических обновлений
                    CreateMenuButton("Запустить", Color.MediumSpringGreen, LaunchAction); // (ЗАПУСТИТЬ)

                CreateMenuButton("Удалить", Color.Red, UninstallAction); // В любом случае (ДЕИНСТАЛЛИРОВАТЬ)
                CreateMenuButton("Открыть каталог", Color.AliceBlue, OpenFolderAction);
                CreateMenuButton("Написать отзыв", Color.White, ReviewAction);
            }
            else // Если не установлено
            {
                CreateMenuButton("Установить", Color.Cyan, InstallAction); // (УСТАНОВИТЬ)
                if(JM.Core.IsInDeveloperMode) // ПРИВИЛЕГИИ: (Developer)
                    CreateMenuButton("Dev: - Удалить продукт", Color.Pink, DeleteProduct); // (УДАЛИТЬ ПРОДУКТ)
            }
        }

        private void Activate(object sender, EventArgs e) {
            SelectedWidget = this;
        }
    }
}
