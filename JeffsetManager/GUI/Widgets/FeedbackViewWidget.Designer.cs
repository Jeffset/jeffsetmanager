﻿namespace JeffsetManager {
    partial class FeedbackViewWidget {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if(disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.pbAva = new System.Windows.Forms.PictureBox();
            this.btnExpand = new System.Windows.Forms.Button();
            this.lblUserName = new System.Windows.Forms.Label();
            this.lblWhen = new System.Windows.Forms.Label();
            this.btnDelete = new System.Windows.Forms.Button();
            this.ratingWidget = new JeffsetManager.RatingWidget();
            ((System.ComponentModel.ISupportInitialize)(this.pbAva)).BeginInit();
            this.SuspendLayout();
            // 
            // pbAva
            // 
            this.pbAva.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.pbAva.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.pbAva.Image = global::JeffsetManager.Properties.Resources.defaultAva;
            this.pbAva.Location = new System.Drawing.Point(8, 7);
            this.pbAva.Name = "pbAva";
            this.pbAva.Size = new System.Drawing.Size(70, 95);
            this.pbAva.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbAva.TabIndex = 8;
            this.pbAva.TabStop = false;
            // 
            // btnExpand
            // 
            this.btnExpand.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExpand.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExpand.Location = new System.Drawing.Point(136, 69);
            this.btnExpand.Margin = new System.Windows.Forms.Padding(5);
            this.btnExpand.Name = "btnExpand";
            this.btnExpand.Size = new System.Drawing.Size(309, 33);
            this.btnExpand.TabIndex = 9;
            this.btnExpand.Text = "<Заголовок отзыва>";
            this.btnExpand.UseVisualStyleBackColor = true;
            this.btnExpand.Click += new System.EventHandler(this.btnExpand_Click);
            // 
            // lblUserName
            // 
            this.lblUserName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblUserName.ForeColor = System.Drawing.Color.MediumSpringGreen;
            this.lblUserName.Location = new System.Drawing.Point(276, 8);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(169, 39);
            this.lblUserName.TabIndex = 10;
            this.lblUserName.Text = "Отзыв от Имя_Пользователя";
            this.lblUserName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblWhen
            // 
            this.lblWhen.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblWhen.Location = new System.Drawing.Point(89, 47);
            this.lblWhen.Name = "lblWhen";
            this.lblWhen.Size = new System.Drawing.Size(356, 21);
            this.lblWhen.TabIndex = 12;
            this.lblWhen.Text = "[дата-время]";
            this.lblWhen.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnDelete
            // 
            this.btnDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnDelete.Image = global::JeffsetManager.Properties.Resources.close;
            this.btnDelete.Location = new System.Drawing.Point(83, 68);
            this.btnDelete.Margin = new System.Windows.Forms.Padding(5);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(29, 33);
            this.btnDelete.TabIndex = 9;
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // ratingWidget
            // 
            this.ratingWidget.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ratingWidget.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ratingWidget.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ratingWidget.Location = new System.Drawing.Point(90, 8);
            this.ratingWidget.Name = "ratingWidget";
            this.ratingWidget.Rating = 3.5F;
            this.ratingWidget.ReadOnly = true;
            this.ratingWidget.Size = new System.Drawing.Size(178, 39);
            this.ratingWidget.TabIndex = 11;
            // 
            // FeedbackViewWidget
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Controls.Add(this.lblWhen);
            this.Controls.Add(this.ratingWidget);
            this.Controls.Add(this.lblUserName);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnExpand);
            this.Controls.Add(this.pbAva);
            this.ForeColor = System.Drawing.Color.Orange;
            this.MinimumSize = new System.Drawing.Size(400, 100);
            this.Name = "FeedbackViewWidget";
            this.Padding = new System.Windows.Forms.Padding(7);
            this.Size = new System.Drawing.Size(457, 114);
            ((System.ComponentModel.ISupportInitialize)(this.pbAva)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pbAva;
        private System.Windows.Forms.Button btnExpand;
        private System.Windows.Forms.Label lblUserName;
        private RatingWidget ratingWidget;
        private System.Windows.Forms.Label lblWhen;
        private System.Windows.Forms.Button btnDelete;
    }
}
