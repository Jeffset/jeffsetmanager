﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ManagerCore;
using JeffsetManager.Properties;
using System.Drawing.Drawing2D;

namespace JeffsetManager
{
    public partial class ArhStatusBar : UserControl
    {
        #region Paint
        private Brush progressBodyBrush;
        #endregion

        #region DesignerProperties
        private int _progress = 0;
        [Category("Appearance")]
        public int Progress
        {
            get { return _progress; }
            set
            {
                _progress = value < 0 ? 0 : value > 100 ? 100 : value;
            }
        }
        //------------------------------------
        private string _opName = "";
        [Category("Appearance")]
        public string OperationName
        {
            get { return _opName; }
            set { _opName = value; Invalidate(); }
        }
        //-------------------------------
        private float _borderWidth = 2.0f;
        [Category("Appearance")]
        public float BorderWidth
        {
            get { return _borderWidth; }
            set
            {
                _borderWidth = value;
            }
        }
        //---------------------------------
        private Color _borderColor = Color.MediumSpringGreen;
        [Category("Appearance")]
        public Color BorderColor
        {
            get { return _borderColor; }
            set
            {
                _borderColor = value;
            }
        }
        //===================================
        private Pen _InitBorderPen(Rectangle r)
        {
            var brush = new LinearGradientBrush(r, Color.Gray, BorderColor, 0f);
            var pbp = new Pen(brush, BorderWidth);
            pbp.DashStyle = DashStyle.Solid;
            return pbp;
        }
        #endregion

        public ArhStatusBar()
        {
            InitializeComponent();
            progressBodyBrush = new TextureBrush(Resources.progress);
        }

        // For progressBar texture resize;
        protected override void OnSizeChanged(EventArgs e)
        {
            base.OnSizeChanged(e);
            if (progressBodyBrush != null)
                progressBodyBrush.Dispose();
            progressBodyBrush = new TextureBrush(new Bitmap(Resources.progress, Size));
        }
        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            //---------------settings----------------------
            var render = e.Graphics;
            render.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            //-------------------render textured progress bar-----------
            //-------------------------body-----------------------------
            if (Progress != 0)
            {
                var wr = DisplayRectangle;
                var pb = new Rectangle(wr.X, wr.Y, (int)(wr.Width * Progress / 100d), wr.Height);
                render.FillRectangle(progressBodyBrush, pb);
                //------------------------border-----------------------------
                wr.Inflate(-1, -1);
                render.DrawRectangle(_InitBorderPen(pb), pb);
                //-------------------------text------------------------------
                string lbl = OperationName + Progress.ToString() + "%";
                var size = render.MeasureString(lbl, Font);
                float x = (wr.Width - size.Width) / 2f;
                float y = (wr.Height - size.Height) / 2f;
                x = pb.Right;
                var lblRect = new RectangleF(x, y, size.Width, size.Height);
                var rgb = 255 - (int)(Progress * 255f / 100f);
                var tbrush = new LinearGradientBrush(DisplayRectangle, Color.Orange, Color.MediumSpringGreen, 0f);
                render.DrawString(lbl, Font, tbrush, lblRect);
            }

        }
    }
}
