﻿namespace JeffsetManager
{
    partial class JReqVerifyWidget
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_Msg = new System.Windows.Forms.Label();
            this.btnDecline = new System.Windows.Forms.Button();
            this.btnVerify = new System.Windows.Forms.Button();
            this.cb_UserLicense = new System.Windows.Forms.ComboBox();
            this.chb_Tester = new System.Windows.Forms.CheckBox();
            this.pbAva = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pbAva)).BeginInit();
            this.SuspendLayout();
            // 
            // lbl_Msg
            // 
            this.lbl_Msg.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_Msg.Font = new System.Drawing.Font("Consolas", 8F);
            this.lbl_Msg.ForeColor = System.Drawing.Color.White;
            this.lbl_Msg.Location = new System.Drawing.Point(81, 9);
            this.lbl_Msg.Margin = new System.Windows.Forms.Padding(5);
            this.lbl_Msg.Name = "lbl_Msg";
            this.lbl_Msg.Size = new System.Drawing.Size(305, 34);
            this.lbl_Msg.TabIndex = 1;
            this.lbl_Msg.Text = "Error4 (Бараев) - Extended";
            this.lbl_Msg.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnDecline
            // 
            this.btnDecline.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDecline.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDecline.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.btnDecline.Location = new System.Drawing.Point(394, 44);
            this.btnDecline.Name = "btnDecline";
            this.btnDecline.Size = new System.Drawing.Size(74, 29);
            this.btnDecline.TabIndex = 0;
            this.btnDecline.Text = "Cancel";
            this.btnDecline.UseVisualStyleBackColor = true;
            this.btnDecline.Click += new System.EventHandler(this.btnDecline_Click);
            // 
            // btnVerify
            // 
            this.btnVerify.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnVerify.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnVerify.ForeColor = System.Drawing.Color.Cyan;
            this.btnVerify.Location = new System.Drawing.Point(394, 9);
            this.btnVerify.Name = "btnVerify";
            this.btnVerify.Size = new System.Drawing.Size(74, 29);
            this.btnVerify.TabIndex = 0;
            this.btnVerify.Text = "Accept";
            this.btnVerify.UseVisualStyleBackColor = true;
            this.btnVerify.Click += new System.EventHandler(this.btnVerify_Click);
            // 
            // cb_UserLicense
            // 
            this.cb_UserLicense.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cb_UserLicense.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cb_UserLicense.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_UserLicense.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb_UserLicense.ForeColor = System.Drawing.Color.MediumSpringGreen;
            this.cb_UserLicense.FormattingEnabled = true;
            this.cb_UserLicense.Items.AddRange(new object[] {
            "Free",
            "Standard",
            "Extended",
            "Developer"});
            this.cb_UserLicense.Location = new System.Drawing.Point(84, 47);
            this.cb_UserLicense.Name = "cb_UserLicense";
            this.cb_UserLicense.Size = new System.Drawing.Size(128, 25);
            this.cb_UserLicense.TabIndex = 5;
            // 
            // chb_Tester
            // 
            this.chb_Tester.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chb_Tester.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chb_Tester.ForeColor = System.Drawing.Color.Yellow;
            this.chb_Tester.Location = new System.Drawing.Point(218, 48);
            this.chb_Tester.Name = "chb_Tester";
            this.chb_Tester.Size = new System.Drawing.Size(168, 21);
            this.chb_Tester.TabIndex = 6;
            this.chb_Tester.Text = "Лицензия тестера";
            this.chb_Tester.UseVisualStyleBackColor = true;
            // 
            // pbAva
            // 
            this.pbAva.Image = global::JeffsetManager.Properties.Resources.defaultAva;
            this.pbAva.Location = new System.Drawing.Point(7, 8);
            this.pbAva.Name = "pbAva";
            this.pbAva.Size = new System.Drawing.Size(70, 70);
            this.pbAva.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbAva.TabIndex = 7;
            this.pbAva.TabStop = false;
            // 
            // JReqVerifyWidget
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(41)))), ((int)(((byte)(70)))));
            this.Controls.Add(this.pbAva);
            this.Controls.Add(this.chb_Tester);
            this.Controls.Add(this.cb_UserLicense);
            this.Controls.Add(this.lbl_Msg);
            this.Controls.Add(this.btnDecline);
            this.Controls.Add(this.btnVerify);
            this.Font = new System.Drawing.Font("Courier New", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Margin = new System.Windows.Forms.Padding(0);
            this.MinimumSize = new System.Drawing.Size(0, 85);
            this.Name = "JReqVerifyWidget";
            this.Size = new System.Drawing.Size(477, 85);
            ((System.ComponentModel.ISupportInitialize)(this.pbAva)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label lbl_Msg;
        private System.Windows.Forms.Button btnDecline;
        private System.Windows.Forms.Button btnVerify;
        private System.Windows.Forms.ComboBox cb_UserLicense;
        private System.Windows.Forms.CheckBox chb_Tester;
        private System.Windows.Forms.PictureBox pbAva;
    }
}
