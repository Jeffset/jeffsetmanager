﻿namespace JeffsetManager
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.flpActions = new System.Windows.Forms.FlowLayoutPanel();
            this.flpSoftware = new System.Windows.Forms.FlowLayoutPanel();
            this.pnAcc = new System.Windows.Forms.Panel();
            this.lbl_Type = new System.Windows.Forms.Label();
            this.pb_Avatar = new System.Windows.Forms.PictureBox();
            this.lbl_Name = new System.Windows.Forms.Label();
            this.statusBar = new System.Windows.Forms.StatusStrip();
            this.lblStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.lbl_OpName = new System.Windows.Forms.ToolStripStatusLabel();
            this.pbProgress = new System.Windows.Forms.ToolStripProgressBar();
            this.btn_PublishSelf = new System.Windows.Forms.Button();
            this._pbMainBanner = new System.Windows.Forms.PictureBox();
            this.btn_Refresh = new System.Windows.Forms.Button();
            this.btn_P2PTest = new System.Windows.Forms.Button();
            this.btnAbout = new System.Windows.Forms.Button();
            this.pnAcc.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Avatar)).BeginInit();
            this.statusBar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._pbMainBanner)).BeginInit();
            this.SuspendLayout();
            // 
            // flpActions
            // 
            this.flpActions.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flpActions.AutoScroll = true;
            this.flpActions.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flpActions.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flpActions.Location = new System.Drawing.Point(424, 308);
            this.flpActions.Name = "flpActions";
            this.flpActions.Size = new System.Drawing.Size(296, 379);
            this.flpActions.TabIndex = 11;
            this.flpActions.WrapContents = false;
            // 
            // flpSoftware
            // 
            this.flpSoftware.AutoScroll = true;
            this.flpSoftware.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flpSoftware.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flpSoftware.Location = new System.Drawing.Point(14, 180);
            this.flpSoftware.Name = "flpSoftware";
            this.flpSoftware.Size = new System.Drawing.Size(404, 517);
            this.flpSoftware.TabIndex = 9;
            this.flpSoftware.WrapContents = false;
            // 
            // pnAcc
            // 
            this.pnAcc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnAcc.Controls.Add(this.lbl_Type);
            this.pnAcc.Controls.Add(this.pb_Avatar);
            this.pnAcc.Controls.Add(this.lbl_Name);
            this.pnAcc.Location = new System.Drawing.Point(424, 180);
            this.pnAcc.Name = "pnAcc";
            this.pnAcc.Size = new System.Drawing.Size(300, 122);
            this.pnAcc.TabIndex = 7;
            // 
            // lbl_Type
            // 
            this.lbl_Type.Location = new System.Drawing.Point(14, 47);
            this.lbl_Type.Name = "lbl_Type";
            this.lbl_Type.Size = new System.Drawing.Size(169, 20);
            this.lbl_Type.TabIndex = 8;
            this.lbl_Type.Text = "---";
            this.lbl_Type.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // pb_Avatar
            // 
            this.pb_Avatar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pb_Avatar.Image = global::JeffsetManager.Properties.Resources.defaultAva;
            this.pb_Avatar.Location = new System.Drawing.Point(189, 3);
            this.pb_Avatar.Name = "pb_Avatar";
            this.pb_Avatar.Size = new System.Drawing.Size(106, 106);
            this.pb_Avatar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_Avatar.TabIndex = 6;
            this.pb_Avatar.TabStop = false;
            this.pb_Avatar.Click += new System.EventHandler(this.pb_Avatar_Click);
            // 
            // lbl_Name
            // 
            this.lbl_Name.Font = new System.Drawing.Font("Courier New", 13F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbl_Name.Location = new System.Drawing.Point(10, 3);
            this.lbl_Name.Name = "lbl_Name";
            this.lbl_Name.Size = new System.Drawing.Size(174, 44);
            this.lbl_Name.TabIndex = 7;
            this.lbl_Name.Text = "Войти в Arahon";
            this.lbl_Name.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // statusBar
            // 
            this.statusBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblStatus,
            this.lbl_OpName,
            this.pbProgress});
            this.statusBar.Location = new System.Drawing.Point(0, 739);
            this.statusBar.Name = "statusBar";
            this.statusBar.Padding = new System.Windows.Forms.Padding(1, 0, 21, 0);
            this.statusBar.Size = new System.Drawing.Size(736, 27);
            this.statusBar.SizingGrip = false;
            this.statusBar.TabIndex = 2;
            this.statusBar.Text = "statusStrip1";
            // 
            // lblStatus
            // 
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(39, 22);
            this.lblStatus.Text = "Ready";
            // 
            // lbl_OpName
            // 
            this.lbl_OpName.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.lbl_OpName.Name = "lbl_OpName";
            this.lbl_OpName.Size = new System.Drawing.Size(0, 22);
            // 
            // pbProgress
            // 
            this.pbProgress.MarqueeAnimationSpeed = 1;
            this.pbProgress.Name = "pbProgress";
            this.pbProgress.Size = new System.Drawing.Size(225, 21);
            this.pbProgress.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            // 
            // btn_PublishSelf
            // 
            this.btn_PublishSelf.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn_PublishSelf.ForeColor = System.Drawing.Color.Gold;
            this.btn_PublishSelf.Location = new System.Drawing.Point(570, 144);
            this.btn_PublishSelf.Name = "btn_PublishSelf";
            this.btn_PublishSelf.Size = new System.Drawing.Size(149, 23);
            this.btn_PublishSelf.TabIndex = 12;
            this.btn_PublishSelf.Text = "Dev: Меню";
            this.btn_PublishSelf.UseVisualStyleBackColor = true;
            this.btn_PublishSelf.Visible = false;
            this.btn_PublishSelf.Click += new System.EventHandler(this.btn_DevMenu_Click);
            // 
            // _pbMainBanner
            // 
            this._pbMainBanner.Image = global::JeffsetManager.Properties.Resources.main_banner;
            this._pbMainBanner.Location = new System.Drawing.Point(14, 49);
            this._pbMainBanner.Name = "_pbMainBanner";
            this._pbMainBanner.Size = new System.Drawing.Size(711, 124);
            this._pbMainBanner.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this._pbMainBanner.TabIndex = 10;
            this._pbMainBanner.TabStop = false;
            // 
            // btn_Refresh
            // 
            this.btn_Refresh.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn_Refresh.Location = new System.Drawing.Point(14, 703);
            this.btn_Refresh.Name = "btn_Refresh";
            this.btn_Refresh.Size = new System.Drawing.Size(404, 23);
            this.btn_Refresh.TabIndex = 13;
            this.btn_Refresh.Text = "Обновить список";
            this.btn_Refresh.UseVisualStyleBackColor = true;
            this.btn_Refresh.Click += new System.EventHandler(this.btn_Refresh_Click);
            // 
            // btn_P2PTest
            // 
            this.btn_P2PTest.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_P2PTest.Location = new System.Drawing.Point(424, 693);
            this.btn_P2PTest.Name = "btn_P2PTest";
            this.btn_P2PTest.Size = new System.Drawing.Size(295, 33);
            this.btn_P2PTest.TabIndex = 14;
            this.btn_P2PTest.Text = "P2PPlugin";
            this.btn_P2PTest.UseVisualStyleBackColor = true;
            this.btn_P2PTest.Click += new System.EventHandler(this.btn_P2PTest_Click);
            // 
            // btnAbout
            // 
            this.btnAbout.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnAbout.ForeColor = System.Drawing.Color.DarkGray;
            this.btnAbout.Location = new System.Drawing.Point(570, 64);
            this.btnAbout.Name = "btnAbout";
            this.btnAbout.Size = new System.Drawing.Size(149, 31);
            this.btnAbout.TabIndex = 15;
            this.btnAbout.Text = "О программе...";
            this.btnAbout.UseVisualStyleBackColor = true;
            this.btnAbout.Click += new System.EventHandler(this.btnAbout_Click);
            // 
            // FormMain
            // 
            this.ArahonText = "Arahon: Jeffset Manager";
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(736, 766);
            this.Controls.Add(this.btnAbout);
            this.Controls.Add(this.btn_P2PTest);
            this.Controls.Add(this.btn_Refresh);
            this.Controls.Add(this.btn_PublishSelf);
            this.Controls.Add(this.flpActions);
            this.Controls.Add(this._pbMainBanner);
            this.Controls.Add(this.flpSoftware);
            this.Controls.Add(this.pnAcc);
            this.Controls.Add(this.statusBar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.Name = "FormMain";
            this.Opacity = 1D;
            this.ShowInTaskbar = true;
            this.Text = "Jeffset Manager";
            this.UseSmoothClosing = false;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormMain_FormClosing);
            this.Load += new System.EventHandler(this.FormMain_Load);
            this.pnAcc.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pb_Avatar)).EndInit();
            this.statusBar.ResumeLayout(false);
            this.statusBar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._pbMainBanner)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.StatusStrip statusBar;
        private System.Windows.Forms.ToolStripStatusLabel lblStatus;
        private System.Windows.Forms.ToolStripProgressBar pbProgress;
        private System.Windows.Forms.PictureBox pb_Avatar;
        private System.Windows.Forms.Panel pnAcc;
        private System.Windows.Forms.Label lbl_Name;
        private System.Windows.Forms.FlowLayoutPanel flpSoftware;
        private System.Windows.Forms.PictureBox _pbMainBanner;
        private System.Windows.Forms.Label lbl_Type;
        private System.Windows.Forms.FlowLayoutPanel flpActions;
        private System.Windows.Forms.Button btn_PublishSelf;
        private System.Windows.Forms.Button btn_Refresh;
        private System.Windows.Forms.ToolStripStatusLabel lbl_OpName;
        private System.Windows.Forms.Button btn_P2PTest;
        private System.Windows.Forms.Button btnAbout;
    }
}

