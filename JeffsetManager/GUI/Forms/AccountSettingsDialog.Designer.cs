﻿namespace JeffsetManager
{
    partial class Arh_AccSettingsDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Label label1;
            this.btn_Accept = new System.Windows.Forms.Button();
            this.lbl_DupNewPs = new System.Windows.Forms.Label();
            this.lbl_NewPs = new System.Windows.Forms.Label();
            this.tb_DupNewPs = new System.Windows.Forms.TextBox();
            this.lbl_OldPs = new System.Windows.Forms.Label();
            this.tb_NewPs = new System.Windows.Forms.TextBox();
            this.tb_OldPs = new System.Windows.Forms.TextBox();
            this.tb_Name = new System.Windows.Forms.TextBox();
            this.btnLogOff = new System.Windows.Forms.Button();
            this.pb_Avatar = new System.Windows.Forms.PictureBox();
            this.btn_LicExtend = new System.Windows.Forms.Button();
            this.lbl_LicInfo = new System.Windows.Forms.Label();
            this.lbl_License = new System.Windows.Forms.Label();
            label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Avatar)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(12, 218);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(161, 17);
            label1.TabIndex = 8;
            label1.Text = "Имя пользователя:";
            // 
            // btn_Accept
            // 
            this.btn_Accept.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Accept.Location = new System.Drawing.Point(14, 583);
            this.btn_Accept.Name = "btn_Accept";
            this.btn_Accept.Size = new System.Drawing.Size(295, 42);
            this.btn_Accept.TabIndex = 228;
            this.btn_Accept.Text = "Применить";
            this.btn_Accept.UseVisualStyleBackColor = true;
            this.btn_Accept.Click += new System.EventHandler(this.btn_Accept_Click);
            // 
            // lbl_DupNewPs
            // 
            this.lbl_DupNewPs.AutoSize = true;
            this.lbl_DupNewPs.Location = new System.Drawing.Point(14, 507);
            this.lbl_DupNewPs.Name = "lbl_DupNewPs";
            this.lbl_DupNewPs.Size = new System.Drawing.Size(161, 17);
            this.lbl_DupNewPs.TabIndex = 8;
            this.lbl_DupNewPs.Text = "Повторить пароль:";
            // 
            // lbl_NewPs
            // 
            this.lbl_NewPs.AutoSize = true;
            this.lbl_NewPs.Location = new System.Drawing.Point(14, 458);
            this.lbl_NewPs.Name = "lbl_NewPs";
            this.lbl_NewPs.Size = new System.Drawing.Size(125, 17);
            this.lbl_NewPs.TabIndex = 8;
            this.lbl_NewPs.Text = "Новый пароль:";
            // 
            // tb_DupNewPs
            // 
            this.tb_DupNewPs.BackColor = System.Drawing.Color.Silver;
            this.tb_DupNewPs.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_DupNewPs.Location = new System.Drawing.Point(14, 528);
            this.tb_DupNewPs.Name = "tb_DupNewPs";
            this.tb_DupNewPs.ShortcutsEnabled = false;
            this.tb_DupNewPs.Size = new System.Drawing.Size(294, 24);
            this.tb_DupNewPs.TabIndex = 3;
            this.tb_DupNewPs.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tb_DupNewPs.UseSystemPasswordChar = true;
            // 
            // lbl_OldPs
            // 
            this.lbl_OldPs.AutoSize = true;
            this.lbl_OldPs.Location = new System.Drawing.Point(14, 409);
            this.lbl_OldPs.Name = "lbl_OldPs";
            this.lbl_OldPs.Size = new System.Drawing.Size(134, 17);
            this.lbl_OldPs.TabIndex = 8;
            this.lbl_OldPs.Text = "Старый пароль:";
            // 
            // tb_NewPs
            // 
            this.tb_NewPs.BackColor = System.Drawing.Color.Silver;
            this.tb_NewPs.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_NewPs.Location = new System.Drawing.Point(14, 479);
            this.tb_NewPs.Name = "tb_NewPs";
            this.tb_NewPs.Size = new System.Drawing.Size(294, 24);
            this.tb_NewPs.TabIndex = 2;
            this.tb_NewPs.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tb_NewPs.UseSystemPasswordChar = true;
            // 
            // tb_OldPs
            // 
            this.tb_OldPs.BackColor = System.Drawing.Color.Silver;
            this.tb_OldPs.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_OldPs.Location = new System.Drawing.Point(14, 430);
            this.tb_OldPs.Name = "tb_OldPs";
            this.tb_OldPs.Size = new System.Drawing.Size(294, 24);
            this.tb_OldPs.TabIndex = 1;
            this.tb_OldPs.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tb_OldPs.UseSystemPasswordChar = true;
            // 
            // tb_Name
            // 
            this.tb_Name.BackColor = System.Drawing.Color.Silver;
            this.tb_Name.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_Name.Location = new System.Drawing.Point(12, 239);
            this.tb_Name.Name = "tb_Name";
            this.tb_Name.Size = new System.Drawing.Size(294, 24);
            this.tb_Name.TabIndex = 0;
            this.tb_Name.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btnLogOff
            // 
            this.btnLogOff.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLogOff.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.btnLogOff.Location = new System.Drawing.Point(17, 65);
            this.btnLogOff.Margin = new System.Windows.Forms.Padding(4);
            this.btnLogOff.Name = "btnLogOff";
            this.btnLogOff.Size = new System.Drawing.Size(291, 38);
            this.btnLogOff.TabIndex = 230;
            this.btnLogOff.Text = "Выход из системы Arahon";
            this.btnLogOff.UseVisualStyleBackColor = true;
            this.btnLogOff.Click += new System.EventHandler(this.btnLogOff_Click);
            // 
            // pb_Avatar
            // 
            this.pb_Avatar.Image = global::JeffsetManager.Properties.Resources.defaultAva;
            this.pb_Avatar.Location = new System.Drawing.Point(107, 110);
            this.pb_Avatar.Name = "pb_Avatar";
            this.pb_Avatar.Size = new System.Drawing.Size(100, 100);
            this.pb_Avatar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_Avatar.TabIndex = 9;
            this.pb_Avatar.TabStop = false;
            // 
            // btn_LicExtend
            // 
            this.btn_LicExtend.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_LicExtend.Location = new System.Drawing.Point(11, 363);
            this.btn_LicExtend.Name = "btn_LicExtend";
            this.btn_LicExtend.Size = new System.Drawing.Size(297, 31);
            this.btn_LicExtend.TabIndex = 234;
            this.btn_LicExtend.Text = "Повышение лицензии...";
            this.btn_LicExtend.UseVisualStyleBackColor = true;
            this.btn_LicExtend.Click += new System.EventHandler(this.btn_LicExtend_Click);
            // 
            // lbl_LicInfo
            // 
            this.lbl_LicInfo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_LicInfo.ForeColor = System.Drawing.Color.Silver;
            this.lbl_LicInfo.Location = new System.Drawing.Point(12, 291);
            this.lbl_LicInfo.Name = "lbl_LicInfo";
            this.lbl_LicInfo.Size = new System.Drawing.Size(294, 66);
            this.lbl_LicInfo.TabIndex = 235;
            this.lbl_LicInfo.Text = "Для расширения лицензии необходимо послать запрос";
            this.lbl_LicInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_License
            // 
            this.lbl_License.Location = new System.Drawing.Point(17, 267);
            this.lbl_License.Name = "lbl_License";
            this.lbl_License.Size = new System.Drawing.Size(289, 24);
            this.lbl_License.TabIndex = 236;
            this.lbl_License.Text = "<Лицензия>";
            this.lbl_License.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Arh_AccSettingsDialog
            // 
            this.AcceptButton = this.btn_Accept;
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 17F);
            this.ClientSize = new System.Drawing.Size(324, 642);
            this.Controls.Add(this.lbl_License);
            this.Controls.Add(this.lbl_LicInfo);
            this.Controls.Add(this.btn_LicExtend);
            this.Controls.Add(this.btnLogOff);
            this.Controls.Add(this.pb_Avatar);
            this.Controls.Add(this.lbl_DupNewPs);
            this.Controls.Add(this.lbl_NewPs);
            this.Controls.Add(this.tb_DupNewPs);
            this.Controls.Add(this.lbl_OldPs);
            this.Controls.Add(this.tb_NewPs);
            this.Controls.Add(label1);
            this.Controls.Add(this.tb_OldPs);
            this.Controls.Add(this.tb_Name);
            this.Controls.Add(this.btn_Accept);
            this.Name = "Arh_AccSettingsDialog";
            this.Load += new System.EventHandler(this.AccountSettingsDialog_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pb_Avatar)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_Accept;
        private System.Windows.Forms.TextBox tb_Name;
        private System.Windows.Forms.TextBox tb_OldPs;
        private System.Windows.Forms.PictureBox pb_Avatar;
        private System.Windows.Forms.TextBox tb_NewPs;
        private System.Windows.Forms.TextBox tb_DupNewPs;
        private System.Windows.Forms.Label lbl_OldPs;
        private System.Windows.Forms.Label lbl_NewPs;
        private System.Windows.Forms.Label lbl_DupNewPs;
        private System.Windows.Forms.Button btnLogOff;
        private System.Windows.Forms.Button btn_LicExtend;
        private System.Windows.Forms.Label lbl_LicInfo;
        private System.Windows.Forms.Label lbl_License;
    }
}
