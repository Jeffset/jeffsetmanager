﻿using ManagerCore;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace JeffsetManager
{
    public partial class Arh_LicenseReqSend : JeffsetManager.Arh_Dialog
    {
        JDRequest Request;
        public Arh_LicenseReqSend(JDRequest req)
        {
            InitializeComponent();
            Request = req;
            if (Request != null)
            {
                var lic = (JeffsetLicence)req.Data["LICENSE"];
                chb_Tester.Checked = lic.TesterLicense;
                cb_UserLicense.Text = lic.UserLicense.ToString();
                cb_UserLicense.Enabled = false;
                chb_Tester.Enabled = false;
                btnSendRequest.Text = "Отменить";
                btnSendRequest.ForeColor = Color.Orange;
            }
            else
            {
                btnSendRequest.Text = "Запросить";
                btnSendRequest.ForeColor = Color.MediumSpringGreen;
            }
        }

        private async void btnSendRequest_Click(object sender, EventArgs e)
        {
            if (Arh_Dialog.BUSY_CONTROL(sender)) return;
            //<--------BUSY--CONTROL--------->
            #region Checks
            if (string.IsNullOrEmpty(cb_UserLicense.Text))
            { _ErrorAnim(lbl_Lic); return; }
            #endregion
            var Lic = new JeffsetLicence()
            {
                TesterLicense = chb_Tester.Checked,
                UserLicense = (JeffsetUserType)Enum.Parse(typeof(JeffsetUserType), cb_UserLicense.Text)
            };
            if (Request != null)
            {
                var last = btnSendRequest.Text; btnSendRequest.Text = "Отмена запроса...";
                await JM.Core.RemoveRequestAsync(Request);
                btnSendRequest.Text = "Запросить";
                btnSendRequest.ForeColor = Color.MediumSpringGreen;
                cb_UserLicense.Enabled = true;
                chb_Tester.Enabled = true;
                Request = null;
            }
            else
            {
                var last = btnSendRequest.Text; btnSendRequest.Text = "Посылка запроса...";
                await JM.Core.SendRequestAsync(JDRequest.CreateAccountChangeLicense(Lic));
                btnSendRequest.Text = last;
                DialogResult = DialogResult.OK;
                Close();
            }
            
        }
    }
}
