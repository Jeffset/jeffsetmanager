﻿namespace JeffsetManager
{
    partial class Arh_WriteReviewDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Arh_WriteReviewDialog));
            this.tb_Text = new System.Windows.Forms.TextBox();
            this.tb_Title = new System.Windows.Forms.TextBox();
            this.lbl_Title = new System.Windows.Forms.Label();
            this.lbl_Text = new System.Windows.Forms.Label();
            this.btn_Send = new System.Windows.Forms.Button();
            this.ratingWidget = new JeffsetManager.RatingWidget();
            this.lblRating = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // tb_Text
            // 
            this.tb_Text.AcceptsReturn = true;
            this.tb_Text.BackColor = System.Drawing.Color.Silver;
            this.tb_Text.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_Text.Font = new System.Drawing.Font("Courier New", 11F);
            this.tb_Text.ForeColor = System.Drawing.Color.Black;
            this.tb_Text.Location = new System.Drawing.Point(12, 187);
            this.tb_Text.MaxLength = 1000;
            this.tb_Text.Multiline = true;
            this.tb_Text.Name = "tb_Text";
            this.tb_Text.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tb_Text.Size = new System.Drawing.Size(493, 294);
            this.tb_Text.TabIndex = 1;
            // 
            // tb_Title
            // 
            this.tb_Title.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_Title.Location = new System.Drawing.Point(12, 146);
            this.tb_Title.MaxLength = 20;
            this.tb_Title.Name = "tb_Title";
            this.tb_Title.Size = new System.Drawing.Size(493, 17);
            this.tb_Title.TabIndex = 0;
            this.tb_Title.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lbl_Title
            // 
            this.lbl_Title.AutoSize = true;
            this.lbl_Title.Location = new System.Drawing.Point(12, 126);
            this.lbl_Title.Name = "lbl_Title";
            this.lbl_Title.Size = new System.Drawing.Size(386, 17);
            this.lbl_Title.TabIndex = 9;
            this.lbl_Title.Text = "Заголовок сообщения (не более 20 знаков)*:";
            // 
            // lbl_Text
            // 
            this.lbl_Text.AutoSize = true;
            this.lbl_Text.Location = new System.Drawing.Point(12, 167);
            this.lbl_Text.Name = "lbl_Text";
            this.lbl_Text.Size = new System.Drawing.Size(287, 17);
            this.lbl_Text.TabIndex = 9;
            this.lbl_Text.Text = "Текст (не более 1000 знаков)* :";
            // 
            // btn_Send
            // 
            this.btn_Send.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Send.Location = new System.Drawing.Point(12, 487);
            this.btn_Send.Name = "btn_Send";
            this.btn_Send.Size = new System.Drawing.Size(493, 37);
            this.btn_Send.TabIndex = 2;
            this.btn_Send.Text = "Отправить сообщение";
            this.btn_Send.UseVisualStyleBackColor = true;
            this.btn_Send.Click += new System.EventHandler(this.btn_Send_Click);
            // 
            // ratingWidget
            // 
            this.ratingWidget.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ratingWidget.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ratingWidget.BackgroundImage")));
            this.ratingWidget.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ratingWidget.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ratingWidget.Location = new System.Drawing.Point(154, 65);
            this.ratingWidget.Margin = new System.Windows.Forms.Padding(4);
            this.ratingWidget.Name = "ratingWidget";
            this.ratingWidget.Rating = 3.5F;
            this.ratingWidget.ReadOnly = false;
            this.ratingWidget.Size = new System.Drawing.Size(249, 57);
            this.ratingWidget.TabIndex = 10;
            // 
            // lblRating
            // 
            this.lblRating.AutoSize = true;
            this.lblRating.Location = new System.Drawing.Point(67, 85);
            this.lblRating.Name = "lblRating";
            this.lblRating.Size = new System.Drawing.Size(80, 17);
            this.lblRating.TabIndex = 9;
            this.lblRating.Text = "Оценка*:";
            // 
            // Arh_WriteReviewDialog
            // 
            this.AcceptButton = this.btn_Send;
            this.ArahonText = "Arahon: Отзыв/Сообщение разработчику";
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 17F);
            this.ClientSize = new System.Drawing.Size(524, 536);
            this.Controls.Add(this.ratingWidget);
            this.Controls.Add(this.btn_Send);
            this.Controls.Add(this.lbl_Text);
            this.Controls.Add(this.lblRating);
            this.Controls.Add(this.lbl_Title);
            this.Controls.Add(this.tb_Title);
            this.Controls.Add(this.tb_Text);
            this.Name = "Arh_WriteReviewDialog";
            this.Opacity = 1D;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tb_Text;
        private System.Windows.Forms.TextBox tb_Title;
        private System.Windows.Forms.Label lbl_Title;
        private System.Windows.Forms.Label lbl_Text;
        private System.Windows.Forms.Button btn_Send;
        private RatingWidget ratingWidget;
        private System.Windows.Forms.Label lblRating;
    }
}
