﻿using ManagerCore;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using JeffsetManager.Properties;
using System.Net;

namespace JeffsetManager
{
    public partial class Arh_AccRegDialog : JeffsetManager.Arh_Dialog
    {
        MemoryStream avatarMs;
        public string Login
        {
            get { return tb_Login.Text; }
            set { tb_Login.Text = value; }
        }

        public string Password
        {
            get { return tb_Pass.Text; }
            set { tb_Pass.Text = value; }
        }


        public Arh_AccRegDialog()
        {
            InitializeComponent();
        }

        private async void btnAccept_Click(object sender, EventArgs e)
        {
            #region Checks
            string Name = tb_Name.Text,
                Login = tb_Login.Text,
                Password = tb_Pass.Text,
                PasswordRepeat = tb_RepPass.Text,
                LicenseStr = cb_UserLicense.Text;
            if (string.IsNullOrEmpty(Name))
            { _ErrorAnim(lbl_Name); return; }
            if (string.IsNullOrWhiteSpace(Login))
            { _ErrorAnim(lbl_Login); return; }
            if (string.IsNullOrWhiteSpace(LicenseStr))
            { _ErrorAnim(lbl_UsrLic); return; }
            if (Password != PasswordRepeat)
            { _ErrorAnim(lbl_RepPass); return; }
            JeffsetUserCredentials credentials = new JeffsetUserCredentials(Login, Password);
            if (!credentials.CheckValidity(true))
            { _ErrorAnim(lbl_Pass); return; }
            if (avatarMs == null)
            { _ErrorAnim(lbl_Avatar); return; }
            #endregion

            JeffsetUserAccount account = new JeffsetUserAccount();
            account.Credentials = credentials;
            account.Name = Name;
            account.License = new JeffsetLicence()
            {
                TesterLicense = false,
                UserLicense = JeffsetUserType.Unverified
            };
            var wishedLicense = new JeffsetLicence()
            {
                TesterLicense = chb_Tester.Checked,
                UserLicense = (JeffsetUserType)Enum.Parse(typeof(JeffsetUserType), LicenseStr)
            };
            btnAccept.Text = "Отправка запроса в систему...";
            try {
                var result = await JM.Core.RegisterAccountAsync(account, wishedLicense, avatarMs);
                btnAccept.Text = "Зарегистрировать";
                switch(result) {
                    case JeffsetManagerCore.SignUpResult.OK:
                        Arh_MessageBox.ShowMB(
    $@"{Name}, Запрос на создание Вашей учетной записи успешно отправлен.
Сейчас вы можете войти в систему в ознакомительном режиме.",
                            "Запрос отправлен успешно", Arh_MessageBox.AMBType.Success);
                        DialogResult = DialogResult.OK; Close();
                        break;
                    case JeffsetManagerCore.SignUpResult.UserNameConflict:
                        _ErrorAnim(lbl_Login);
                        break;
                    case JeffsetManagerCore.SignUpResult.IncorrectData:
                        _ErrorAnim(this);
                        break;
                }
            } catch(WebException ex) when(ex.Status == WebExceptionStatus.NameResolutionFailure) {
                Arh_MessageBox.ShowMB("Ошибка соединения! Проверьте подключение к интернету.",
                    type: Arh_MessageBox.AMBType.Error);
            } catch(WebException ex) {
                Arh_MessageBox.ShowMB(
                    $"Неожиданная ошибка соединения [{ex.Status}]! Невозможно зарегистрироваться!",
                    type: Arh_MessageBox.AMBType.Error);
            } catch(Exception ex) {
                Arh_MessageBox.ShowMB(
                    $"Неожиданная ошибка! Невозможно зарегистрироваться!\r\ninfo:{ex.Message}",
                    type: Arh_MessageBox.AMBType.Error);
            }
        }

        const int E = 120;
        private void pbAva_Click(object sender, EventArgs e)
        {
            var ans = chooseAva.ShowDialog();
            if (ans == DialogResult.Cancel) return;
            //
            try {
                if(pbAva.Image != Resources.defaultAva)
                    pbAva.Image.Dispose();
                //
                Image orig = Image.FromFile(chooseAva.FileName, true);
                Image clipped = new Bitmap(E, E);
                Graphics g1 = Graphics.FromImage(clipped);
                g1.InterpolationMode = InterpolationMode.HighQualityBicubic;
                int A = Math.Min(orig.Width, orig.Height);
                Rectangle sq = new Rectangle((orig.Width - A) / 2, (orig.Height - A) / 2, A, A);
                g1.DrawImage(orig, new Rectangle(0, 0, E, E), sq, GraphicsUnit.Pixel);
                orig.Dispose();
                g1.Dispose();
                //
                Image clr = new Bitmap(120, 120);
                Graphics render = Graphics.FromImage(clr);
                render.SmoothingMode = SmoothingMode.HighQuality;
                TextureBrush tbr = new TextureBrush(clipped);
                render.FillEllipse(tbr, 0, 0, 120, 120);
                pbAva.Image = clr;
                render.Dispose();
                //
                ImageFormat f = ImageFormat.Png;
                if(avatarMs != null)
                    avatarMs.Dispose();
                avatarMs = new MemoryStream();
                clr.Save(avatarMs, f);
                lbl_Avatar.Text = "Аватар (ОК)";
            } catch(Exception) {
                Arh_MessageBox.ShowMB("Ошибка обработки аватара!", "Графическая ошибка", Arh_MessageBox.AMBType.Error);
                pbAva.Image = Resources.defaultAva;
                lbl_Avatar.Text = "Аватар (Ошибка)";
            }
        }
    }
}
