﻿namespace JeffsetManager {
    partial class Arh_AccountManagement {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if(disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.Windows.Forms.Label label1;
            this.flpAccounts = new System.Windows.Forms.FlowLayoutPanel();
            this.btn_AccRefresh = new System.Windows.Forms.Button();
            label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            label1.Location = new System.Drawing.Point(9, 67);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(364, 29);
            label1.TabIndex = 7;
            label1.Text = "Все зарегистрированные аккаунты:";
            label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // flpAccounts
            // 
            this.flpAccounts.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flpAccounts.AutoScroll = true;
            this.flpAccounts.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flpAccounts.Location = new System.Drawing.Point(12, 102);
            this.flpAccounts.Name = "flpAccounts";
            this.flpAccounts.Size = new System.Drawing.Size(496, 557);
            this.flpAccounts.TabIndex = 6;
            this.flpAccounts.WrapContents = false;
            // 
            // btn_AccRefresh
            // 
            this.btn_AccRefresh.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn_AccRefresh.Location = new System.Drawing.Point(379, 66);
            this.btn_AccRefresh.Name = "btn_AccRefresh";
            this.btn_AccRefresh.Size = new System.Drawing.Size(129, 30);
            this.btn_AccRefresh.TabIndex = 14;
            this.btn_AccRefresh.Text = "Обновить";
            this.btn_AccRefresh.UseVisualStyleBackColor = true;
            this.btn_AccRefresh.Click += new System.EventHandler(this.btn_AccRefresh_Click);
            // 
            // Arh_AccountManagement
            // 
            this.ArahonText = "Arahon: Управление аккаунтами";
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 17F);
            this.ClientSize = new System.Drawing.Size(520, 671);
            this.Controls.Add(this.btn_AccRefresh);
            this.Controls.Add(label1);
            this.Controls.Add(this.flpAccounts);
            this.MinimumSize = new System.Drawing.Size(500, 600);
            this.Name = "Arh_AccountManagement";
            this.Opacity = 1D;
            this.Load += new System.EventHandler(this.Arh_AccountManagement_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpAccounts;
        private System.Windows.Forms.Button btn_AccRefresh;
    }
}
