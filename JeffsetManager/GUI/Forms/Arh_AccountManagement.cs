﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ManagerCore;

namespace JeffsetManager {
    public partial class Arh_AccountManagement : JeffsetManager.Arh_Dialog {
        public Arh_AccountManagement() {
            InitializeComponent();
        }

        private async void Arh_AccountManagement_Load(object sender, EventArgs e) {
            await RefreshAccounts();
        }

        private async System.Threading.Tasks.Task RefreshAccounts() {
            flpAccounts.Controls.Clear();
            flpAccounts.Controls.Add(GenInfoLabel("Загрузка списка аккаунтов...", Color.Orange));
            var accs = await JM.Core.QueryAllAccountsAsync();
            flpAccounts.Controls.Clear();
            foreach(var account in accs)
            {
                var fakeReq = JDRequest.CreateAccountVerification(account, account.License);
                JReqVerifyWidget w = new JReqVerifyWidget();
                w.Fake = true;
                w.Request = fakeReq;
                w.Width = flpAccounts.Width - 30;
                flpAccounts.Controls.Add(w);
            }
        }

        private async void btn_AccRefresh_Click(object sender, EventArgs e) {
            await RefreshAccounts();
        }
    }
}
