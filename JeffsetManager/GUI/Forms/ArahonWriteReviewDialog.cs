﻿using ManagerCore;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Net;
using System.Text;
using System.Windows.Forms;

namespace JeffsetManager
{
    public partial class Arh_WriteReviewDialog : JeffsetManager.Arh_Dialog
    {
        public Arh_WriteReviewDialog()
        {
            InitializeComponent();
        }

        private async void btn_Send_Click(object sender, EventArgs e)
        {
            #region Checks
            string Title = tb_Title.Text;
            string Text = tb_Text.Text;
            if (string.IsNullOrEmpty(Title))
            { _ErrorAnim(lbl_Title); return; }
            if (string.IsNullOrEmpty(Text))
            { _ErrorAnim(lbl_Text); return; }
            #endregion

            var last = btn_Send.Text; btn_Send.Text = "Посылка запроса...";
            try
            {
                await JM.Core.SendRequestAsync(JDRequest.CreateUserReport
                        (JM.Core.CurrentUser, Text, title: Title, 
                        rating: ratingWidget.Rating));
            }
            catch (WebException)
            {
                Arh_MessageBox.ShowMB($"Ошибка отправки запроса!",
                    "Arahon: Ошибка запроса", Arh_MessageBox.AMBType.Error);
                btn_Send.Text = last;
                return;
            }
            Arh_MessageBox.ShowMB($"Ваш отзыв/вопрос был успешно отправлен разработчику.",
                    "Arahon: Запрос отправлен", Arh_MessageBox.AMBType.Success);
            DialogResult = DialogResult.OK;
            Close();
        }
    }
}
