﻿using ManagerCore;
using System;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JeffsetManager
{
    public partial class FormMain : Arh_Dialog
    {

        public FormMain()
        {
            InitializeComponent();
            JM.Core.StateChanged += Manager_StateChanged;
            JM.Core.ReportProgress += Manager_ReportProgress;
            JM.Core.OperationReport += Manager_OperationReport;
            ArahonText += $" [{JM.Core.ManagerVersion}]";
        }

        private async Task FullRefreshSoftListAsync()
        {
            flpSoftware.Controls.Clear();
            var wl = GenInfoLabel("Загрузка списка публикаций... Подождите.", Color.Gray);
            flpSoftware.Controls.Add(wl);
            try
            {
                await JM.Core.RefreshAllSoftwareListAsync();
            }
            #region EXCEPT
            catch (WebException exc) when (exc.Status == WebExceptionStatus.NameResolutionFailure)
            {
                Arh_MessageBox.ShowMB($"Невозможно подключиться к серверу (интернет недоступен)!\r\n",
                    "Arahon: Ошибка соединения", Arh_MessageBox.AMBType.Error);
                Close();
            }
            catch (InvalidOperationException exc)
            {
                Arh_MessageBox.ShowMB($"{exc.GetType()}:\n\r{exc.Message}", "Arahon: Ошибка соединения", Arh_MessageBox.AMBType.Error);
                Close();
            }
            finally
            {
                flpSoftware.Controls.Clear();
                flpActions.Controls.Clear();
            }
            #endregion
            const int D = 30;
            if (JM.Core.IsInDeveloperMode)
            {
                Button addSoft = new Button();
                addSoft.FlatStyle = FlatStyle.Flat;
                addSoft.Margin = new Padding(0, 3, 0, 3);
                addSoft.Text = "Dev: + Опубликовать новый продукт";
                addSoft.Size = new Size(flpSoftware.Width - D, 35);
                addSoft.Click += AddSoft_Click;
                flpSoftware.Controls.Add(addSoft);
                flpSoftware.Controls.SetChildIndex(addSoft, 0);
                //
                //Button viewReqs = new Button();
                //viewReqs.FlatStyle = FlatStyle.Flat;
                //viewReqs.Margin = new Padding(0, 3, 0, 3);
                //viewReqs.Text = "Dev: Просмотреть запросы пользователей";
                //viewReqs.Size = new Size(flpSoftware.Width - D, 35);
                //viewReqs.Click += ViewReqs_Click;
                //flpSoftware.Controls.Add(viewReqs);
                //flpSoftware.Controls.SetChildIndex(viewReqs, 0);
            }
            var availableSoft = JM.Core.GetSoftwareForCurrentUser();
            availableSoft = availableSoft.OrderBy(s => s.ProductFriendlyName);
            foreach (var entry in availableSoft)
            {
                JeffsetSoftwareWidget w = new JeffsetSoftwareWidget();
                w.Width = flpSoftware.Width - D;
                w.Margin = new Padding(0, 3, 0, 3);
                w.RepresentedSoftware = entry;
                w.ActionPanel = flpActions;
                flpSoftware.Controls.Add(w);
            }
        }

        private async void AddSoft_Click(object sender, EventArgs e)
        {
            if (Arh_Dialog.BUSY_CONTROL(sender)) return;
            //<--------BUSY--CONTROL--------->
            var res = new Arh_SoftwareEdit().ShowDialog(this);
            if (res != DialogResult.OK) return;
            await FullRefreshSoftListAsync();
        }

        //private void ViewReqs_Click(object sender, EventArgs e) {

        //}

        private async void FormMain_Load(object sender, EventArgs e)
        {
            try
            {
                Visible = false;
                // Пробуем использовать "куки"-логин (если юзер уже в системе)
                var autoLoginRes = await JM.Core.CookieLoginAsync();
                // Если куки устарел или его нет, то пробуем логиниться явно
                if (autoLoginRes != JeffsetManagerCore.CookieLogInResult.OK
                    && !LogInExplicitly()) { Close(); return; } // Если нет, то валим с проги, все НЕ ОК.
                                                                // Если все ОК:
                else
                {
                    lbl_Name.Text = JM.Core.CurrentUser.Name;
                    lbl_Type.Text = JM.Core.CurrentUser.License.ToString();
                    btn_PublishSelf.Visible = JM.Core.IsInDeveloperMode;
                    var backTask1 = JM.Core.DownloadAvatarAsync(ms =>
                    {
                        Invoke((Action)(() =>
                        {
                            Arh_LoginDialog.InitAvatar(ms, base.BackColor);
                            pb_Avatar.Image = JM.Core.Avatar as Image;
                        }));
                    });
                    var backTask2 = FullRefreshSoftListAsync();
                }
                Visible = true;
            }
            #region EXCEPT
            catch (WebException exc) when (exc.Status == WebExceptionStatus.NameResolutionFailure)
            {
                Arh_MessageBox.ShowMB($"Невозможно подключиться к серверу!\r\n[Оффлайн режим пока недоступен]",
                    "Arahon: Ошибка соединения", Arh_MessageBox.AMBType.Error);
                Close();
            }
            catch (InvalidOperationException exc)
            {
                Arh_MessageBox.ShowMB($"{exc.GetType()}:\n\r{exc.Message}", "Arahon: Ошибка соединения", Arh_MessageBox.AMBType.Error);
                Close();
            }
            #endregion
        }

        private bool LogInExplicitly()
        {
            Arh_LoginDialog ld = new Arh_LoginDialog();
            return ld.ShowDialog() == DialogResult.OK;
        }

        private void Manager_OperationReport(JMOpRepState state, string name)
        {

            Invoke((Action)(() =>
            {
                if (state == JMOpRepState.Started)
                {
                    lbl_OpName.Text = name;
                    pbProgress.Style = ProgressBarStyle.Marquee;
                }
                else
                {
                    pbProgress.Style = ProgressBarStyle.Continuous;
                    pbProgress.Value = 0;
                }
            }));
        }

        private void Manager_ReportProgress(long bytesDone, long bytesTotal, int progress)
        {
            Invoke((Action)(() =>
            {
                pbProgress.Style = ProgressBarStyle.Continuous;
                pbProgress.Value = progress;
                if (progress == 100)
                    pbProgress.Value = 0;
            }));
        }

        private void Manager_StateChanged(JeffsetManagerState oldState, JeffsetManagerState newState)
        {
            Invoke((Action)(() =>
            {
                lblStatus.Text = newState.ToString();
                if (newState == JeffsetManagerState.Ready)
                    lbl_OpName.Text = "";
            }));
        }

        private void btnChangeAcc_Click(object sender, EventArgs e)
        {
            if (Arh_Dialog.BUSY_CONTROL(sender)) return;
            //<--------BUSY--CONTROL--------->
            JM.Core.LogOff();
            Application.Restart();
        }

        private void pb_Avatar_Click(object sender, EventArgs e)
        {
            if (Arh_Dialog.BUSY_CONTROL(sender)) return;
            //<--------BUSY--CONTROL--------->
            if (!JM.Core.IsLoggedIn) return;
            var chd = new Arh_AccSettingsDialog();
            chd.ArahonText = $"{JM.Core.CurrentUser.Credentials.UserName} - Редактировать";
            var res = chd.ShowDialog();
            if (!JM.Core.IsLoggedIn) return;
            lbl_Name.Text = JM.Core.CurrentUser.Name;
        }

        private void btn_DevMenu_Click(object sender, EventArgs e)
        {
            new Arh_DevMenu().ShowDialog(this);
        }

        private void FormMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!JM.Core.IsLoggedIn)
                return;
            string msg = "Вы хотите выйти из программы JM?\n\r(OK/(X)Escape)";
            if (JM.Core.IsBusy)
                msg = "Выполняется операция. Вы действительно хотите выйти?\n\r(OK/(X)Escape)";
            var res = Arh_MessageBox.ShowMB(msg, "Выход из JM", Arh_MessageBox.AMBType.Info);
            if (res != DialogResult.Cancel)
                return;
            e.Cancel = true;
            if (!JM.Core.IsLoggedIn)
                if (!LogInExplicitly())
                    Close();

        }

        private void btn_Refresh_Click(object sender, EventArgs e)
        {
            if (Arh_Dialog.BUSY_CONTROL(sender)) return;
            //<--------BUSY--CONTROL--------->
            var backTask = FullRefreshSoftListAsync();
        }

        private void btn_P2PTest_Click(object sender, EventArgs e)
        {
            new Arh_P2PPlugin().ShowDialog(this);
        }

        private void btnAbout_Click(object sender, EventArgs e)
        {
            new Arh_AboutBox().ShowDialog(this);
        }
    }
}
