﻿namespace JeffsetManager
{
    partial class Arh_LoginDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_OK = new System.Windows.Forms.Button();
            this.lbl_Pass = new System.Windows.Forms.Label();
            this.lbl_UserName = new System.Windows.Forms.Label();
            this.tb_Pass = new System.Windows.Forms.TextBox();
            this.tb_Name = new System.Windows.Forms.TextBox();
            this.btnSignUp = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btn_OK
            // 
            this.btn_OK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_OK.Location = new System.Drawing.Point(12, 229);
            this.btn_OK.Name = "btn_OK";
            this.btn_OK.Size = new System.Drawing.Size(314, 59);
            this.btn_OK.TabIndex = 2;
            this.btn_OK.Text = "Вход";
            this.btn_OK.UseVisualStyleBackColor = true;
            this.btn_OK.Click += new System.EventHandler(this.btn_OK_Click);
            // 
            // lbl_Pass
            // 
            this.lbl_Pass.AutoSize = true;
            this.lbl_Pass.Location = new System.Drawing.Point(12, 165);
            this.lbl_Pass.Name = "lbl_Pass";
            this.lbl_Pass.Size = new System.Drawing.Size(71, 17);
            this.lbl_Pass.TabIndex = 8;
            this.lbl_Pass.Text = "Пароль:";
            // 
            // lbl_UserName
            // 
            this.lbl_UserName.AutoSize = true;
            this.lbl_UserName.Location = new System.Drawing.Point(12, 111);
            this.lbl_UserName.Name = "lbl_UserName";
            this.lbl_UserName.Size = new System.Drawing.Size(62, 17);
            this.lbl_UserName.TabIndex = 8;
            this.lbl_UserName.Text = "Логин:";
            // 
            // tb_Pass
            // 
            this.tb_Pass.BackColor = System.Drawing.Color.Silver;
            this.tb_Pass.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_Pass.Location = new System.Drawing.Point(12, 185);
            this.tb_Pass.Name = "tb_Pass";
            this.tb_Pass.Size = new System.Drawing.Size(314, 17);
            this.tb_Pass.TabIndex = 1;
            this.tb_Pass.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tb_Pass.UseSystemPasswordChar = true;
            // 
            // tb_Name
            // 
            this.tb_Name.BackColor = System.Drawing.Color.DarkGray;
            this.tb_Name.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_Name.Location = new System.Drawing.Point(12, 131);
            this.tb_Name.Name = "tb_Name";
            this.tb_Name.Size = new System.Drawing.Size(314, 17);
            this.tb_Name.TabIndex = 0;
            this.tb_Name.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btnSignUp
            // 
            this.btnSignUp.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSignUp.ForeColor = System.Drawing.Color.Cyan;
            this.btnSignUp.Location = new System.Drawing.Point(12, 64);
            this.btnSignUp.Name = "btnSignUp";
            this.btnSignUp.Size = new System.Drawing.Size(314, 35);
            this.btnSignUp.TabIndex = 3;
            this.btnSignUp.Text = "Регистрация";
            this.btnSignUp.UseVisualStyleBackColor = true;
            this.btnSignUp.Click += new System.EventHandler(this.btnSignUp_Click);
            // 
            // Arh_LoginDialog
            // 
            this.AcceptButton = this.btn_OK;
            this.ArahonText = "Arahon: Вход в систему";
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(338, 311);
            this.Controls.Add(this.lbl_Pass);
            this.Controls.Add(this.lbl_UserName);
            this.Controls.Add(this.tb_Pass);
            this.Controls.Add(this.tb_Name);
            this.Controls.Add(this.btnSignUp);
            this.Controls.Add(this.btn_OK);
            this.Name = "Arh_LoginDialog";
            this.ShowInTaskbar = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Arahon: Вход в систему";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_OK;
        private System.Windows.Forms.TextBox tb_Name;
        private System.Windows.Forms.TextBox tb_Pass;
        private System.Windows.Forms.Label lbl_UserName;
        private System.Windows.Forms.Label lbl_Pass;
        private System.Windows.Forms.Button btnSignUp;
    }
}
