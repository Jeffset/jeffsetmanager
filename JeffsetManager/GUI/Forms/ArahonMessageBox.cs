﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace JeffsetManager
{
    public partial class Arh_MessageBox : JeffsetManager.Arh_Dialog
    {
        public Arh_MessageBox()
        {
            InitializeComponent();
        }
        public enum AMBType
        {
            Error, Warning, Info, Success
        }
        public static DialogResult ShowMB(string message, string caption = "", AMBType type = AMBType.Info)
        {
            Arh_MessageBox mb = new Arh_MessageBox();
            mb.ArahonText = "Arahon: " + caption;
            mb.lblText.Text = message;
            Color c = Color.White;
            switch (type)
            {
                case AMBType.Error:
                    c = Color.Red;
                    break;
                case AMBType.Warning:
                    c = Color.Yellow;
                    break;
                case AMBType.Info:
                    c = Color.Cyan;
                    break;
                case AMBType.Success:
                    c = Color.Lime;
                    break;
            }
            mb.ForeColor = c;
            DialogResult dr = mb.ShowDialog();
            return dr;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();
        }
    }
}
