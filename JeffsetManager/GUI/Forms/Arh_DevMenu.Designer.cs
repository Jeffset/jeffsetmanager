﻿namespace JeffsetManager {
    partial class Arh_DevMenu {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.btn_AccManage = new System.Windows.Forms.Button();
            this.btn_PublishSelf = new System.Windows.Forms.Button();
            this.btnViewReqs = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btn_AccManage
            // 
            this.btn_AccManage.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_AccManage.ForeColor = System.Drawing.Color.Yellow;
            this.btn_AccManage.Location = new System.Drawing.Point(51, 213);
            this.btn_AccManage.Name = "btn_AccManage";
            this.btn_AccManage.Size = new System.Drawing.Size(255, 53);
            this.btn_AccManage.TabIndex = 13;
            this.btn_AccManage.Text = "Управление аккаунтами";
            this.btn_AccManage.UseVisualStyleBackColor = true;
            this.btn_AccManage.Click += new System.EventHandler(this.btn_AccManage_Click);
            // 
            // btn_PublishSelf
            // 
            this.btn_PublishSelf.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_PublishSelf.ForeColor = System.Drawing.Color.Aqua;
            this.btn_PublishSelf.Location = new System.Drawing.Point(51, 75);
            this.btn_PublishSelf.Name = "btn_PublishSelf";
            this.btn_PublishSelf.Size = new System.Drawing.Size(255, 53);
            this.btn_PublishSelf.TabIndex = 13;
            this.btn_PublishSelf.Text = "Выгрузить текущую сборку JM в облако";
            this.btn_PublishSelf.UseVisualStyleBackColor = true;
            this.btn_PublishSelf.Click += new System.EventHandler(this.btn_PublishSelf_Click);
            // 
            // btnViewReqs
            // 
            this.btnViewReqs.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnViewReqs.Location = new System.Drawing.Point(51, 145);
            this.btnViewReqs.Name = "btnViewReqs";
            this.btnViewReqs.Size = new System.Drawing.Size(255, 53);
            this.btnViewReqs.TabIndex = 13;
            this.btnViewReqs.Text = "Просмотреть запросы пользователей";
            this.btnViewReqs.UseVisualStyleBackColor = true;
            this.btnViewReqs.Click += new System.EventHandler(this.btnViewReqs_Click);
            // 
            // Arh_DevMenu
            // 
            this.ArahonText = "Arahon: Меню разработчика";
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 17F);
            this.ClientSize = new System.Drawing.Size(348, 292);
            this.Controls.Add(this.btnViewReqs);
            this.Controls.Add(this.btn_AccManage);
            this.Controls.Add(this.btn_PublishSelf);
            this.Name = "Arh_DevMenu";
            this.Opacity = 1D;
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_PublishSelf;
        private System.Windows.Forms.Button btn_AccManage;
        private System.Windows.Forms.Button btnViewReqs;
    }
}
