﻿using ManagerCore;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace JeffsetManager {
    public partial class Arh_ViewDevReqsDialog : JeffsetManager.Arh_Dialog {
        public Arh_ViewDevReqsDialog() {
            InitializeComponent();
        }

        private void InitReqPanel(IEnumerable<JDRequest> requests) {
            const int D = 30;
            flp_Reqs.Controls.Clear();
            if(requests.Count() == 0) {
                var lbl = GenInfoLabel("<Запросов пока не поступало>", Color.DarkGray);
                flp_Reqs.Controls.Add(lbl);
            }
            foreach(var req in requests) {
                if(req.State != JDevRequestState.Waiting)
                    continue;
                switch(req.What) {
                    case JDevRequestType.LicenseVerify: {
                            JReqVerifyWidget w = new JReqVerifyWidget();
                            w.Request = req;
                            w.Width = flp_Reqs.Width - D;
                            flp_Reqs.Controls.Add(w);
                            //w.ReadOnly = true;
                        }
                        break;
                    case JDevRequestType.UserReport: {
                            FeedbackViewWidget w = new FeedbackViewWidget();
                            w.Request = req;
                            w.Width = flp_Reqs.Width - D;
                            flp_Reqs.Controls.Add(w);
                        }
                        break;
                    default:
                        continue;
                }
            }
        }

        private async void ViewReqsDialog_Load(object sender, EventArgs e) {
            var req = await JM.Core.GetDevRequestsAsync();
            InitReqPanel(req);
        }

        private void flp_Reqs_ControlRemoved(object sender, ControlEventArgs e) {
            if(flp_Reqs.Controls.Count != 0) return;
            //var lbl = GenInfoLabel("<Запросов больше нет>", Color.DarkGray);
            //flp_Reqs.Controls.Add(lbl);
        }
    }
}
