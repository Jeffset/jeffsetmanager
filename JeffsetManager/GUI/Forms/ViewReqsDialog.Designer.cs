﻿namespace JeffsetManager
{
    partial class Arh_ViewDevReqsDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Label lbl_reqs;
            this.flp_Reqs = new System.Windows.Forms.FlowLayoutPanel();
            lbl_reqs = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lbl_reqs
            // 
            lbl_reqs.AutoSize = true;
            lbl_reqs.Location = new System.Drawing.Point(9, 74);
            lbl_reqs.Name = "lbl_reqs";
            lbl_reqs.Size = new System.Drawing.Size(260, 17);
            lbl_reqs.TabIndex = 7;
            lbl_reqs.Text = "Список поступивших запросов:";
            // 
            // flp_Reqs
            // 
            this.flp_Reqs.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flp_Reqs.AutoScroll = true;
            this.flp_Reqs.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flp_Reqs.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flp_Reqs.Location = new System.Drawing.Point(12, 94);
            this.flp_Reqs.Margin = new System.Windows.Forms.Padding(0);
            this.flp_Reqs.Name = "flp_Reqs";
            this.flp_Reqs.Size = new System.Drawing.Size(506, 543);
            this.flp_Reqs.TabIndex = 6;
            this.flp_Reqs.WrapContents = false;
            this.flp_Reqs.ControlRemoved += new System.Windows.Forms.ControlEventHandler(this.flp_Reqs_ControlRemoved);
            // 
            // Arh_ViewDevReqsDialog
            // 
            this.ArahonText = "Arahon:  Работа с запросами пользователей";
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 17F);
            this.ClientSize = new System.Drawing.Size(511, 646);
            this.Controls.Add(lbl_reqs);
            this.Controls.Add(this.flp_Reqs);
            this.MinimumSize = new System.Drawing.Size(527, 400);
            this.Name = "Arh_ViewDevReqsDialog";
            this.Opacity = 1D;
            this.Load += new System.EventHandler(this.ViewReqsDialog_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flp_Reqs;
    }
}
