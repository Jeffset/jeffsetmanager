﻿namespace JeffsetManager
{
    partial class Arh_LicenseReqSend
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSendRequest = new System.Windows.Forms.Button();
            this.cb_UserLicense = new System.Windows.Forms.ComboBox();
            this.chb_Tester = new System.Windows.Forms.CheckBox();
            this.lbl_Lic = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnAction
            // 
            this.btnSendRequest.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSendRequest.Location = new System.Drawing.Point(14, 200);
            this.btnSendRequest.Name = "btnAction";
            this.btnSendRequest.Size = new System.Drawing.Size(354, 44);
            this.btnSendRequest.TabIndex = 236;
            this.btnSendRequest.Text = "Запросить";
            this.btnSendRequest.UseVisualStyleBackColor = true;
            this.btnSendRequest.Click += new System.EventHandler(this.btnSendRequest_Click);
            // 
            // cb_UserLicense
            // 
            this.cb_UserLicense.BackColor = System.Drawing.Color.Gainsboro;
            this.cb_UserLicense.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_UserLicense.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb_UserLicense.ForeColor = System.Drawing.Color.Black;
            this.cb_UserLicense.FormattingEnabled = true;
            this.cb_UserLicense.Items.AddRange(new object[] {
            "Free",
            "Standard",
            "Extended",
            "Developer"});
            this.cb_UserLicense.Location = new System.Drawing.Point(43, 97);
            this.cb_UserLicense.Name = "cb_UserLicense";
            this.cb_UserLicense.Size = new System.Drawing.Size(294, 25);
            this.cb_UserLicense.TabIndex = 235;
            // 
            // chb_Tester
            // 
            this.chb_Tester.AutoSize = true;
            this.chb_Tester.ForeColor = System.Drawing.Color.MediumSpringGreen;
            this.chb_Tester.Location = new System.Drawing.Point(43, 128);
            this.chb_Tester.Name = "chb_Tester";
            this.chb_Tester.Size = new System.Drawing.Size(216, 21);
            this.chb_Tester.TabIndex = 234;
            this.chb_Tester.Text = "Лицензия тестировщика";
            this.chb_Tester.UseVisualStyleBackColor = true;
            // 
            // lbl_Lic
            // 
            this.lbl_Lic.AutoSize = true;
            this.lbl_Lic.ForeColor = System.Drawing.Color.MediumSpringGreen;
            this.lbl_Lic.Location = new System.Drawing.Point(46, 76);
            this.lbl_Lic.Name = "lbl_Lic";
            this.lbl_Lic.Size = new System.Drawing.Size(278, 17);
            this.lbl_Lic.TabIndex = 233;
            this.lbl_Lic.Text = "Запрашиваемая польз. лицензия:";
            // 
            // Arh_LicenseReqSend
            // 
            this.AcceptButton = this.btnSendRequest;
            this.ArahonText = "Arahon: Запрос лицензии";
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 17F);
            this.ClientSize = new System.Drawing.Size(380, 256);
            this.Controls.Add(this.btnSendRequest);
            this.Controls.Add(this.cb_UserLicense);
            this.Controls.Add(this.chb_Tester);
            this.Controls.Add(this.lbl_Lic);
            this.Name = "Arh_LicenseReqSend";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cb_UserLicense;
        private System.Windows.Forms.CheckBox chb_Tester;
        private System.Windows.Forms.Button btnSendRequest;
        private System.Windows.Forms.Label lbl_Lic;
    }
}
