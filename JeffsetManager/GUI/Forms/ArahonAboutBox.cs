﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ManagerCore;

namespace JeffsetManager {
    public partial class Arh_AboutBox : JeffsetManager.Arh_Dialog {
        public Arh_AboutBox() {
            InitializeComponent();
        }

        private void lnkVk_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e) {
            Process.Start(@"http://vk.com/giffist");
        }

        private void Arh_AboutBox_Load(object sender, EventArgs e) {
            lblVersion.Text = string.Format(lblVersion.Text, JM.Core.ManagerVersion);
        }

        private void lnkFeedback_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e) {
            var feedBack = new Arh_WriteReviewDialog();
            feedBack.ShowDialog();
        }
    }
}
