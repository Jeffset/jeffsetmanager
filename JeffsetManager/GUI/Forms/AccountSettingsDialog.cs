﻿using ManagerCore;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Linq;
using System.Threading.Tasks;
using System.Net;

namespace JeffsetManager
{
    public partial class Arh_AccSettingsDialog : JeffsetManager.Arh_Dialog
    {

        public Arh_AccSettingsDialog()
        {
            InitializeComponent();
            string licText = "";
            lbl_License.Text = JM.Core.CurrentUser.License.ToString();
            switch (JM.Core.CurrentUser.License.UserLicense)
            {
                case JeffsetUserType.Unverified:
                    licText = "ОЗНАКОМИТЕЛЬНЫЙ РЕЖИМ\n\rВаша учетная запись еще не подтверждена.";
                    lbl_LicInfo.ForeColor = Color.Salmon;
                    btn_LicExtend.Enabled = false;
                    break;
                case JeffsetUserType.Free:
                case JeffsetUserType.Standard:
                case JeffsetUserType.Extended:
                    licText = lbl_LicInfo.Text;
                    break;
                case JeffsetUserType.Developer:
                    licText = "РЕЖИМ РАЗРАБОТЧИКА\n\rПриветствую, мастер!";
                    btn_LicExtend.Enabled = false;
                    break;
                default:
                    licText = "Ошибка лицензии!";
                    break;
            }
            lbl_LicInfo.Text = licText;
        }


        JeffsetUserCredentials newCredent;
        private bool CheckSettings()
        {
            var cred = JM.Core.CurrentUser.Credentials;
            var newc = new JeffsetUserCredentials(cred.UserName, tb_NewPs.Text);

            if (tb_OldPs.Text == "" && tb_NewPs.Text == "" && tb_DupNewPs.Text == "")
            {
                newCredent = cred; return true;
            }

            if (tb_OldPs.Text != cred.Password)
                return _ErrorAnim(lbl_OldPs);
            if (!newc.CheckValidity(true))
                return _ErrorAnim(lbl_NewPs);
            if (newc.Password != tb_DupNewPs.Text)
                return _ErrorAnim(lbl_DupNewPs);

            newCredent = newc;
            return true;
        }

        private async void btn_Accept_Click(object sender, EventArgs e)
        {
            if (Arh_Dialog.BUSY_CONTROL(sender)) return;
            //<--------BUSY--CONTROL--------->
            if (!CheckSettings())
                return;
            JM.Core.CurrentUser.Credentials = newCredent;

            JM.Core.CurrentUser.Name = tb_Name.Text;
            btn_Accept.Text = "Применение изменений...";
            try
            {
                await JM.Core.ChangeAccountSettingsAsync();
            }
            catch (WebException)
            {
                _ErrorAnim(btn_Accept);
                btn_Accept.Text = "Применить";
                return;
            }
            DialogResult = DialogResult.OK;
            Close();
        }

        private async void AccountSettingsDialog_Load(object sender, EventArgs e)
        {
            pb_Avatar.Image = JM.Core.Avatar as Image;
            tb_Name.Text = JM.Core.CurrentUser.Name;
            //
            try
            {
                await LoadLicenseRequestAsync();
            }
            #region EXCEPT
            catch (WebException)
            {
                Arh_MessageBox.ShowMB("Ошибка загрузки запросов лицензии!", "Arahon: Ошибка загрузки", Arh_MessageBox.AMBType.Error);
                Close();
            }
            #endregion
        }

        JDRequest LicenceRequest = null;
        private async Task LoadLicenseRequestAsync()
        {
            var reqs = await JM.Core.GetCurrentUserRequestAsync(JDevRequestType.LicenseVerify);
            if (reqs.Count() == 0)
            {
                LicenceRequest = null;
                if (!JM.Core.IsInDeveloperMode)
                {
                    lbl_LicInfo.Text = "Для расширения лицензии необходимо послать запрос";
                    lbl_LicInfo.ForeColor = Color.Silver; 
                }
                return;
            }
            else LicenceRequest = reqs.First();
            switch (LicenceRequest.State)
            {
                case JDevRequestState.Waiting:
                    if (JM.Core.CurrentUser.License.UserLicense == JeffsetUserType.Unverified)
                    {
                        lbl_LicInfo.Text =
                        string.Format("Ваш аккаунт еще не подтвержден. Ожидаемая лицензия:\n\r {0}.", LicenceRequest.Data["LICENSE"]);
                        lbl_LicInfo.ForeColor = Color.Salmon;
                    }
                    else
                    {
                        lbl_LicInfo.Text =
                        string.Format("Послан запрос на расширение лицензии:\n\r {0}.", LicenceRequest.Data["LICENSE"]);
                        lbl_LicInfo.ForeColor = Color.Cyan;
                    }
                    break;
                case JDevRequestState.Accepted:
                    lbl_LicInfo.Text =
                    string.Format("Запрос на расширение лицензии {0} удовлетоврен.", LicenceRequest.Data["LICENSE"]);
                    lbl_LicInfo.ForeColor = Color.Lime;
                    await JM.Core.RemoveRequestAsync(LicenceRequest);
                    LicenceRequest = null;
                    break;
                case JDevRequestState.Declined:
                    lbl_LicInfo.Text =
                    string.Format("Запрос на расширение лицензии {0} был отклонен.", LicenceRequest.Data["LICENSE"]);
                    lbl_LicInfo.ForeColor = Color.OrangeRed;
                    await JM.Core.RemoveRequestAsync(LicenceRequest);
                    LicenceRequest = null;
                    break;
                default:
                    break;
            }
        }

        private void btnLogOff_Click(object sender, EventArgs e)
        {
            JM.Core.LogOff();
            Application.Restart();
        }

        private async void btn_LicExtend_Click(object sender, EventArgs e)
        {
            if (Arh_Dialog.BUSY_CONTROL(sender)) return;
            //<--------BUSY--CONTROL--------->
            var res = new Arh_LicenseReqSend(LicenceRequest).ShowDialog();
            await LoadLicenseRequestAsync();
        }
    }
}
