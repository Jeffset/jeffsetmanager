﻿using ManagerCore;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Net;

namespace JeffsetManager
{
    public partial class Arh_LoginDialog : JeffsetManager.Arh_Dialog
    {
        public string UserName
        {
            get { return tb_Name.Text; }
            set { tb_Name.Text = value; }
        }

        public string Password
        {
            get { return tb_Pass.Text; }
            set { tb_Pass.Text = value; }
        }

        public Arh_LoginDialog()
        {
            InitializeComponent();
        }

        public static void InitAvatar(Stream ms, Color bc)
        {
            if (ms == null)
            {
                JM.Core.Avatar = null;
                return;
            }
            var bmp = Image.FromStream(ms);
            Bitmap back = new Bitmap(150, 150);
            var g = Graphics.FromImage(back);
            g.FillRectangle(new SolidBrush(bc), new Rectangle(0, 0, 150, 150));
            g.DrawImage(bmp, 0, 0, 150, 150);
            g.Dispose();
            bmp.Dispose();
            JM.Core.Avatar = back;
        }

        private async void btn_OK_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(UserName))
            { _ErrorAnim(lbl_UserName); return; }
            if (string.IsNullOrWhiteSpace(Password))
            { _ErrorAnim(lbl_Pass); return; }
            btn_OK.Text = "Вход в систему...";
            try {
                var loginRes = await JM.Core.LogInAsync(UserName, Password);
                switch(loginRes) {
                    case JeffsetManagerCore.LogInResult.OK:
                        DialogResult = DialogResult.OK;
                        Close();
                        break;
                    case JeffsetManagerCore.LogInResult.WrongName:
                        _ErrorAnim(lbl_UserName);
                        btn_OK.Text = "Вход";
                        break;
                    case JeffsetManagerCore.LogInResult.WrongPassword:
                        _ErrorAnim(lbl_Pass);
                        btn_OK.Text = "Вход";
                        break;
                }
            } catch(WebException ex) when (ex.Status == WebExceptionStatus.NameResolutionFailure){
                Arh_MessageBox.ShowMB("Ошибка соединения! Проверьте подключение к интернету.",
                    type: Arh_MessageBox.AMBType.Error);
            }
            catch (WebException) {
                Arh_MessageBox.ShowMB("Неожиданная ошибка соединения! Невозможно войти!",
                    type: Arh_MessageBox.AMBType.Error);
            } catch(Exception ex) {
                Arh_MessageBox.ShowMB($"Неожиданная ошибка! Невозможно войти!\r\ninfo:{ex.Message}",
                    type: Arh_MessageBox.AMBType.Error);
            }
        }

        private void btnSignUp_Click(object sender, EventArgs e)
        {
            var regDialog = new Arh_AccRegDialog();
            Visible = false;
            var res = regDialog.ShowDialog();
            if (res == DialogResult.OK)
            {
                UserName = regDialog.Login;
                Password = regDialog.Password;
            }
            Visible = true;
            Focus();
        }
    }
}
