﻿namespace JeffsetManager
{
    partial class Arh_AccRegDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnAccept = new System.Windows.Forms.Button();
            this.cb_UserLicense = new System.Windows.Forms.ComboBox();
            this.chb_Tester = new System.Windows.Forms.CheckBox();
            this.lbl_UsrLic = new System.Windows.Forms.Label();
            this.lbl_RepPass = new System.Windows.Forms.Label();
            this.lbl_Pass = new System.Windows.Forms.Label();
            this.lbl_Login = new System.Windows.Forms.Label();
            this.lbl_Name = new System.Windows.Forms.Label();
            this.tb_RepPass = new System.Windows.Forms.TextBox();
            this.tb_Pass = new System.Windows.Forms.TextBox();
            this.tb_Login = new System.Windows.Forms.TextBox();
            this.tb_Name = new System.Windows.Forms.TextBox();
            this.pbAva = new System.Windows.Forms.PictureBox();
            this.chooseAva = new System.Windows.Forms.OpenFileDialog();
            this.lbl_Avatar = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pbAva)).BeginInit();
            this.SuspendLayout();
            // 
            // btnAccept
            // 
            this.btnAccept.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAccept.Location = new System.Drawing.Point(12, 534);
            this.btnAccept.Name = "btnAccept";
            this.btnAccept.Size = new System.Drawing.Size(310, 38);
            this.btnAccept.TabIndex = 228;
            this.btnAccept.Text = "Зарегистрировать";
            this.btnAccept.UseVisualStyleBackColor = true;
            this.btnAccept.Click += new System.EventHandler(this.btnAccept_Click);
            // 
            // cb_UserLicense
            // 
            this.cb_UserLicense.BackColor = System.Drawing.Color.Gray;
            this.cb_UserLicense.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_UserLicense.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb_UserLicense.FormattingEnabled = true;
            this.cb_UserLicense.Items.AddRange(new object[] {
            "Free",
            "Standard",
            "Extended",
            "Developer"});
            this.cb_UserLicense.Location = new System.Drawing.Point(16, 456);
            this.cb_UserLicense.Name = "cb_UserLicense";
            this.cb_UserLicense.Size = new System.Drawing.Size(305, 25);
            this.cb_UserLicense.TabIndex = 4;
            // 
            // chb_Tester
            // 
            this.chb_Tester.AutoSize = true;
            this.chb_Tester.Location = new System.Drawing.Point(11, 487);
            this.chb_Tester.Name = "chb_Tester";
            this.chb_Tester.Size = new System.Drawing.Size(261, 21);
            this.chb_Tester.TabIndex = 232;
            this.chb_Tester.Text = "Запросить лицензию тестера";
            this.chb_Tester.UseVisualStyleBackColor = true;
            // 
            // lbl_UsrLic
            // 
            this.lbl_UsrLic.AutoSize = true;
            this.lbl_UsrLic.Location = new System.Drawing.Point(12, 436);
            this.lbl_UsrLic.Name = "lbl_UsrLic";
            this.lbl_UsrLic.Size = new System.Drawing.Size(305, 17);
            this.lbl_UsrLic.TabIndex = 231;
            this.lbl_UsrLic.Text = "Запрос пользовательской лицензии:";
            // 
            // lbl_RepPass
            // 
            this.lbl_RepPass.AutoSize = true;
            this.lbl_RepPass.Location = new System.Drawing.Point(12, 365);
            this.lbl_RepPass.Name = "lbl_RepPass";
            this.lbl_RepPass.Size = new System.Drawing.Size(161, 17);
            this.lbl_RepPass.TabIndex = 8;
            this.lbl_RepPass.Text = "Повторить пароль:";
            // 
            // lbl_Pass
            // 
            this.lbl_Pass.AutoSize = true;
            this.lbl_Pass.Location = new System.Drawing.Point(12, 322);
            this.lbl_Pass.Name = "lbl_Pass";
            this.lbl_Pass.Size = new System.Drawing.Size(269, 17);
            this.lbl_Pass.TabIndex = 8;
            this.lbl_Pass.Text = "Пароль (не менее 5 символов):";
            // 
            // lbl_Login
            // 
            this.lbl_Login.AutoSize = true;
            this.lbl_Login.Location = new System.Drawing.Point(11, 270);
            this.lbl_Login.Name = "lbl_Login";
            this.lbl_Login.Size = new System.Drawing.Size(179, 17);
            this.lbl_Login.TabIndex = 8;
            this.lbl_Login.Text = "Логин (уникальный):";
            // 
            // lbl_Name
            // 
            this.lbl_Name.AutoSize = true;
            this.lbl_Name.Location = new System.Drawing.Point(12, 217);
            this.lbl_Name.Name = "lbl_Name";
            this.lbl_Name.Size = new System.Drawing.Size(89, 17);
            this.lbl_Name.TabIndex = 8;
            this.lbl_Name.Text = "Ваше имя:";
            // 
            // tb_RepPass
            // 
            this.tb_RepPass.BackColor = System.Drawing.Color.Silver;
            this.tb_RepPass.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_RepPass.Location = new System.Drawing.Point(12, 385);
            this.tb_RepPass.Name = "tb_RepPass";
            this.tb_RepPass.Size = new System.Drawing.Size(310, 17);
            this.tb_RepPass.TabIndex = 3;
            this.tb_RepPass.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tb_RepPass.UseSystemPasswordChar = true;
            // 
            // tb_Pass
            // 
            this.tb_Pass.BackColor = System.Drawing.Color.Silver;
            this.tb_Pass.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_Pass.Location = new System.Drawing.Point(12, 342);
            this.tb_Pass.Name = "tb_Pass";
            this.tb_Pass.Size = new System.Drawing.Size(310, 17);
            this.tb_Pass.TabIndex = 2;
            this.tb_Pass.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tb_Pass.UseSystemPasswordChar = true;
            // 
            // tb_Login
            // 
            this.tb_Login.BackColor = System.Drawing.Color.Silver;
            this.tb_Login.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_Login.Location = new System.Drawing.Point(11, 290);
            this.tb_Login.Name = "tb_Login";
            this.tb_Login.Size = new System.Drawing.Size(310, 17);
            this.tb_Login.TabIndex = 1;
            this.tb_Login.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tb_Name
            // 
            this.tb_Name.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.tb_Name.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_Name.Location = new System.Drawing.Point(12, 237);
            this.tb_Name.Name = "tb_Name";
            this.tb_Name.Size = new System.Drawing.Size(310, 17);
            this.tb_Name.TabIndex = 0;
            this.tb_Name.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // pbAva
            // 
            this.pbAva.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbAva.Image = global::JeffsetManager.Properties.Resources.defaultAva;
            this.pbAva.Location = new System.Drawing.Point(105, 64);
            this.pbAva.Name = "pbAva";
            this.pbAva.Size = new System.Drawing.Size(120, 120);
            this.pbAva.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbAva.TabIndex = 233;
            this.pbAva.TabStop = false;
            this.pbAva.Click += new System.EventHandler(this.pbAva_Click);
            // 
            // chooseAva
            // 
            this.chooseAva.Filter = "Изображения|*.jpg; *.gif; *.png";
            this.chooseAva.FilterIndex = 3;
            // 
            // lbl_Avatar
            // 
            this.lbl_Avatar.ForeColor = System.Drawing.Color.Silver;
            this.lbl_Avatar.Location = new System.Drawing.Point(105, 187);
            this.lbl_Avatar.Name = "lbl_Avatar";
            this.lbl_Avatar.Size = new System.Drawing.Size(120, 17);
            this.lbl_Avatar.TabIndex = 8;
            this.lbl_Avatar.Text = "Аватар";
            this.lbl_Avatar.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // AccountRegisterDialog
            // 
            this.AcceptButton = this.btnAccept;
            this.ArahonText = "Arahon: Регистрация аккаунта";
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 17F);
            this.ClientSize = new System.Drawing.Size(334, 584);
            this.Controls.Add(this.pbAva);
            this.Controls.Add(this.cb_UserLicense);
            this.Controls.Add(this.chb_Tester);
            this.Controls.Add(this.lbl_UsrLic);
            this.Controls.Add(this.lbl_RepPass);
            this.Controls.Add(this.lbl_Pass);
            this.Controls.Add(this.lbl_Login);
            this.Controls.Add(this.lbl_Avatar);
            this.Controls.Add(this.lbl_Name);
            this.Controls.Add(this.tb_RepPass);
            this.Controls.Add(this.tb_Pass);
            this.Controls.Add(this.tb_Login);
            this.Controls.Add(this.tb_Name);
            this.Controls.Add(this.btnAccept);
            this.Name = "AccountRegisterDialog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            ((System.ComponentModel.ISupportInitialize)(this.pbAva)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnAccept;
        private System.Windows.Forms.TextBox tb_Name;
        private System.Windows.Forms.Label lbl_Name;
        private System.Windows.Forms.TextBox tb_Login;
        private System.Windows.Forms.Label lbl_Login;
        private System.Windows.Forms.TextBox tb_Pass;
        private System.Windows.Forms.Label lbl_Pass;
        private System.Windows.Forms.TextBox tb_RepPass;
        private System.Windows.Forms.Label lbl_RepPass;
        private System.Windows.Forms.CheckBox chb_Tester;
        private System.Windows.Forms.ComboBox cb_UserLicense;
        private System.Windows.Forms.Label lbl_UsrLic;
        private System.Windows.Forms.PictureBox pbAva;
        private System.Windows.Forms.OpenFileDialog chooseAva;
        private System.Windows.Forms.Label lbl_Avatar;
    }
}
