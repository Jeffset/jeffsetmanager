﻿using ManagerCore;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Net;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;

namespace JeffsetManager {
    public partial class Arh_SoftwareEdit : JeffsetManager.Arh_Dialog {
        int packSize = 0;
        private JeffsetSoftware software;

        public JeffsetSoftware Software {
            get { return software; }
            set {
                software = value;
                tb_InternalName.Text = value.ProductInternalName;
                tb_FriendlyName.Text = value.ProductFriendlyName;
                tb_CurVer.Text = value.CurrentVersion.ToString();
                tb_CurVer.ReadOnly = true;
                tb_NewVersion.Text = tb_CurVer.Text; // default
                tb_MinVer.Text = value.MinYetSupportedVersion.ToString();
                cb_UserLicense.Text = value.License.UserLicense.ToString();
                chb_Tester.Checked = value.License.TesterLicense;
                lbl_PackSize.Text =
                    string.Format("Размер пакета (в облаке): {0} байт", value.AdditInfo.DownloadPackageSize);
                if(value.AdditInfo.IconBitmap != null)
                    value.AdditInfo.IconBitmap.UpdateImage((s) => { pb_Icon.Image = Image.FromStream(s); });
                if(value.AdditInfo.BannerBitmap != null)
                    value.AdditInfo.BannerBitmap.UpdateImage((s) => { pb_Banner.Image = Image.FromStream(s); });
                tb_Descr.Text = value.PublicDescription;
            }
        }

        public Arh_SoftwareEdit() {
            InitializeComponent();
        }

        private async void btn_Publish_Click(object sender, EventArgs e) {
            if(Arh_Dialog.BUSY_CONTROL(sender)) return;
            //<--------BUSY--CONTROL--------->

            #region Checks
            bool correct = true;

            string intName = tb_InternalName.Text;
            if(intName == "") { _ErrorAnim(lbl_IntName); return; }

            string frName = tb_FriendlyName.Text;
            if(frName == "") { _ErrorAnim(lbl_FriendName); return; }

            JeffsetUserType userLic;
            correct = Enum.TryParse(cb_UserLicense.Text, out userLic);
            if(!correct) { _ErrorAnim(lbl_UserLic); return; }

            Version curVer, newVer, minVer;
            correct &= Version.TryParse(tb_CurVer.Text, out curVer);
            correct &= Version.TryParse(tb_NewVersion.Text, out newVer);
            correct &= Version.TryParse(tb_MinVer.Text, out minVer);
            if(!correct) { _ErrorAnim(lbl_Ver); return; }
            correct = newVer >= curVer && newVer >= minVer;
            if(!correct) { _ErrorAnim(lbl_Ver); return; }

            if(!tb_InternalName.ReadOnly && !packageSelected) { _ErrorAnim(btn_Package); return; }

            string description = tb_Descr.Text;
            if(string.IsNullOrWhiteSpace(description)) { _ErrorAnim(lbl_Descr); return; }

            #endregion

            MemoryStream msi = null;
            if(iconSelected) {
                msi = new MemoryStream();
                using(var fs = File.OpenRead(chooseIcon.FileName))
                    fs.CopyTo(msi);
                msi.Position = 0;
            }
            MemoryStream msb = null;
            if(bannerSelected) {
                msb = new MemoryStream();
                using(var fs = File.OpenRead(chooseBanner.FileName))
                    fs.CopyTo(msb);
                msb.Position = 0;
            }
            JeffsetLicence licence = new JeffsetLicence {
                TesterLicense = chb_Tester.Checked,
                UserLicense = userLic
            };

            var _software =
                new JeffsetSoftware(intName, frName, newVer, minVer, description, licence,
                new JeffsetSoftwareAdditionInfo(packSize, new JMCloudImage(msi), new JMCloudImage(msb)));

            var last = btn_Accept.Text; btn_Accept.Text = "Выгрузка...";
            try {
                await JM.Core.PublishSoftwareVersionAsync(_software, packageSelected ? choosePackage.FileName : "");
            }
            #region EXCEPT
            catch(WebException) {
                Arh_MessageBox.ShowMB($"Ошибка выгрузки продукта \"{intName}\"!",
                    "Arahon: Ошибка публикации", Arh_MessageBox.AMBType.Error);
                btn_Accept.Text = last;
                return;
            } catch(Exception ex) {
                Arh_MessageBox.ShowMB($"Неожиданная ошибка [{ex}] выгрузки продукта \"{intName}\"!",
                    "Arahon: Ошибка публикации", Arh_MessageBox.AMBType.Error);
                btn_Accept.Text = last;
                return;
            }
            #endregion
            Arh_MessageBox.ShowMB(string.Format("Продукт \"{0}\" был успешно {1}.)", frName,
                tb_InternalName.ReadOnly ? "обновлен" : "опубликован"));
            DialogResult = DialogResult.OK;
            Close();
        }

        private bool packageSelected = false;
        private void btn_Package_Click(object sender, EventArgs e) {
            var r = choosePackage.ShowDialog();
            if(r == DialogResult.OK) {
                _OnPackageSelected();
            }
        }

        private void _OnPackageSelected() {
            btn_Package.Text = choosePackage.FileName;
            FileInfo fi = new FileInfo(choosePackage.FileName);
            lbl_PackSize.Text = string.Format("Размер пакета: {0} байт", fi.Length);
            packSize = (int)fi.Length;
            packageSelected = true;
        }

        private void ArahonProductEdit_Shown(object sender, EventArgs e) {
            if(Software == null)// если новый софт нужно выложить
            {
                gr_Common.Enabled = true;
                ArahonText = "Arahon: Опубликовать новый продукт";
            }
            else // если редактировать или обновить
            {

            }
        }

        private bool bannerSelected = false;
        private void btn_Banner_Click(object sender, EventArgs e) {
            var res = chooseBanner.ShowDialog();
            if(res == DialogResult.OK) {
                btn_Banner.Text = "баннер установлен";
                pb_Banner.Image = Image.FromFile(chooseBanner.FileName);
                bannerSelected = true;
            }
        }
        private bool iconSelected = false;
        private void btn_Icon_Click(object sender, EventArgs e) {
            var res = chooseIcon.ShowDialog();
            if(res == DialogResult.OK) {
                btn_Icon.Text = "выбрана";
                pb_Icon.Image = Image.FromFile(chooseIcon.FileName);
                iconSelected = true;
            }
        }

        private void btn_UpdateMode_Click(object sender, EventArgs e) {
            if(Software != null) return;
            if(string.IsNullOrWhiteSpace(tb_InternalName.Text)) { _ErrorAnim(lbl_IntName); return; }
            var reqSoft = JM.Core.AllSoftware.Find((s) => s.ProductInternalName == tb_InternalName.Text);
            if(reqSoft == null) {
                Action a = () => {
                    Arh_MessageBox.ShowMB("Продукт с таким именем не зарегистрирован в системе.",
                  "Внутреннее имя не найдено", Arh_MessageBox.AMBType.Warning);
                }; Owner.Invoke(a);
                return;
            }
            Software = reqSoft;
            btn_UpdateMode.Text = "Продукт будет обновлен";
            tb_InternalName.ReadOnly = true;
            btn_Package.Text = "-Не изменять установочный пакет-";
            btn_Icon.Text = "- та же -";
            btn_Banner.Text = "- тот же баннер -";
        }

        private void tb_NewVersion_TextChanged(object sender, EventArgs e) {
            if(!tb_InternalName.ReadOnly) // Новый софт
                tb_CurVer.Text = tb_NewVersion.Text;
        }

        private void btn_PackFolder_Click(object sender, EventArgs e) {
            var res = folderBrowserDialog.ShowDialog();
            if(res == DialogResult.Cancel) return;

            string path = folderBrowserDialog.SelectedPath;
            Environment.CurrentDirectory = path;

            try {

                var packFolder = new DirectoryInfo(path);
                var exes = packFolder.EnumerateFiles("*.exe");

                if(exes.Count() == 1 && exes.First().Name == "package.exe") {
                    res = Arh_MessageBox.ShowMB("Каталог уже содержит установщик package.exe. Он будет переписан. Продолжить?",
                "Создание пакета-установщика", Arh_MessageBox.AMBType.Info);
                    if(res != DialogResult.OK) return;
                    exes.First().Delete();
                    //exes = packFolder.EnumerateFiles();
                }
                if(exes.Count() == 0) {
                    res = Arh_MessageBox.ShowMB("Каталог не содержит исполняемых файлов! Продолжить?",
                "Создание пакета-установщика", Arh_MessageBox.AMBType.Warning);
                    if(res != DialogResult.OK) return;
                }

                string archiever = $@"{Application.StartupPath}\Rar.exe";
                string args =
                $@"-x*.sdf -x*.suo -x*.pdb -x*.ilk -x*.tlog a -r -m5 -ma5 -md256m -sfxWinCon.SFX package.rar .\";
                var Archiever = Process.Start(archiever, args);
                Archiever.WaitForExit();
                if(Archiever.ExitCode != 0)
                    throw new InvalidOperationException(Archiever.ExitCode.ToString());
                choosePackage.FileName = path + @"\package.exe";

                Arh_MessageBox.ShowMB("Каталог успешно запакован в установщик",
                "Создание пакета-установщика", Arh_MessageBox.AMBType.Success);

                _OnPackageSelected();
                exes = from exe in exes where exe.Name != "package.exe" select exe;
                if(!tb_InternalName.ReadOnly) {
                    if((tb_InternalName.Text = Path.GetFileNameWithoutExtension(
                        exes.FirstOrDefault()?.Name ?? "")) == "")
                        tb_InternalName.Text = Path.GetFileName(path); 
                }

            } catch(InvalidOperationException) {
                Arh_MessageBox.ShowMB("Ошибка сжатия каталога!",
                 "Сжатие", Arh_MessageBox.AMBType.Error);

            } finally {
                Environment.CurrentDirectory = Application.StartupPath;
            }
        }
    }
}
