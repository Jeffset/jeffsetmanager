﻿namespace JeffsetManager
{
    partial class Arh_SoftwareEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.choosePackage = new System.Windows.Forms.OpenFileDialog();
            this.chooseIcon = new System.Windows.Forms.OpenFileDialog();
            this.chooseBanner = new System.Windows.Forms.OpenFileDialog();
            this.btn_Accept = new System.Windows.Forms.Button();
            this.gr_Public = new System.Windows.Forms.GroupBox();
            this.pb_Banner = new System.Windows.Forms.PictureBox();
            this.tb_Descr = new System.Windows.Forms.TextBox();
            this.btn_Icon = new System.Windows.Forms.Button();
            this.btn_Banner = new System.Windows.Forms.Button();
            this.lbl_Descr = new System.Windows.Forms.Label();
            this.pb_Icon = new System.Windows.Forms.PictureBox();
            this.gr_Common = new System.Windows.Forms.GroupBox();
            this.chb_Tester = new System.Windows.Forms.CheckBox();
            this.cb_UserLicense = new System.Windows.Forms.ComboBox();
            this.lbl_UserLic = new System.Windows.Forms.Label();
            this.lbl_FriendName = new System.Windows.Forms.Label();
            this.lbl_Ver = new System.Windows.Forms.Label();
            this.tb_FriendlyName = new System.Windows.Forms.TextBox();
            this.tb_MinVer = new System.Windows.Forms.TextBox();
            this.tb_CurVer = new System.Windows.Forms.TextBox();
            this.tb_NewVersion = new System.Windows.Forms.TextBox();
            this.btn_PackFolder = new System.Windows.Forms.Button();
            this.btn_Package = new System.Windows.Forms.Button();
            this.lbl_Package = new System.Windows.Forms.Label();
            this.lbl_PackSize = new System.Windows.Forms.Label();
            this.lbl_IntName = new System.Windows.Forms.Label();
            this.tb_InternalName = new System.Windows.Forms.TextBox();
            this.btn_UpdateMode = new System.Windows.Forms.Button();
            this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.gr_Public.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Banner)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Icon)).BeginInit();
            this.gr_Common.SuspendLayout();
            this.SuspendLayout();
            // 
            // choosePackage
            // 
            this.choosePackage.FileName = "package.exe";
            this.choosePackage.Filter = "Файл пакета-установщика|package.exe";
            // 
            // chooseIcon
            // 
            this.chooseIcon.FileName = "icon";
            this.chooseIcon.Filter = "JPG|*.jpg|PNG|*.png|GIF|*.gif";
            this.chooseIcon.FilterIndex = 3;
            // 
            // chooseBanner
            // 
            this.chooseBanner.FileName = "banner";
            this.chooseBanner.Filter = "JPG|*.jpg|PNG|*.png|GIF|*.gif";
            this.chooseBanner.FilterIndex = 3;
            // 
            // btn_Accept
            // 
            this.btn_Accept.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Accept.ForeColor = System.Drawing.Color.Aqua;
            this.btn_Accept.Location = new System.Drawing.Point(12, 522);
            this.btn_Accept.Name = "btn_Accept";
            this.btn_Accept.Size = new System.Drawing.Size(819, 42);
            this.btn_Accept.TabIndex = 228;
            this.btn_Accept.Text = "Опубликовать";
            this.btn_Accept.UseVisualStyleBackColor = true;
            this.btn_Accept.Click += new System.EventHandler(this.btn_Publish_Click);
            // 
            // gr_Public
            // 
            this.gr_Public.Controls.Add(this.pb_Banner);
            this.gr_Public.Controls.Add(this.tb_Descr);
            this.gr_Public.Controls.Add(this.btn_Icon);
            this.gr_Public.Controls.Add(this.btn_Banner);
            this.gr_Public.Controls.Add(this.lbl_Descr);
            this.gr_Public.Controls.Add(this.pb_Icon);
            this.gr_Public.Location = new System.Drawing.Point(438, 65);
            this.gr_Public.Name = "gr_Public";
            this.gr_Public.Size = new System.Drawing.Size(393, 451);
            this.gr_Public.TabIndex = 741;
            this.gr_Public.TabStop = false;
            this.gr_Public.Text = "Дизайн и описание";
            // 
            // pb_Banner
            // 
            this.pb_Banner.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pb_Banner.Image = global::JeffsetManager.Properties.Resources.defaultAva;
            this.pb_Banner.Location = new System.Drawing.Point(6, 57);
            this.pb_Banner.Name = "pb_Banner";
            this.pb_Banner.Size = new System.Drawing.Size(269, 100);
            this.pb_Banner.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pb_Banner.TabIndex = 229;
            this.pb_Banner.TabStop = false;
            // 
            // tb_Descr
            // 
            this.tb_Descr.AcceptsReturn = true;
            this.tb_Descr.AcceptsTab = true;
            this.tb_Descr.AllowDrop = true;
            this.tb_Descr.BackColor = System.Drawing.Color.DimGray;
            this.tb_Descr.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_Descr.Location = new System.Drawing.Point(6, 218);
            this.tb_Descr.Multiline = true;
            this.tb_Descr.Name = "tb_Descr";
            this.tb_Descr.Size = new System.Drawing.Size(381, 227);
            this.tb_Descr.TabIndex = 8;
            this.tb_Descr.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btn_Icon
            // 
            this.btn_Icon.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Icon.Location = new System.Drawing.Point(6, 163);
            this.btn_Icon.Name = "btn_Icon";
            this.btn_Icon.Size = new System.Drawing.Size(376, 29);
            this.btn_Icon.TabIndex = 8;
            this.btn_Icon.Text = "<иконка>";
            this.btn_Icon.UseVisualStyleBackColor = true;
            this.btn_Icon.Click += new System.EventHandler(this.btn_Icon_Click);
            // 
            // btn_Banner
            // 
            this.btn_Banner.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Banner.Location = new System.Drawing.Point(6, 23);
            this.btn_Banner.Name = "btn_Banner";
            this.btn_Banner.Size = new System.Drawing.Size(375, 29);
            this.btn_Banner.TabIndex = 7;
            this.btn_Banner.Text = "<баннер>";
            this.btn_Banner.UseVisualStyleBackColor = true;
            this.btn_Banner.Click += new System.EventHandler(this.btn_Banner_Click);
            // 
            // lbl_Descr
            // 
            this.lbl_Descr.AutoSize = true;
            this.lbl_Descr.Location = new System.Drawing.Point(3, 198);
            this.lbl_Descr.Name = "lbl_Descr";
            this.lbl_Descr.Size = new System.Drawing.Size(170, 17);
            this.lbl_Descr.TabIndex = 8;
            this.lbl_Descr.Text = "Описание продукта:";
            // 
            // pb_Icon
            // 
            this.pb_Icon.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pb_Icon.Image = global::JeffsetManager.Properties.Resources.defaultAva;
            this.pb_Icon.Location = new System.Drawing.Point(282, 58);
            this.pb_Icon.Name = "pb_Icon";
            this.pb_Icon.Size = new System.Drawing.Size(100, 100);
            this.pb_Icon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pb_Icon.TabIndex = 229;
            this.pb_Icon.TabStop = false;
            // 
            // gr_Common
            // 
            this.gr_Common.Controls.Add(this.chb_Tester);
            this.gr_Common.Controls.Add(this.cb_UserLicense);
            this.gr_Common.Controls.Add(this.lbl_UserLic);
            this.gr_Common.Controls.Add(this.lbl_FriendName);
            this.gr_Common.Controls.Add(this.lbl_Ver);
            this.gr_Common.Controls.Add(this.tb_FriendlyName);
            this.gr_Common.Controls.Add(this.tb_MinVer);
            this.gr_Common.Controls.Add(this.tb_CurVer);
            this.gr_Common.Controls.Add(this.tb_NewVersion);
            this.gr_Common.Controls.Add(this.btn_PackFolder);
            this.gr_Common.Controls.Add(this.btn_Package);
            this.gr_Common.Controls.Add(this.lbl_Package);
            this.gr_Common.Controls.Add(this.lbl_PackSize);
            this.gr_Common.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.gr_Common.Location = new System.Drawing.Point(12, 156);
            this.gr_Common.Name = "gr_Common";
            this.gr_Common.Size = new System.Drawing.Size(420, 360);
            this.gr_Common.TabIndex = 9;
            this.gr_Common.TabStop = false;
            this.gr_Common.Text = "Общие параметры продукта";
            // 
            // chb_Tester
            // 
            this.chb_Tester.AutoSize = true;
            this.chb_Tester.Location = new System.Drawing.Point(12, 163);
            this.chb_Tester.Name = "chb_Tester";
            this.chb_Tester.Size = new System.Drawing.Size(369, 21);
            this.chb_Tester.TabIndex = 9;
            this.chb_Tester.Text = "Тестовая версия (для тестеров, скрыта)";
            this.chb_Tester.UseVisualStyleBackColor = true;
            // 
            // cb_UserLicense
            // 
            this.cb_UserLicense.BackColor = System.Drawing.Color.Gray;
            this.cb_UserLicense.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_UserLicense.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb_UserLicense.FormattingEnabled = true;
            this.cb_UserLicense.Items.AddRange(new object[] {
            "Free",
            "Standard",
            "Extended"});
            this.cb_UserLicense.Location = new System.Drawing.Point(213, 125);
            this.cb_UserLicense.Name = "cb_UserLicense";
            this.cb_UserLicense.Size = new System.Drawing.Size(174, 25);
            this.cb_UserLicense.TabIndex = 5;
            // 
            // lbl_UserLic
            // 
            this.lbl_UserLic.AutoSize = true;
            this.lbl_UserLic.Location = new System.Drawing.Point(10, 128);
            this.lbl_UserLic.Name = "lbl_UserLic";
            this.lbl_UserLic.Size = new System.Drawing.Size(197, 17);
            this.lbl_UserLic.TabIndex = 8;
            this.lbl_UserLic.Text = "Минимальная лицензия:";
            // 
            // lbl_FriendName
            // 
            this.lbl_FriendName.AutoSize = true;
            this.lbl_FriendName.Location = new System.Drawing.Point(3, 23);
            this.lbl_FriendName.Name = "lbl_FriendName";
            this.lbl_FriendName.Size = new System.Drawing.Size(161, 17);
            this.lbl_FriendName.TabIndex = 8;
            this.lbl_FriendName.Text = "Отображаемое имя:";
            // 
            // lbl_Ver
            // 
            this.lbl_Ver.AutoSize = true;
            this.lbl_Ver.Location = new System.Drawing.Point(3, 73);
            this.lbl_Ver.Name = "lbl_Ver";
            this.lbl_Ver.Size = new System.Drawing.Size(404, 17);
            this.lbl_Ver.TabIndex = 8;
            this.lbl_Ver.Text = "Новая версия / Текущая версия / Минимальная:";
            // 
            // tb_FriendlyName
            // 
            this.tb_FriendlyName.BackColor = System.Drawing.Color.Silver;
            this.tb_FriendlyName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_FriendlyName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tb_FriendlyName.Location = new System.Drawing.Point(7, 44);
            this.tb_FriendlyName.Name = "tb_FriendlyName";
            this.tb_FriendlyName.Size = new System.Drawing.Size(389, 16);
            this.tb_FriendlyName.TabIndex = 1;
            this.tb_FriendlyName.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tb_MinVer
            // 
            this.tb_MinVer.BackColor = System.Drawing.Color.Silver;
            this.tb_MinVer.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_MinVer.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tb_MinVer.Location = new System.Drawing.Point(267, 94);
            this.tb_MinVer.Name = "tb_MinVer";
            this.tb_MinVer.Size = new System.Drawing.Size(120, 16);
            this.tb_MinVer.TabIndex = 4;
            this.tb_MinVer.Text = "1.0.0.0";
            this.tb_MinVer.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tb_CurVer
            // 
            this.tb_CurVer.BackColor = System.Drawing.Color.Silver;
            this.tb_CurVer.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_CurVer.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tb_CurVer.Location = new System.Drawing.Point(140, 94);
            this.tb_CurVer.Name = "tb_CurVer";
            this.tb_CurVer.ReadOnly = true;
            this.tb_CurVer.Size = new System.Drawing.Size(120, 16);
            this.tb_CurVer.TabIndex = 3;
            this.tb_CurVer.Text = "1.0.0.0";
            this.tb_CurVer.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tb_NewVersion
            // 
            this.tb_NewVersion.BackColor = System.Drawing.Color.Silver;
            this.tb_NewVersion.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_NewVersion.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tb_NewVersion.Location = new System.Drawing.Point(12, 94);
            this.tb_NewVersion.Name = "tb_NewVersion";
            this.tb_NewVersion.Size = new System.Drawing.Size(120, 16);
            this.tb_NewVersion.TabIndex = 2;
            this.tb_NewVersion.Text = "1.0.0.0";
            this.tb_NewVersion.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tb_NewVersion.TextChanged += new System.EventHandler(this.tb_NewVersion_TextChanged);
            // 
            // btn_PackFolder
            // 
            this.btn_PackFolder.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_PackFolder.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.btn_PackFolder.Location = new System.Drawing.Point(11, 291);
            this.btn_PackFolder.Name = "btn_PackFolder";
            this.btn_PackFolder.Size = new System.Drawing.Size(389, 31);
            this.btn_PackFolder.TabIndex = 6;
            this.btn_PackFolder.Text = "Упаковать каталог...";
            this.btn_PackFolder.UseVisualStyleBackColor = true;
            this.btn_PackFolder.Click += new System.EventHandler(this.btn_PackFolder_Click);
            // 
            // btn_Package
            // 
            this.btn_Package.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Package.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.btn_Package.Location = new System.Drawing.Point(12, 233);
            this.btn_Package.Name = "btn_Package";
            this.btn_Package.Size = new System.Drawing.Size(389, 31);
            this.btn_Package.TabIndex = 6;
            this.btn_Package.Text = "<Выбрать файл пакета>";
            this.btn_Package.UseVisualStyleBackColor = true;
            this.btn_Package.Click += new System.EventHandler(this.btn_Package_Click);
            // 
            // lbl_Package
            // 
            this.lbl_Package.AutoSize = true;
            this.lbl_Package.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.lbl_Package.Location = new System.Drawing.Point(8, 212);
            this.lbl_Package.Name = "lbl_Package";
            this.lbl_Package.Size = new System.Drawing.Size(278, 17);
            this.lbl_Package.TabIndex = 8;
            this.lbl_Package.Text = "Пакет-установщик (package.exe)";
            // 
            // lbl_PackSize
            // 
            this.lbl_PackSize.AutoSize = true;
            this.lbl_PackSize.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.lbl_PackSize.Location = new System.Drawing.Point(14, 267);
            this.lbl_PackSize.Name = "lbl_PackSize";
            this.lbl_PackSize.Size = new System.Drawing.Size(152, 17);
            this.lbl_PackSize.TabIndex = 8;
            this.lbl_PackSize.Text = "Размер пакета: -";
            // 
            // lbl_IntName
            // 
            this.lbl_IntName.AutoSize = true;
            this.lbl_IntName.Location = new System.Drawing.Point(14, 63);
            this.lbl_IntName.Name = "lbl_IntName";
            this.lbl_IntName.Size = new System.Drawing.Size(215, 17);
            this.lbl_IntName.TabIndex = 8;
            this.lbl_IntName.Text = "Внутренее имя продукта:";
            // 
            // tb_InternalName
            // 
            this.tb_InternalName.BackColor = System.Drawing.Color.Silver;
            this.tb_InternalName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_InternalName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tb_InternalName.Location = new System.Drawing.Point(14, 84);
            this.tb_InternalName.Name = "tb_InternalName";
            this.tb_InternalName.Size = new System.Drawing.Size(418, 16);
            this.tb_InternalName.TabIndex = 0;
            this.tb_InternalName.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btn_UpdateMode
            // 
            this.btn_UpdateMode.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_UpdateMode.ForeColor = System.Drawing.Color.Lime;
            this.btn_UpdateMode.Location = new System.Drawing.Point(12, 114);
            this.btn_UpdateMode.Name = "btn_UpdateMode";
            this.btn_UpdateMode.Size = new System.Drawing.Size(420, 36);
            this.btn_UpdateMode.TabIndex = 740;
            this.btn_UpdateMode.Text = "Это обовление";
            this.btn_UpdateMode.UseVisualStyleBackColor = true;
            this.btn_UpdateMode.Click += new System.EventHandler(this.btn_UpdateMode_Click);
            // 
            // folderBrowserDialog
            // 
            this.folderBrowserDialog.Description = "Выберите каталог с программой для сжатия в пакет-установщик";
            this.folderBrowserDialog.RootFolder = System.Environment.SpecialFolder.MyComputer;
            this.folderBrowserDialog.ShowNewFolderButton = false;
            // 
            // Arh_SoftwareEdit
            // 
            this.AcceptButton = this.btn_Accept;
            this.ArahonText = "Arahon: Публикация продукта";
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 17F);
            this.ClientSize = new System.Drawing.Size(846, 581);
            this.Controls.Add(this.gr_Public);
            this.Controls.Add(this.gr_Common);
            this.Controls.Add(this.lbl_IntName);
            this.Controls.Add(this.tb_InternalName);
            this.Controls.Add(this.btn_UpdateMode);
            this.Controls.Add(this.btn_Accept);
            this.Name = "Arh_SoftwareEdit";
            this.Opacity = 1D;
            this.Shown += new System.EventHandler(this.ArahonProductEdit_Shown);
            this.gr_Public.ResumeLayout(false);
            this.gr_Public.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Banner)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Icon)).EndInit();
            this.gr_Common.ResumeLayout(false);
            this.gr_Common.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_Accept;
        private System.Windows.Forms.TextBox tb_InternalName;
        private System.Windows.Forms.Label lbl_IntName;
        private System.Windows.Forms.TextBox tb_NewVersion;
        private System.Windows.Forms.Label lbl_Ver;
        private System.Windows.Forms.Button btn_Package;
        private System.Windows.Forms.GroupBox gr_Common;
        private System.Windows.Forms.Label lbl_UserLic;
        private System.Windows.Forms.Label lbl_Package;
        private System.Windows.Forms.Label lbl_FriendName;
        private System.Windows.Forms.TextBox tb_FriendlyName;
        private System.Windows.Forms.TextBox tb_MinVer;
        private System.Windows.Forms.TextBox tb_CurVer;
        private System.Windows.Forms.OpenFileDialog choosePackage;
        private System.Windows.Forms.Label lbl_PackSize;
        private System.Windows.Forms.ComboBox cb_UserLicense;
        private System.Windows.Forms.PictureBox pb_Icon;
        private System.Windows.Forms.Button btn_Icon;
        private System.Windows.Forms.OpenFileDialog chooseIcon;
        private System.Windows.Forms.Button btn_UpdateMode;
        private System.Windows.Forms.TextBox tb_Descr;
        private System.Windows.Forms.PictureBox pb_Banner;
        private System.Windows.Forms.Label lbl_Descr;
        private System.Windows.Forms.Button btn_Banner;
        private System.Windows.Forms.OpenFileDialog chooseBanner;
        private System.Windows.Forms.CheckBox chb_Tester;
        private System.Windows.Forms.GroupBox gr_Public;
        private System.Windows.Forms.Button btn_PackFolder;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
    }
}
