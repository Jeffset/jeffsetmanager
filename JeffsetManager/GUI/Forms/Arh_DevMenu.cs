﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Net;
using System.Text;
using System.Windows.Forms;
using ManagerCore;

namespace JeffsetManager {
    public partial class Arh_DevMenu : JeffsetManager.Arh_Dialog {
        public Arh_DevMenu() {
            InitializeComponent();
        }

        private async void btn_PublishSelf_Click(object sender, EventArgs e) {
            if(Arh_Dialog.BUSY_CONTROL(sender)) return;
            //<--------BUSY--CONTROL--------->
            try
            {
                await JM.Core.PublishThisManagerVersionAsync();
                Arh_MessageBox.ShowMB($"Версия JM {JM.Core.ManagerVersion} была опубликована и теперь доступна для загрузки.",
                       "JM Self-Update", Arh_MessageBox.AMBType.Success);
            }
            #region Exception Handling
            catch(WebException)
            {
                Arh_MessageBox.ShowMB($"Версия JM {JM.Core.ManagerVersion} не была опубликована, ошибка выгрузки!",
                       "JM Self-Update Web Error", Arh_MessageBox.AMBType.Error);
            }
            catch(InvalidOperationException exc)
            {
                Arh_MessageBox.ShowMB(
                    $"Версия JM {JM.Core.ManagerVersion} не была опубликована, ошибка сжатия пакета. ErrCode:[{exc.Message}]",
                       "JM Self-Update Compressing Error", Arh_MessageBox.AMBType.Error);
            }
            #endregion
        }

        private void btn_AccManage_Click(object sender, EventArgs e) {
            if(Arh_Dialog.BUSY_CONTROL(sender)) return;
            //<--------BUSY--CONTROL--------->
            var accManage = new Arh_AccountManagement();
            accManage.ShowDialog();
        }

        private void btnViewReqs_Click(object sender, EventArgs e) {
            if(Arh_Dialog.BUSY_CONTROL(sender)) return;
            //<--------BUSY--CONTROL--------->
            var vrDialog = new Arh_ViewDevReqsDialog();
            var res = vrDialog.ShowDialog();
        }
    }
}
