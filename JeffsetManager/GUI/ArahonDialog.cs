﻿using ManagerCore;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Threading.Tasks;

namespace JeffsetManager {
    public partial class Arh_Dialog : Form {
        #region Animation
        private const int _animInt = 75;
        private const int _animTimes = 3;
        protected static bool _ErrorAnim(Control lbl) {
            //_orginalColor = this.ForeColor;
            //_animator.Tag = lbl;
            //_animator.Start();
            new Action(() => {
                Color orig = lbl.ForeColor;
                for(int i = 0; i < _animTimes * 2; i++) {
                    lbl.Invoke(new Action(() => {
                        lbl.ForeColor = lbl.ForeColor == Color.Red ? orig : Color.Red;
                    }));
                    Thread.Sleep(_animInt);
                }
            }).BeginInvoke(null, null);
            return false;
        }
        #endregion

        const int offset = 4;
        const float width = 2.0f;
        public Arh_Dialog() {
            InitializeComponent();
            border = new Pen(Color.Black);
            border.DashStyle = System.Drawing.Drawing2D.DashStyle.Dash;
            border.Width = width;
            //
        }


        private void _btnCLose_Click(object sender, EventArgs e) {
            this.DialogResult = DialogResult.Cancel;
            Close();
        }

        [Category("Appearance")]
        public string ArahonText {
            get { return _lblCaption.Text; }
            set { _lblCaption.Text = value; }
        }

        [Category("Appearance")]
        public bool UseSmoothClosing { get; set; } = true;

        private Pen border;
        protected override void OnPaint(PaintEventArgs e) {
            base.OnPaint(e);
            Rectangle r = ClientRectangle;
            r.Inflate(-offset, -offset);
            e.Graphics.DrawRectangle(border, r);
        }

        Size _last;
        private void _lblCaption_MouseMove(object sender, MouseEventArgs e) {
            if(e.Button != MouseButtons.Left) {
                _last = new Size(e.X, e.Y);
                Opacity = 1.0;
                return;
            }
            Size delta = Size.Subtract(_last, new Size(e.X, e.Y));
            Point tmp = Location;
            tmp.Offset(-delta.Width, -delta.Height);
            Location = tmp;
            Opacity = 0.5;
        }

        protected Label GenInfoLabel(string text, Color color) {
            #region Label
            Label lbl = new Label();
            lbl.Text = text;
            lbl.AutoSize = true;
            lbl.TextAlign = ContentAlignment.MiddleCenter;
            lbl.ForeColor = color;
            return lbl;
            #endregion   
        }

        public static bool BUSY_CONTROL(object s) {
            //<------------------------BUSY--CONTROL----------------------->
            if(JM.Core.IsBusy) { _ErrorAnim(s as Control); return true; }
            else return false;
            //<------------------------BUSY--CONTROL----------------------->
        }

        private async void Arh_Dialog_Load(object sender, EventArgs e) {
            Location += new Size(0, 30);
            for(int i = 0; i < 30; i++) {
                Opacity = i / 30.0;
                Location -= new Size(0, 1);
                await Task.Run(new Action(() => { Thread.Sleep(7); }));
            }
            Opacity = 1.0;
        }

        //private bool _animClosing = false;
        protected override void OnFormClosing(FormClosingEventArgs e) {
            base.OnFormClosing(e);
            if(UseSmoothClosing) {
                for(int i = 0; i < 30; i++) {
                    Opacity = 1.0 - i / 30.0;
                    Location += new Size(0, 1);
                    Thread.Sleep(5);
                }
                Opacity = 0.0;
            }
        }
    }
}
