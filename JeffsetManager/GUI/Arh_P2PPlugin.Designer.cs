﻿namespace JeffsetManager
{
    partial class Arh_P2PPlugin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_ListOnline = new System.Windows.Forms.Label();
            this.btn_Connect = new System.Windows.Forms.Button();
            this.lb_Online = new System.Windows.Forms.ListBox();
            this.btn_Refresh = new System.Windows.Forms.Button();
            this.lbl_Out = new System.Windows.Forms.TextBox();
            this.btn_Clear = new System.Windows.Forms.Button();
            this.lbl_output = new System.Windows.Forms.Label();
            this.tb_Message = new System.Windows.Forms.TextBox();
            this.lbl_remoteWrite = new System.Windows.Forms.Label();
            this.btn_Disconnect = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbl_ListOnline
            // 
            this.lbl_ListOnline.AutoSize = true;
            this.lbl_ListOnline.Location = new System.Drawing.Point(627, 80);
            this.lbl_ListOnline.Name = "lbl_ListOnline";
            this.lbl_ListOnline.Size = new System.Drawing.Size(188, 17);
            this.lbl_ListOnline.TabIndex = 6;
            this.lbl_ListOnline.Text = "Пользователи онлайн:";
            // 
            // btn_Connect
            // 
            this.btn_Connect.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Connect.ForeColor = System.Drawing.Color.Cyan;
            this.btn_Connect.Location = new System.Drawing.Point(630, 227);
            this.btn_Connect.Name = "btn_Connect";
            this.btn_Connect.Size = new System.Drawing.Size(180, 31);
            this.btn_Connect.TabIndex = 7;
            this.btn_Connect.Text = "Подключиться";
            this.btn_Connect.UseVisualStyleBackColor = true;
            this.btn_Connect.Click += new System.EventHandler(this.btn_Connect_Click);
            // 
            // lb_Online
            // 
            this.lb_Online.BackColor = System.Drawing.Color.Gray;
            this.lb_Online.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lb_Online.FormattingEnabled = true;
            this.lb_Online.ItemHeight = 17;
            this.lb_Online.Location = new System.Drawing.Point(630, 103);
            this.lb_Online.Name = "lb_Online";
            this.lb_Online.Size = new System.Drawing.Size(180, 70);
            this.lb_Online.TabIndex = 8;
            // 
            // btn_Refresh
            // 
            this.btn_Refresh.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Refresh.Location = new System.Drawing.Point(630, 184);
            this.btn_Refresh.Name = "btn_Refresh";
            this.btn_Refresh.Size = new System.Drawing.Size(180, 31);
            this.btn_Refresh.TabIndex = 7;
            this.btn_Refresh.Text = "Обновить";
            this.btn_Refresh.UseVisualStyleBackColor = true;
            this.btn_Refresh.Click += new System.EventHandler(this.btn_Refresh_Click);
            // 
            // lbl_Out
            // 
            this.lbl_Out.AcceptsReturn = true;
            this.lbl_Out.BackColor = System.Drawing.Color.Black;
            this.lbl_Out.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lbl_Out.Font = new System.Drawing.Font("Courier New", 11F);
            this.lbl_Out.ForeColor = System.Drawing.Color.Lime;
            this.lbl_Out.Location = new System.Drawing.Point(12, 103);
            this.lbl_Out.Multiline = true;
            this.lbl_Out.Name = "lbl_Out";
            this.lbl_Out.ReadOnly = true;
            this.lbl_Out.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.lbl_Out.Size = new System.Drawing.Size(612, 423);
            this.lbl_Out.TabIndex = 9;
            this.lbl_Out.WordWrap = false;
            // 
            // btn_Clear
            // 
            this.btn_Clear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Clear.ForeColor = System.Drawing.Color.Silver;
            this.btn_Clear.Location = new System.Drawing.Point(630, 409);
            this.btn_Clear.Name = "btn_Clear";
            this.btn_Clear.Size = new System.Drawing.Size(180, 31);
            this.btn_Clear.TabIndex = 7;
            this.btn_Clear.Text = "Очистить вывод";
            this.btn_Clear.UseVisualStyleBackColor = true;
            this.btn_Clear.Click += new System.EventHandler(this.btn_Clear_Click);
            // 
            // lbl_output
            // 
            this.lbl_output.AutoSize = true;
            this.lbl_output.Location = new System.Drawing.Point(12, 80);
            this.lbl_output.Name = "lbl_output";
            this.lbl_output.Size = new System.Drawing.Size(134, 17);
            this.lbl_output.TabIndex = 6;
            this.lbl_output.Text = "Вывод плагина:";
            // 
            // tb_Message
            // 
            this.tb_Message.BackColor = System.Drawing.Color.LightGray;
            this.tb_Message.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_Message.Location = new System.Drawing.Point(12, 532);
            this.tb_Message.Multiline = true;
            this.tb_Message.Name = "tb_Message";
            this.tb_Message.Size = new System.Drawing.Size(805, 91);
            this.tb_Message.TabIndex = 10;
            this.tb_Message.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tb_Message.TextChanged += new System.EventHandler(this.tb_Message_TextChanged);
            this.tb_Message.KeyUp += new System.Windows.Forms.KeyEventHandler(this.textBox1_KeyUp);
            // 
            // lbl_remoteWrite
            // 
            this.lbl_remoteWrite.AutoSize = true;
            this.lbl_remoteWrite.Location = new System.Drawing.Point(630, 509);
            this.lbl_remoteWrite.Name = "lbl_remoteWrite";
            this.lbl_remoteWrite.Size = new System.Drawing.Size(80, 17);
            this.lbl_remoteWrite.TabIndex = 11;
            this.lbl_remoteWrite.Text = "пишет...";
            this.lbl_remoteWrite.Visible = false;
            // 
            // btn_Disconnect
            // 
            this.btn_Disconnect.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Disconnect.ForeColor = System.Drawing.Color.IndianRed;
            this.btn_Disconnect.Location = new System.Drawing.Point(630, 264);
            this.btn_Disconnect.Name = "btn_Disconnect";
            this.btn_Disconnect.Size = new System.Drawing.Size(180, 31);
            this.btn_Disconnect.TabIndex = 7;
            this.btn_Disconnect.Text = "Отключится";
            this.btn_Disconnect.UseVisualStyleBackColor = true;
            this.btn_Disconnect.Click += new System.EventHandler(this.btn_Disconnect_Click);
            // 
            // Arh_P2PPlugin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 17F);
            this.ClientSize = new System.Drawing.Size(829, 635);
            this.Controls.Add(this.lbl_remoteWrite);
            this.Controls.Add(this.tb_Message);
            this.Controls.Add(this.lbl_Out);
            this.Controls.Add(this.lb_Online);
            this.Controls.Add(this.btn_Clear);
            this.Controls.Add(this.btn_Refresh);
            this.Controls.Add(this.btn_Disconnect);
            this.Controls.Add(this.btn_Connect);
            this.Controls.Add(this.lbl_output);
            this.Controls.Add(this.lbl_ListOnline);
            this.Name = "Arh_P2PPlugin";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Arh_P2PPlugin_FormClosed);
            this.Load += new System.EventHandler(this.Arh_P2PPlugin_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_ListOnline;
        private System.Windows.Forms.Button btn_Connect;
        private System.Windows.Forms.ListBox lb_Online;
        private System.Windows.Forms.Button btn_Refresh;
        private System.Windows.Forms.TextBox lbl_Out;
        private System.Windows.Forms.Button btn_Clear;
        private System.Windows.Forms.Label lbl_output;
        private System.Windows.Forms.TextBox tb_Message;
        private System.Windows.Forms.Label lbl_remoteWrite;
        private System.Windows.Forms.Button btn_Disconnect;
    }
}
