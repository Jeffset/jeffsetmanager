﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using P2PJeffConnect;
using System.Threading.Tasks;
using System.Linq;
using System.Diagnostics;
using System.Threading;
using System.IO;
using System.Drawing.Imaging;
using ManagerCore;

namespace JeffsetManager
{
    public partial class Arh_P2PPlugin : JeffsetManager.Arh_Dialog
    {
        private enum ComandId
        {
            UserTypesMessageText = 3,
        }

        P2PThisPeer thisPeer;
        public Arh_P2PPlugin()
        {
            InitializeComponent();
            thisPeer = new P2PThisPeer(JM.Core.CurrentUser.Credentials.UserName);
            thisPeer.ConnectionEstablished += ThisPeer_ConnectionEstablished;
            _p2ptrace.Print += Print;
            gr = Graphics.FromImage(img);
        }

        private void ThisPeer_TextReceived(string peerName, P2PTinyMessage text)
        {
            Print($"{peerName}: {text.GetContentAsString()}");
        }

        private void Print(string msg)
        {
            try
            {
                Invoke(new Action(() =>
                   {
                       lbl_Out.AppendText(msg + "\r\n");
                       lbl_Out.ScrollToCaret();
                   }));
            }
            catch (InvalidOperationException)
            { }
        }


        private void ThisPeer_ConnectionEstablished(P2PConnection connection, EventArgs e)
        {
            Print($"EVT -> connected to: {connection.RemotePeerName}");
            connection.Disconnected += OnDisconnected;
            connection.MessageReceived += OnMessageReceived;
        }

        private void OnMessageReceived(P2PConnection sender, P2PMessageEventArgs e)
        {
            Invoke(new Action(() =>
            {
                switch (e.Message.Type)
                {
                    case MessageType.DisconnectNotif:
                        Print($"SERVICE-> {sender.RemotePeerName} wants to disconnect.");
                        var not_wait = sender.SafeDisconnectAsync();
                        break;
                    case MessageType.TextWin1251:
                    case MessageType.TextUnicode:
                        Print($"{sender.RemotePeerName}: {e.Message.GetContentAsString()}");
                        break;
                    case MessageType.UserService:
                        ComandId cmd = (ComandId)e.Message.GetContentsAsInt();
                        switch (cmd)
                        {
                            case ComandId.UserTypesMessageText:
                                if (!lbl_remoteWrite.Visible)
                                    new Action(() =>
                                    {
                                        do
                                        {
                                            lbl_remoteWrite.Tag = null;
                                            Thread.Sleep(1000);
                                        } while (lbl_remoteWrite.Tag != null);
                                        Invoke(new Action(() => { lbl_remoteWrite.Visible = false; }));
                                    }).BeginInvoke(null, null);
                                lbl_remoteWrite.Text = $"{sender.RemotePeerName} пишет...";
                                lbl_remoteWrite.Visible = true;
                                lbl_remoteWrite.Tag = 1000;
                                break;
                            default:
                                Print($"PLUGIN-> Unknown or unsupported command: {cmd}");
                                break;
                        }
                        break;
                    default:
                        break;
                }
                if (e.Message.Type == MessageType.Bytepack)
                {
                    if (BackgroundImage != null)
                        BackgroundImage.Dispose();
                    BackgroundImage = Image.FromStream(new MemoryStream(e.Message.GetBytepack()));
                    Invalidate();
                }
            }));
        }

        private void OnDisconnected(P2PConnection sender, EventArgs e)
        {
            Print($"SERVICE -> {sender.RemotePeerName} Disconnected.");
        }

        private async void Arh_P2PPlugin_Load(object sender, EventArgs e)
        {
            await thisPeer.SetOnlineAsync(true);
            await thisPeer.RunRequestWatcherService(true);
            btn_Refresh_Click(null, null);
        }

        private async void btn_Connect_Click(object sender, EventArgs e)
        {
            try
            {
                if (lb_Online.SelectedItem != null)
                    await thisPeer.ConnectToAsync(lb_Online.SelectedItem as string);
            }
            catch (InvalidOperationException ex)
            {
                Print($"ERR_INVALID_OP -> {ex.Message}");
            }
            catch (TimeoutException ex)
            {
                Print($"ERR_TIME_IS_OUT -> {ex.Message}");
            }
            await thisPeer.RefreshOnlinePeersAsync();
        }

        private async void btn_Refresh_Click(object sender, EventArgs e)
        {
            var peers = await thisPeer.RefreshOnlinePeersAsync();
            lb_Online.Items.Clear();
            lb_Online.Items.AddRange(peers.ToArray());
        }

        private void Arh_P2PPlugin_FormClosed(object sender, FormClosedEventArgs e)
        {
            thisPeer.Dispose();
        }

        private void btn_Clear_Click(object sender, EventArgs e)
        {
            lbl_Out.Text = "";
            //P2PConnection tunnel = _GetSelectedConnection();
            //if(tunnel!= null)
            //{
            //    tunnel.SupportedModes.Add(P2PConnectionMode.Synchronizator);
            //    tunnel.ChangeMode(P2PConnectionMode.Synchronizator);
            //}
        }

        Image img = new Bitmap(64, 64); Graphics gr;
        private async void textBox1_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode != Keys.Enter) return;
            //gr.CopyFromScreen(MousePosition, Point.Empty, new Size(64, 64));
            //MemoryStream ms = new MemoryStream();
            //img.Save(ms, ImageFormat.Gif);
            //var msg = P2PTinyMessage.CreateBytepack(ms.GetBuffer());
            //bool res = await tunnel1.SendMessageAsync(msg);
            //if (res) ThisPeer_Output("OK");
            //else
            //    ThisPeer_Output("NO");
            //return;
            P2PConnection tunnel = _GetSelectedConnection();
            if (tunnel != null && !string.IsNullOrEmpty(tb_Message.Text))
            {
                string text = tb_Message.Text;
                text = text.TrimEnd();
                bool result = await tunnel.SendMessageAsync(P2PTinyMessage.CreateTextMessage(text));
                if (result)
                {
                    Print($"{thisPeer.PeerName}: {text}");
                }
                else
                    Print($"-------ERROR SEND-------");
                tb_Message.Text = "";
            }
        }

        private P2PConnection _GetSelectedConnection()
        {
            P2PConnection tunnel = null;
            if (lb_Online.SelectedItem != null)
                tunnel = thisPeer.GetConnection(lb_Online.SelectedItem as string);
            return tunnel;
        }

        private async void tb_Message_TextChanged(object sender, EventArgs e)
        {
            var tunnel = _GetSelectedConnection();
            if (tunnel == null) return;
            bool delivered = await tunnel.SendMessageAsync(P2PTinyMessage.CreateControlMessage((int)ComandId.UserTypesMessageText));
        }

        private async void btn_Disconnect_Click(object sender, EventArgs e)
        {
            var tunnel = _GetSelectedConnection();
            if (tunnel == null) return;
            await tunnel.SafeDisconnectAsync();
            await thisPeer.RefreshOnlinePeersAsync();
        }
    }
}
