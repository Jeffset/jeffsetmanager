﻿namespace JeffsetManager
{
    partial class Arh_Dialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Panel _panelCaption;
            System.Windows.Forms.Panel _pnClose;
            System.Windows.Forms.Button _btnCLose;
            this._pnLabl = new System.Windows.Forms.Panel();
            this._lblCaption = new System.Windows.Forms.Label();
            _panelCaption = new System.Windows.Forms.Panel();
            _pnClose = new System.Windows.Forms.Panel();
            _btnCLose = new System.Windows.Forms.Button();
            _panelCaption.SuspendLayout();
            this._pnLabl.SuspendLayout();
            _pnClose.SuspendLayout();
            this.SuspendLayout();
            // 
            // _panelCaption
            // 
            _panelCaption.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            _panelCaption.Controls.Add(this._pnLabl);
            _panelCaption.Controls.Add(_pnClose);
            _panelCaption.Dock = System.Windows.Forms.DockStyle.Top;
            _panelCaption.Location = new System.Drawing.Point(0, 0);
            _panelCaption.Name = "_panelCaption";
            _panelCaption.Size = new System.Drawing.Size(380, 58);
            _panelCaption.TabIndex = 5;
            // 
            // _pnLabl
            // 
            this._pnLabl.Controls.Add(this._lblCaption);
            this._pnLabl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._pnLabl.Location = new System.Drawing.Point(0, 0);
            this._pnLabl.Name = "_pnLabl";
            this._pnLabl.Size = new System.Drawing.Size(323, 58);
            this._pnLabl.TabIndex = 1;
            // 
            // _lblCaption
            // 
            this._lblCaption.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(43)))), ((int)(((byte)(43)))));
            this._lblCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this._lblCaption.Font = new System.Drawing.Font("Courier New", 14F, System.Drawing.FontStyle.Bold);
            this._lblCaption.Image = global::JeffsetManager.Properties.Resources.border;
            this._lblCaption.Location = new System.Drawing.Point(0, 0);
            this._lblCaption.Name = "_lblCaption";
            this._lblCaption.Size = new System.Drawing.Size(323, 58);
            this._lblCaption.TabIndex = 4;
            this._lblCaption.Text = "Arahon: заголовок окна";
            this._lblCaption.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this._lblCaption.MouseMove += new System.Windows.Forms.MouseEventHandler(this._lblCaption_MouseMove);
            // 
            // _pnClose
            // 
            _pnClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(36)))), ((int)(((byte)(36)))));
            _pnClose.Controls.Add(_btnCLose);
            _pnClose.Dock = System.Windows.Forms.DockStyle.Right;
            _pnClose.Location = new System.Drawing.Point(323, 0);
            _pnClose.Name = "_pnClose";
            _pnClose.Size = new System.Drawing.Size(57, 58);
            _pnClose.TabIndex = 0;
            // 
            // _btnCLose
            // 
            _btnCLose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            _btnCLose.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            _btnCLose.Image = global::JeffsetManager.Properties.Resources.close;
            _btnCLose.Location = new System.Drawing.Point(12, 13);
            _btnCLose.Name = "_btnCLose";
            _btnCLose.Size = new System.Drawing.Size(32, 32);
            _btnCLose.TabIndex = 2;
            _btnCLose.TabStop = false;
            _btnCLose.UseVisualStyleBackColor = true;
            _btnCLose.Click += new System.EventHandler(this._btnCLose_Click);
            // 
            // Arh_Dialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.CancelButton = _btnCLose;
            this.ClientSize = new System.Drawing.Size(380, 256);
            this.ControlBox = false;
            this.Controls.Add(_panelCaption);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Courier New", 11F, System.Drawing.FontStyle.Bold);
            this.ForeColor = System.Drawing.Color.MediumSpringGreen;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Arh_Dialog";
            this.Opacity = 0D;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.Arh_Dialog_Load);
            _panelCaption.ResumeLayout(false);
            this._pnLabl.ResumeLayout(false);
            _pnClose.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label _lblCaption;
        private System.Windows.Forms.Panel _pnLabl;
    }
}