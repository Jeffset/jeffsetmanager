﻿using ManagerCore;
using System;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;

namespace JeffsetManager
{

    static class Program
    {
        /// <summary>
        /// The return codes used to check updates by apps.
        /// </summary>
        public enum JMAnswer
        {
            NoRequest = 0,
            ManagerNeedUpdate,
            ManagerUpdateFinished,
            Error,
            Cancel,
        }

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static int Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            JM.CreateManager();

            // [ОСНОВНОЙ ПРОЦЕСС]
            // Самый обычный пользовательский запуск
            if (args.Length == 0)
            {
                //var p = Environment.GetEnvironmentVariable("PATH", EnvironmentVariableTarget.User);
                if (JM.Core.IsNeedUpdate())
                {
                    //var res = MessageBox.Show("Обнаружена новая версия. Для запуска необходимо обновление. Продолжить?",
                    //    "Self-Update", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                    var res = Arh_MessageBox.ShowMB
                        ("Обнаружена новая версия. Для запуска необходимо обновление. Продолжить?\n\r(OK/(X)Escape)",
                       "Self-Update", Arh_MessageBox.AMBType.Info);
                    if (res == DialogResult.Cancel)
                    {
                        Process.Start(Application.ExecutablePath, "-noupdate");
                        return (int)JMAnswer.Cancel;
                    }
                    File.Copy(Application.ExecutablePath, "selfUpdate.exe");
                    Process.Start("selfUpdate.exe", "-selfupdate");
                    return (int)JMAnswer.ManagerNeedUpdate;
                }
            }
            else if (args[0] == "-noupdate")
            {

            }
            // [ОБНОВЛЯЛЬНИК]
            // Процесс обновления: делает бекап основного екзе, скачивает новый,
            // Проверяет корректность, закрывается.
            else if (args[0] == "-selfupdate")
            {
                JM.Core.SelfUpdate();
                return (int)JMAnswer.ManagerUpdateFinished;
            }
            // [ОСНОВНОЙ ПРОЦЕСС]
            // Если обновление не прокатило.
            else if (args[0] == "-updatefailed")
            {
                File.Delete("selfUpdate.exe");
                var ans = MessageBox.Show("Error during self-update! Try restarting.", "Update failed",
                    MessageBoxButtons.RetryCancel, MessageBoxIcon.Error);
                if (ans == DialogResult.Retry)
                    Process.Start("JeffsetManager.exe");
                return (int)JMAnswer.Error;
            }
            // [ОСНОВНОЙ ПРОЦЕСС]
            // Уже основной процесс только что после обновления
            else if (args[0] == "-updated")
            {
                File.Delete("selfUpdate.exe");
            }

            Environment.SetEnvironmentVariable("ARAHONPATH", Application.ExecutablePath, EnvironmentVariableTarget.User);
            Application.Run(new FormMain());
            return (int)JMAnswer.NoRequest;
        }
    }
}
