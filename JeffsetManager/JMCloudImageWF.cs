﻿using ManagerCore;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using CloudOperations;

namespace JeffsetManager
{
    public class JMCloudImageWF : JMCloudImage
    { 
        public JMCloudImageWF(string cloudPath, CloudClient cloud)
            : base(cloudPath, cloud)
        { }

        protected override ImageObj _ImageProcessor<ImageObj>(Stream s)
        => Image.FromStream(s) as ImageObj;
    }
}
