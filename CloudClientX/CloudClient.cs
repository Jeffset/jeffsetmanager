﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;

namespace CloudOperations
{
    /// <summary>
    /// Specifies weither a cloud element is a file or a directory.
    /// </summary>
    public enum CloudEntityType
    {
        File, Directory
    }
    /// <summary>
    /// Represents a file or a directory in a cloud, providing all necessary info about it.
    /// </summary>
    public class CloudEntityInfo
    {
        public int Size { get; set; }
        public CloudEntityType Type { get; set; }
        public string Name { get; set; }
        public string FullPath { get; set; }
        public DateTime CreationTime { get; set; }
        public DateTime LastModifiedTime { get; set; }
        public string MD5 { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }

    //public delegate void CloudListFinishedEHandler(CloudEntityInfo[] info);
    //public delegate void CloudDownloadFinishedEHandler(string downloadedLocal, MemoryStream memory);
    //public delegate void CloudUploadFinishedEHandler(string uploadedFileCloud);
    //public delegate void CloudCatalogEHandler(string catalogName, bool alreadyExisted);
    //public delegate void CloudDeleteEntityEHandler(string deletedEntity);
    //public delegate void CloudCopyEntityEHandler(string destEntity, bool overWritten);

    public delegate void CloudReportProgressEHandler(long bytesDone, long bytesTotal, int progress);

    [Serializable]
    public class CloudNotFoundException : InvalidOperationException
    {
        public CloudNotFoundException(string path, Exception inner)
            : base(path, inner)
        { }
    }
    [Serializable]
    public class CloudDestPathInvalidException : Exception
    {
        public CloudDestPathInvalidException(string path, Exception inner)
            : base(string.Format("Path \"{0}\" is partialy missing or invalid.", path), inner)
        { }
    }

    /// <summary>
    /// Provides basic methods to access and operate data within a WebDAV-compliant server (cloud).
    /// </summary>
    public class CloudClient
    {
        const int TRANSFER_BUFFER = 4096 * 16;

        // sorry, but I can't give my credentials
        private string _username = "*****";
        private string _password = "*****";
        private NetworkCredential _authInfo;

        /// <summary>
        /// Sets/gets a path to current working directory in a cloud.
        /// All requests are rooted to this directory.
        /// </summary>
        public string WorkingPath { get; set; }

        //public event CloudListFinishedEHandler ListFinished;
        //public event CloudDownloadFinishedEHandler DownloadFinished;
        //public event CloudUploadFinishedEHandler UploadFinished;
        //public event CloudCatalogEHandler CatalogCreated;
        //public event CloudDeleteEntityEHandler FileCatalogDeleted;
        //public event CloudCopyEntityEHandler FileCatalogCopied;

        /// <summary>
        /// Occurs when progress of transfer (download or upload) operation is changed.
        /// (Transfer-buffer is recieved/transmitted)
        /// </summary>
        public event CloudReportProgressEHandler ReportProgress;

        public CloudClient(string overuser = "", string overpassword = "", string workingPath = "")
        {
            //System.Net.ServicePointManager.Expect100Continue = false;
            if (overuser != "") _username = overuser;
            if (overpassword != "") _password = overpassword;
            _authInfo = new NetworkCredential(_username, _password);
            WorkingPath = workingPath;
        }


        #region _CloudUtils
        private void _PathValidityCheck(string path)
        {
            //if (path.Where(ch => Path.GetInvalidPathChars().Contains(ch)).Count() != 0)
            var invalidChars = Path.GetInvalidPathChars();
            if ((from ch in path where invalidChars.Contains(ch) select ch).Count() != 0)
                throw new ArgumentException("Invalid characters in path!");
        }

        private HttpWebRequest FormGeneralRequest(string method, string addPath)
        {
            _PathValidityCheck(addPath);
            if (string.IsNullOrWhiteSpace(method))
                throw new ArgumentNullException("Method string cannot be null");
            var request = WebRequest.CreateHttp(@"https://webdav.yandex.ru:443" + WorkingPath + addPath);
            request.Method = method;
            request.Credentials = _authInfo;
            request.Accept = "*/*";
            request.Headers["Depth"] = "1";
            return request;
        }
        private void HandleException(WebException e, string cloudPath)
        {
            switch ((e.Response as HttpWebResponse).StatusCode)
            {
                case HttpStatusCode.NotFound:
                    throw new CloudNotFoundException(cloudPath, e);
                default:
                    throw e;
            }
        }


        private void _Transfer(Stream input, Stream output, long totalSize)
        {
            byte[] buffer = new byte[TRANSFER_BUFFER];
            int bytesRead = 0;
            long bytesDone = 0;
            do
            {
                bytesRead = input.Read(buffer, 0, TRANSFER_BUFFER);
                output.Write(buffer, 0, bytesRead);
                bytesDone += bytesRead;
                if (ReportProgress != null)
                    ReportProgress(bytesDone, totalSize, (int)(bytesDone * 100L / totalSize));
            } while (bytesDone < totalSize);
        }
        #endregion

        #region List
        private List<CloudEntityInfo> _ParseResponse(Stream resp)
        {
            List<CloudEntityInfo> info = new List<CloudEntityInfo>();

            CloudEntityInfo current = null;

            using (var xml = XmlReader.Create(resp))
            {
                while (!xml.EOF)
                {
                    if (!xml.IsStartElement()) { xml.Read(); continue; }
                    switch (xml.LocalName)
                    {
                        case "response":
                            if (current != null)
                                info.Add(current);
                            current = new CloudEntityInfo();
                            xml.Read();
                            break;
                        case "displayname":
                            current.Name = xml.ReadElementContentAsString();
                            break;
                        case "creationdate":
                            current.CreationTime = DateTime.Parse(xml.ReadElementContentAsString());
                            break;
                        case "getlastmodified":
                            current.LastModifiedTime = DateTime.Parse(xml.ReadElementContentAsString());
                            break;
                        case "getcontentlength":
                            current.Size = int.Parse(xml.ReadElementContentAsString());
                            break;
                        case "getetag":
                            current.MD5 = xml.ReadElementContentAsString();
                            break;
                        case "resourcetype":
                            xml.Read();
                            current.Type = xml.LocalName == "collection" ?
                            CloudEntityType.Directory : CloudEntityType.File;
                            break;
                        case "href":
                            current.FullPath = xml.ReadElementContentAsString();
                            break;
                        default:
                            xml.Read();
                            break;
                    }
                };
                if (current != null)
                    info.Add(current);
            }
            return info;
        }
        /// <summary>
        /// Retrieves all available info about the contents of the catalog in the cloud,
        /// including the specified "root" catalog.
        /// </summary>
        /// <param name="whereToList">The cloud-catalog path to list</param>
        /// <returns>All info retrieved info. The first element represent root catalog itself.</returns>
        public CloudEntityInfo[] GetInfo(string whereToList)
        {
            HttpWebRequest request = FormGeneralRequest("PROPFIND", whereToList);
            HttpWebResponse resp = null;
            try
            {
                List<CloudEntityInfo> info;
                using (resp = request.GetResponse() as HttpWebResponse)
                    info = _ParseResponse(resp.GetResponseStream());
                info.RemoveAt(0);
                return info.ToArray();
            }
            catch (WebException e) when (e.Response != null)
            {
                HandleException(e, whereToList);
                return null;
            }
        }
        public async Task<CloudEntityInfo[]> GetInfoAsync(string whereToList)
            => await Task<CloudEntityInfo[]>.Factory.StartNew(() => GetInfo(whereToList));

        #endregion

        #region Download
        private MemoryStream _DownloadFile(string cloudFile, string destination, ref string md5)
        {
            HttpWebRequest req = FormGeneralRequest("GET", cloudFile);
            //req.AllowReadStreamBuffering = false;
            try
            {
                MemoryStream memoryStream = null;
                using (var response = req.GetResponse() as HttpWebResponse)
                {
                    if (md5 != response.Headers["ETag"])
                    {
                        md5 = response.Headers["ETag"];
                        using (var ifs = response.GetResponseStream())
                        {
                            long size = response.ContentLength;
                            // Если нужно грузить в МемориСтрим (т е в память)
                            if (destination == null)
                            {
                                memoryStream = new MemoryStream((int)size);
                                _Transfer(ifs, memoryStream, size);
                                memoryStream.Position = 0;
                            }
                            else // Открываем локальный файл, если вылетит "птичка", то это проблемы вызывающего
                                using (var local = File.Create(destination))
                                    _Transfer(ifs, local, size);
                        }
                    }
                }
                return memoryStream;
            }
            catch (WebException e) when (e.Response != null)
            {
                HandleException(e, cloudFile);
                return null;
            }
        }

        public void Download(string file, string destination, ref string md5)
        => _DownloadFile(file, destination, ref md5);
        public void Download(string file, string destination)
        {
            string md5 = "";
            _DownloadFile(file, destination, ref md5);
        }

        public MemoryStream Download(string file, ref string md5)
        { return _DownloadFile(file, null, ref md5); }
        public MemoryStream Download(string file)
        {
            string md5 = "";
            return _DownloadFile(file, null, ref md5);
        }

        public async Task DownloadAsync(string file, string destination)
            => await Task.Factory.StartNew(() => Download(file, destination));
        public async Task<MemoryStream> DownloadAsync(string file)
            => await Task<MemoryStream>.Factory.StartNew(() => Download(file));

        #endregion

        #region Upload

        public void Upload(Stream dataStream, string cloudDestination, bool closeOnFinish = false)
        {
            if (dataStream == null || !dataStream.CanRead)
                throw new ArgumentException("Source Stream is invalid!", "dataStram");
            _PathValidityCheck(cloudDestination);
            HttpWebRequest request = FormGeneralRequest("PUT", cloudDestination);
            request.ContentType = "application/binary";
            request.ContentLength = dataStream.Length;
            //request.AllowWriteStreamBuffering = false;
            request.SendChunked = true;
            try
            {
                using (var netStream = request.GetRequestStream())
                {
                    int size = (int)dataStream.Length;
                    _Transfer(dataStream, netStream, size);
                }
                if (closeOnFinish)
                    dataStream.Dispose();
                using (var response = request.GetResponse())
                { };
            }
            catch (WebException e) when (e.Response != null)
            {
                HandleException(e, cloudDestination);
            }
        }

        public void Upload(string localFile, string cloudDestination)
        => Upload(File.OpenRead(localFile), cloudDestination, true);

        public async Task UploadAsync(string localFile, string cloudDestination)
            => await Task.Factory.StartNew(() => Upload(localFile, cloudDestination));
        public async Task UploadAsync(Stream dataStream, string cloudDestination, bool closeOnFinish = false)
            => await Task.Factory.StartNew(() => Upload(dataStream, cloudDestination, closeOnFinish));

        #endregion

        #region CatalogOps
        /// <summary>
        /// Creates catalog in cloud.
        /// </summary>
        /// <param name="catalogPath">Catalog to create.</param>
        /// <returns>True, if specified catalog already existed, otherwise false.</returns>
        public bool CreateCatalog(string catalogPath)
        {
            HttpWebRequest reqest = FormGeneralRequest("MKCOL", catalogPath);
            try
            {
                using (var resp = reqest.GetResponse() as HttpWebResponse)
                { }
            }
            catch (WebException e) when (e.Response != null)
            {
                var sc = (e.Response as HttpWebResponse).StatusCode;
                if (sc == HttpStatusCode.MethodNotAllowed)
                    return true;
                else if (sc == HttpStatusCode.Conflict)
                    throw new CloudDestPathInvalidException(catalogPath, e);
                else
                    throw;
            }
            return false;
        }

        public async Task<bool> CreateCatalogAsync(string catalogPath)
            => await Task<bool>.Factory.StartNew(() => CreateCatalog(catalogPath));

        #endregion

        #region Delete
        /// <summary>
        /// Permanently deletes cloud entity. If directory specified, it'll be deleted with all its contents.
        /// </summary>
        /// <param name="path">File or directory to delete.</param>
        /// <returns>True, if file/directory was actually deleted; false, if it didn't exist.</returns>
        public bool Delete(string path)
        {
            HttpWebRequest reqest = FormGeneralRequest("DELETE", path);
            try
            {
                using (var resp = reqest.GetResponse() as HttpWebResponse)
                { };
                return true;
            }
            catch (WebException e) when (e.Response != null)
            {
                var sc = (e.Response as HttpWebResponse).StatusCode;
                if (sc == HttpStatusCode.NotFound)
                    return false;
                else throw;
            }
        }

        public async Task<bool> DeleteAsync(string path)
            => await Task<bool>.Factory.StartNew(() => Delete(path));

        #endregion

        #region CopyMove
        private bool _CopyMove(string operation, string cloudSource, string cloudDestination, bool overWrite)
        {
            _PathValidityCheck(cloudDestination);
            HttpWebRequest req = FormGeneralRequest(operation, cloudSource);
            req.Headers["Destination"] = WorkingPath + cloudDestination;
            req.Headers["Overwrite"] = overWrite ? "T" : "F";
            try
            {
                var resp = req.GetResponse() as HttpWebResponse;
            }
            catch (WebException e) when (e.Response != null)
            {
                var sc = (e.Response as HttpWebResponse).StatusCode;
                if (sc == HttpStatusCode.NotFound)
                    throw new CloudNotFoundException(cloudSource, e);
                else if (sc == HttpStatusCode.PreconditionFailed)
                    throw new CloudDestPathInvalidException(cloudDestination, e);
                else throw;
            }
            return true;
        }
        public bool Copy(string cloudSource, string cloudDestination, bool overWrite = true)
            => _CopyMove("COPY", cloudSource, cloudDestination, overWrite);
        public bool Move(string cloudSource, string cloudDestination, bool overWrite = true)
            => _CopyMove("MOVE", cloudSource, cloudDestination, overWrite);



        public async Task<bool> CopyAsync(string cloudSource, string cloudDestination, bool overWrite = true)
            => await Task<bool>.Factory.StartNew(() => Copy(cloudSource, cloudDestination, overWrite));
        public async Task<bool> MoveAsync(string cloudSource, string cloudDestination, bool overWrite = true)
            => await Task<bool>.Factory.StartNew(() => Move(cloudSource, cloudDestination, overWrite));
        #endregion

        #region UtilExists
        /// <summary>
        /// Checks if the specified file/directory exists in the cloud.
        /// </summary>
        /// <param name="cloudEntity">File or directory name.</param>
        /// <returns></returns>
        public bool CheckExists(string cloudEntity)
        {
            _PathValidityCheck(cloudEntity);
            try
            {
                string path = Path.GetDirectoryName(cloudEntity);
                path = path.Replace('\\', '/');
                var info = GetInfo(path);
                var name = Path.GetFileName(cloudEntity);
                return info.Select(i => i.Name).Contains(name);
            }
            catch (CloudNotFoundException)
            {
                return false;
            }
        }

        public async Task<bool> CheckExistsAsync(string cloudEntity)
            => await Task<bool>.Factory.StartNew(() => CheckExists(cloudEntity));
        #endregion

        #region UploadStructsUtils
        private BinaryFormatter _bf = new BinaryFormatter();
        public S DownloadSerializedInfo<S> (string cloudPath) where S : class
        {
            MemoryStream dataStr = null;
            dataStr = Download(cloudPath);
            return _bf.Deserialize(dataStr) as S;
        }

        public async Task<S> DownloadSerializedInfoAsync<S>(string cloudPath) where S : class
        {
            MemoryStream dataStr = null;
            dataStr = await DownloadAsync(cloudPath);
            return _bf.Deserialize(dataStr) as S;
        }

        public void UploadSerializedInfo<S>(S @strcut, string cloudPath) where S : class
        {
            MemoryStream dataStr = new MemoryStream();
            _bf.Serialize(dataStr, @strcut);
            dataStr.Position = 0;
            Upload(dataStr, cloudPath, true);
        }
        public async Task UploadSerializedInfoAsync<S>(S @strcut, string cloudPath) where S : class
        {
            MemoryStream dataStr = new MemoryStream();
            _bf.Serialize(dataStr, @strcut);
            dataStr.Position = 0;
            await UploadAsync(dataStr, cloudPath, true);
        }
        #endregion
    }
}
