﻿using System;
using System.Collections.Generic;

namespace ManagerCore
{
    #region GlobalJMObject
    public static class JM
    {
        public static JeffsetManagerCore Core { get; set; }
        public static void CreateManager()
        {
            if (Core == null)
                Core = new JeffsetManagerCore();
            else
                throw new InvalidOperationException("Manager has already been created.");
        }

    }
    #endregion

    #region Exceptions

    [Serializable]
    public class JMNotAuthorizedException : Exception
    {
        public JMNotAuthorizedException(string operName)
            : base(string.Format("Can't perform \"{0}\" without been logged in!", operName))
        { }
    }

    [Serializable]
    public class JMSoftwareCorruptedException : Exception
    {
        public JMSoftwareCorruptedException(string software, Exception inner)
            : base(string.Format("Software entry \"{0}\" was corrupted! It'l be ignored.", software), inner)
        { }
    }

    #endregion

    #region OperationUtil
    public enum JMOpRepState { Completed, Started };

    public delegate void ManagerStateChangedEventHandler(JeffsetManagerState oldState, JeffsetManagerState newState);
    public delegate void ManagerOperationEventHandler(JMOpRepState state, string description);

    internal static class OperName
    {
        internal static readonly string MANAGER_UPLOAD = "Выгрузка релизной JM в облако...";
        internal static readonly string CHANGE_ACCOUNT_INFO = "Обновление информации аккаунта...";
        internal static readonly string LOGIN = "Вход в систему...";
        internal static readonly string CREATE_ACCOUNT = "Создание аккаунта...";
        internal static readonly string WIPE_ACCOUNT = "Удаление аккаунта из системы...";
        internal static readonly string REFRESH_SOFTWARE_LIST = "Обновление списка публикаций...";
        internal static readonly string WIPE_PRODUCT = "Удаление продукта из списка публикаций...";
        internal static readonly string DOWNLOAD_PRODUCT = "Загрузка и установка продукта...";
        internal static readonly string INSTALL_PRODUCT = "Установка продукта...";
        internal static readonly string UNINSTALL_PRODUCT = "Деинсталляция продукта из локальной машины...";
        internal static readonly string PUBLISH_SOFTW_VERSION = "Выгрузка версии продукта в облако (публикация)...";
        internal static readonly string SEND_REQUEST = "Отправка запроса...";
        internal static readonly string GET_REQUESTS = "Получение списка запросов...";
        internal static readonly string ANSWER_REQUEST = "Ответ на запрос...";
        internal static readonly string REMOVE_REQUEST = "Удаление запроса...";
        internal static readonly string QUERY_ALL_ACCOUNTS = "Загрузка списка аккаунтов...";
    }

    #endregion

    #region SoftwareRefreshEvent
    public class SoftwareRefreshedEventArgs : EventArgs
    {
        public IEnumerable<JeffsetInstalledSoftwareInfo> ObsoletedSoftware { get; private set; }
        public IEnumerable<JeffsetSoftware> HasCriticalUpdates { get; private set; }
        public IEnumerable<string> DamagedEntries { get; private set; }
        public SoftwareRefreshedEventArgs(IEnumerable<JeffsetInstalledSoftwareInfo> obs,
            IEnumerable<JeffsetSoftware> crit, IEnumerable<string> dam)
        {
            ObsoletedSoftware = obs;
            HasCriticalUpdates = crit;
            DamagedEntries = dam;
        }
    }
    public delegate void ManagerSoftwareRefreshed(object sender, SoftwareRefreshedEventArgs e);
    #endregion
}