﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace ManagerCore
{
    public partial class JeffsetManagerCore
    {

        #region ProductUpdate
        private void _RegisterInstalledProducts()
        {
            List<JeffsetInstalledSoftwareInfo> info = new List<JeffsetInstalledSoftwareInfo>();

            if (!Directory.Exists(CurrentUser.Credentials.UserName))
                Directory.CreateDirectory(CurrentUser.Credentials.UserName);
            using (var ms = File.Create(CurrentUser.Credentials.UserName + "\\data.jeff"))
            {
                foreach (var pr in AllSoftware)
                    if (pr.Installation != null)
                        info.Add(pr.Installation);
                new BinaryFormatter().Serialize(ms, info);
            }
        }
        public async Task UpdateInstalledProductAsync(JeffsetSoftware product)
        {
            BEGIN_OPERATION(OperName.DOWNLOAD_PRODUCT);
            try
            {
                if (product == null)
                    throw new ArgumentNullException("product");
                string package = SoftwareCatalogPath + product.ProductInternalName + "/package.exe";
                string destination = product.Installation.InstallationCatalog + "\\package.exe";
                await Cloud.DownloadAsync(package, destination);
            }
            finally
            {
                END_OPERATION();
            }
            BEGIN_OPERATION(OperName.INSTALL_PRODUCT);
            try
            {
                string tmp = Environment.CurrentDirectory;
                Environment.CurrentDirectory = product.Installation.InstallationCatalog;
                Process installer = Process.Start("package.exe");
                installer.WaitForExit();
                File.Delete("package.exe");
                Environment.CurrentDirectory = tmp;
                product.Installation = new JeffsetInstalledSoftwareInfo(product.ProductInternalName,
                    product.Installation.InstallationCatalog, product.CurrentVersion);
                _RegisterInstalledProducts();
            }
            finally
            {
                END_OPERATION();
            }
        }
        #endregion

        #region ProductInstall
        public async Task InstallNewProductAsync(JeffsetSoftware product, string installationPath)
        {
            if (product == null)
                throw new ArgumentNullException("pruduct");
            if (string.IsNullOrEmpty(installationPath))
                throw new ArgumentNullException("installationPath");
            product.Installation =
                new JeffsetInstalledSoftwareInfo(product.ProductInternalName, installationPath, product.CurrentVersion);
            await UpdateInstalledProductAsync(product);
        }
        #endregion

        #region ProductUninstall
        public void UninstallProduct(JeffsetSoftware soft)
        {
            BEGIN_OPERATION(OperName.UNINSTALL_PRODUCT);
            try
            {
                DirectoryInfo ic = new DirectoryInfo(soft.Installation.InstallationCatalog);
                var files = ic.GetFiles();
                var dirs = ic.GetDirectories();
                foreach (var file in files)
                    File.Delete(file.FullName);
                foreach (var dir in dirs)
                    Directory.Delete(dir.FullName, true);
                soft.Installation = null;
                _RegisterInstalledProducts();
            }
            catch(DirectoryNotFoundException) {
                soft.Installation = null;
                _RegisterInstalledProducts();
                throw;
            }
            finally
            {
                END_OPERATION();
            }
        }
        #endregion

        #region ProductUpload
        public async Task PublishSoftwareVersionAsync(JeffsetSoftware soft, string packageLocalPath)
        {
            BEGIN_OPERATION(OperName.PUBLISH_SOFTW_VERSION);
            try
            {
                string pathCat = SoftwareCatalogPath + soft.ProductInternalName;
                string path = pathCat + "/software.jeff";
                string pathIcon = pathCat + "/icon.bin";
                string pathBanner = pathCat + "/banner.bin";
                string packagePath = pathCat + "/package.exe";
                Cloud.CreateCatalog(pathCat);
                await Cloud.UploadSerializedInfoAsync(soft, path);
                if (soft.AdditInfo.IconBitmap.ImageStream != null)
                {
                    soft.AdditInfo.IconBitmap.ImageStream.Position = 0;
                    await Cloud.UploadAsync(soft.AdditInfo.IconBitmap.ImageStream, pathIcon, true);
                }
                if (soft.AdditInfo.BannerBitmap.ImageStream != null)
                {
                    soft.AdditInfo.BannerBitmap.ImageStream.Position = 0;
                    await Cloud.UploadAsync(soft.AdditInfo.BannerBitmap.ImageStream, pathBanner, true);
                }
                if (!string.IsNullOrEmpty(packageLocalPath))
                    await Cloud.UploadAsync(packageLocalPath, packagePath);
            }
            finally
            {
                END_OPERATION();
            }
        }

        #endregion
    }
}
