﻿using CloudOperations;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace ManagerCore {
    public enum JDevRequestType {
        LicenseVerify,
        UserReport,
    }
    public enum JDevRequestState {
        Waiting,
        Accepted,
        Declined
    }
    [Serializable]
    public sealed class JDRequest {
        public int ReqID { get; private set; }
        public JeffsetUserAccount Who { get; private set; }
        public JDevRequestState State { get; set; } = JDevRequestState.Waiting;
        public JDevRequestType What { get; private set; }
        public DateTime When { get; private set; }
        public Dictionary<string, object> Data { get; private set; } = new Dictionary<string, object>();

        public JDRequest() {
            ReqID = new Random(Environment.TickCount).Next();
        }

        #region StaticCreations

        public static JDRequest CreateAccountVerification
            (JeffsetUserAccount acc, JeffsetLicence license) {
            var req = new JDRequest();
            req.What = JDevRequestType.LicenseVerify;
            req.Who = acc;
            req.When = DateTime.Now;
            req.Data["LICENSE"] = license;
            req.Data["ACTION"] = "регистрация";
            return req;
        }
        public static JDRequest CreateAccountChangeLicense
            (JeffsetLicence license) {
            var req = CreateAccountVerification(JM.Core.CurrentUser, license);
            req.Data["ACTION"] = "повышение";
            return req;
        }


        public static JDRequest CreateUserReport
            (JeffsetUserAccount acc, string text, string title = null, string softwareName = null, float? rating = null) {
            var req = new JDRequest();
            req.What = JDevRequestType.UserReport;
            req.Who = acc;
            req.When = DateTime.Now;
            req.Data["TEXT"] = text;
            req.Data["TITLE"] = title ?? "$";
            req.Data["SOFTWARE"] = softwareName ?? "$";
            req.Data["RATING"] = rating ?? -1.0f;
            return req;
        }
        #endregion


        #region Equals
        public override int GetHashCode() {
            return ReqID ^ Who.Credentials.UserName.GetHashCode() ^ When.GetHashCode();
        }
        // override object.Equals
        public override bool Equals(object obj) {

            if(obj == null || GetType() != obj.GetType()) {
                return false;
            }
            return this.GetHashCode() == obj.GetHashCode();
        }
        #endregion
        public override string ToString() {
            return string.Format("[{0}]{1} - {1}", When, Who, What);
        }
    }

    public partial class JeffsetManagerCore {
        const string _RequestPath = "@Requests/requests.jeff";

        public void RefreshAllSoftwareListAsync(object p) {
            throw new NotImplementedException();
        }

        public async Task SendRequestAsync(JDRequest r) {
            BEGIN_OPERATION(OperName.SEND_REQUEST);
            try {
                List<JDRequest> reqQueue;
                try { reqQueue = await Cloud.DownloadSerializedInfoAsync<List<JDRequest>>(_RequestPath); } catch(CloudNotFoundException) { reqQueue = new List<JDRequest>(); }
                reqQueue.Add(r);
                await Cloud.UploadSerializedInfoAsync(reqQueue, _RequestPath);
            } finally {
                END_OPERATION();
            }
        }
        public async Task<IEnumerable<JDRequest>> GetDevRequestsAsync() {
            BEGIN_OPERATION(OperName.GET_REQUESTS);
            try {
                var allreqests = await Cloud.DownloadSerializedInfoAsync<List<JDRequest>>(_RequestPath);
                allreqests.Sort((r1, r2) => r1.When < r2.When ? +1 : r1.When > r2.When ? -1 : 0);
                return allreqests;
            } catch(CloudNotFoundException) {
                return new List<JDRequest>();
            } finally { END_OPERATION(); }
        }
        public async Task RequestHasBeenHandledAsync(JDRequest r, JDevRequestState newState, JDRequest answer = null) {
            BEGIN_OPERATION(OperName.ANSWER_REQUEST);
            try {
                List<JDRequest> reqs = null;
                try {
                    reqs = await Cloud.DownloadSerializedInfoAsync<List<JDRequest>>(_RequestPath);
                } catch(CloudNotFoundException) {
                    reqs = new List<JDRequest>();
                }
                var corrReq = reqs.Find((t) => t.Equals(r));
                if(corrReq == null)
                    return;
                reqs.Remove(corrReq);
                corrReq.State = newState;
                corrReq.Data["ANSWER"] = answer;
                reqs.Add(corrReq);
                await Cloud.UploadSerializedInfoAsync(reqs, _RequestPath);
            } finally {
                END_OPERATION();
            }
        }
        public async Task RemoveRequestAsync(JDRequest r) {
            BEGIN_OPERATION(OperName.REMOVE_REQUEST);
            try {
                var requests = await Cloud.DownloadSerializedInfoAsync<List<JDRequest>>(_RequestPath);
                var req = requests.Find(t => t.Equals(r));
                if(req == null)
                    throw new InvalidOperationException("No such request!");
                requests.Remove(req);
                await Cloud.UploadSerializedInfoAsync(requests, _RequestPath);
            } catch(CloudNotFoundException) {
                throw new InvalidOperationException("No such request!");
            } finally { END_OPERATION(); }
        }
        public async Task<IEnumerable<JDRequest>> GetCurrentUserRequestAsync(JDevRequestType? filter = null) {
            BEGIN_OPERATION(OperName.GET_REQUESTS);
            try {
                var allRequests = await Cloud.DownloadSerializedInfoAsync<List<JDRequest>>(_RequestPath);
                var retval = from req in allRequests
                             where req.Who.Equals(CurrentUser) && (filter == null ? true : req.What == filter)
                             orderby req.When descending
                             select req;
                return retval;
            } catch(CloudNotFoundException) {
                return new List<JDRequest>();
            } finally { END_OPERATION(); }
        }
    }
}
