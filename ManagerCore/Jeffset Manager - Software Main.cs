﻿using CloudOperations;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace ManagerCore
{

    [Serializable]
    public class JeffsetInstalledSoftwareInfo
    {
        public string ProductInternalName { get; private set; }
        public string InstallationCatalog { get; private set; }
        public Version InstalledVersion { get; private set; }

        public JeffsetInstalledSoftwareInfo(string intName, string path, Version ver)
        {
            ProductInternalName = intName;
            InstalledVersion = ver; InstallationCatalog = path;
        }
    }

    public class JeffsetSoftwareAdditionInfo
    {
        public JMCloudImage IconBitmap { get; set; }

        public JMCloudImage BannerBitmap { get; set; }

        public int DownloadPackageSize { get; private set; }

        public JeffsetSoftwareAdditionInfo(int size, JMCloudImage icon = null, JMCloudImage banner = null)
        {
            DownloadPackageSize = size;
            IconBitmap = icon;
            BannerBitmap = banner;
        }
    }

    [Serializable]
    public class JeffsetSoftware
    {
        public JeffsetLicence License { get; private set; }
        public string ProductFriendlyName { get; private set; }
        public string ProductInternalName { get; private set; }

        public Version CurrentVersion { get; private set; }
        public Version MinYetSupportedVersion { get; private set; }

        public string PublicDescription { get; set; }
        public string Documentation { get; set; }

        [NonSerialized]
        private JeffsetSoftwareAdditionInfo _addInfo;
        public JeffsetSoftwareAdditionInfo AdditInfo
        {
            get { return _addInfo; }
            set { _addInfo = value; }
        }

        [NonSerialized]
        private JeffsetInstalledSoftwareInfo _instInfo;
        public JeffsetInstalledSoftwareInfo Installation
        {
            get { return _instInfo; }
            set { _instInfo = value; }
        }

        public bool CheckUpdates()
        { return Installation != null && (Installation.InstalledVersion < CurrentVersion); }
        public bool CheckCriticalUpdates()
        { return CheckUpdates() && (Installation.InstalledVersion < MinYetSupportedVersion); }

        public JeffsetSoftware(string intName, string frName, Version verCur, Version minVer,
            string descr,
            JeffsetLicence licence, JeffsetSoftwareAdditionInfo adinf)
        {
            ProductInternalName = intName;
            ProductFriendlyName = frName;
            CurrentVersion = verCur;
            MinYetSupportedVersion = minVer;
            AdditInfo = adinf;
            License = licence;
            PublicDescription = descr;
        }
    }

    public partial class JeffsetManagerCore
    {
        private const string SoftwareCatalogPath = "@Products/";
        public List<JeffsetSoftware> AllSoftware { get; private set; } = new List<JeffsetSoftware>();

        #region RefreshAll
        public event ManagerSoftwareRefreshed SoftwareRefreshed;
        private IEnumerable<JeffsetInstalledSoftwareInfo> _AsquireLocalInstalledSoftware()
        {
            try
            {
                List<JeffsetInstalledSoftwareInfo> info = new List<JeffsetInstalledSoftwareInfo>();
                using (var ms = File.OpenRead(CurrentUser.Credentials.UserName + "\\data.jeff"))
                {
                    BinaryFormatter bf = new BinaryFormatter();
                    info = bf.Deserialize(ms) as List<JeffsetInstalledSoftwareInfo>;
                    foreach (var product in AllSoftware)
                        product.Installation = info.Find(p => p.ProductInternalName == product.ProductInternalName);
                }
                return from i in info
                       where (from soft in AllSoftware select soft.ProductInternalName).Contains(i.ProductInternalName)
                       select i;
            }
            catch (FileNotFoundException) { return new List<JeffsetInstalledSoftwareInfo>(); }
            catch (DirectoryNotFoundException) { return new List<JeffsetInstalledSoftwareInfo>(); }
        }
        private async Task _RepairDamagedSoftwareEntryAsync(string productPath)
        {
            await Cloud.DeleteAsync(productPath);
        }
        public async Task RefreshAllSoftwareListAsync()
        {
            BEGIN_OPERATION(OperName.REFRESH_SOFTWARE_LIST);
            try
            {
                // Проверки
                if (!IsLoggedIn) throw new JMNotAuthorizedException("RefreshSoftware");
                // Получаем список всех продуктов
                CloudEntityInfo[] entryList = await Cloud.GetInfoAsync(SoftwareCatalogPath);
                AllSoftware.Clear();
                // Список для поврежденных записей продуктов
                List<string> damaged = new List<string>();
                foreach (var entry in entryList)
                {
                    // Пропускаем текущий каталог
                    if (entry.Name == "@Products") continue;
                    // путь к конкретному файлу информации о продукте
                    string ProductPath = SoftwareCatalogPath + entry.Name;
                    JeffsetSoftware software;
                    JeffsetSoftwareAdditionInfo addInfo;
                    try
                    {
                        // скачиваем информацию
                        software = await Cloud.DownloadSerializedInfoAsync<JeffsetSoftware>(ProductPath + "/software.jeff");
                        // получаем фактическое содержимое каталога продукта
                        CloudEntityInfo[] contetns = await Cloud.GetInfoAsync(ProductPath);
                        // Получаем дополнительную информацию
                        addInfo = new JeffsetSoftwareAdditionInfo(contetns.First(f => f.Name == "package.exe").Size);
                    }
                    catch (CloudNotFoundException)
                    {
                        // нет файла информации "джефф"
                        Debug.WriteLine($"Possibly damaged software entry: {entry}");
                        await _RepairDamagedSoftwareEntryAsync(ProductPath);
                        damaged.Add(entry.Name);
                        continue;
                    }
                    catch (InvalidOperationException)
                    {
                        // нет пакета установщика
                        Debug.WriteLine($"Possibly damaged software entry: {entry}");
                        await _RepairDamagedSoftwareEntryAsync(ProductPath);
                        damaged.Add(entry.Name);
                        continue;
                    }
                    // Изображения
                    addInfo.IconBitmap = new JMCloudImage(ProductPath + "/icon.bin");
                    addInfo.BannerBitmap = new JMCloudImage(ProductPath + "/banner.bin");
                    // Конец работы с облачной частью
                    software.AdditInfo = addInfo;
                    AllSoftware.Add(software);
                }
                // То ПО, которое установлено у пользователя, но которого уже нет в облаке (устаревшее)
                var obsoleted = _AsquireLocalInstalledSoftware();
                // Событие и его аргументы: список программ с критическим обновлением, и список устаревших программ.
                if (SoftwareRefreshed != null)
                {
                    var criticalUpd = from soft in AllSoftware where soft.CheckCriticalUpdates() select soft;
                    SoftwareRefreshedEventArgs e = new SoftwareRefreshedEventArgs(obsoleted, criticalUpd, damaged);
                    SoftwareRefreshed(this, e);
                }
            }
            finally { END_OPERATION(); }
        }
        #endregion

        #region UserPermissions

        public IEnumerable<JeffsetSoftware> GetSoftwareForCurrentUser()
        => from soft in AllSoftware where soft.License.CanInstallWith(CurrentUser.License) select soft;

        #endregion

        #region DeletePublishedProduct
        public async Task DeletePublishedProductAsync(JeffsetSoftware soft)
        {
            BEGIN_OPERATION(OperName.WIPE_PRODUCT);
            try
            {
                if (soft == null)
                    throw new ArgumentNullException("soft");
                if (!AllSoftware.Contains(soft))
                    throw new ArgumentException("Such software is not present in list!");
                await _RepairDamagedSoftwareEntryAsync(SoftwareCatalogPath + soft.ProductInternalName);
                AllSoftware.Remove(soft);
            }
            finally { END_OPERATION(); }
        }
        #endregion
    }
}
