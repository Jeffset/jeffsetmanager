﻿using CloudOperations;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace ManagerCore {
    public enum JeffsetUserType {
        Unverified = 0, Free = 5, Standard = 10, Extended = 20, Developer = 740
    }

    [Serializable]
    public struct JeffsetLicence {
        public JeffsetUserType UserLicense { get; set; }
        public bool TesterLicense { get; set; }
        public bool DeveloperLicense { get { return UserLicense == JeffsetUserType.Developer; } }

        public override string ToString() {
            if(TesterLicense)
                return UserLicense.ToString() + " (тестер)";
            else
                return UserLicense.ToString();
        }

        public bool CanInstallWith(JeffsetLicence user) {
            if(user.DeveloperLicense) return true;
            if(user.TesterLicense && this.TesterLicense) return true;
            if(!user.TesterLicense && this.TesterLicense) return false;
            return user.UserLicense >= UserLicense;
        }

        #region CompareSystem
        public static bool operator ==(JeffsetLicence v1, JeffsetLicence v2) {
            return v1.UserLicense == v2.UserLicense && v1.TesterLicense == v2.TesterLicense;
        }
        public static bool operator !=(JeffsetLicence v1, JeffsetLicence v2) {
            return !(v1 == v2);
        }

        public override bool Equals(object obj) {
            if(obj is JeffsetLicence)
                return this == (JeffsetLicence)obj;
            else return false;
        }

        public override int GetHashCode() {
            return UserLicense.GetHashCode() ^ TesterLicense.GetHashCode();
        }
        #endregion
    }

    [Serializable]
    public class JeffsetUserCredentials {
        public const int MinPasswordCharCount = 5;
        public string UserName { get; private set; }
        public string Password { get; private set; }
        public JeffsetUserCredentials(string un, string ps) {
            UserName = un; Password = ps;
        }
        public bool CanAuthWith(JeffsetUserCredentials test)
        => UserName == test.UserName && Password == test.Password;
        public bool CanAuthWith(string un, string ps)
        => UserName == un && Password == ps;
        public bool CheckValidity(bool checkPsLength = false) {
            if(string.IsNullOrWhiteSpace(UserName)) return false;
            if(checkPsLength && Password.Length < MinPasswordCharCount) return false;
            if(!UserName.All(c => char.IsLetterOrDigit(c))) return false;
            return true;
        }
    }
    [Serializable]
    public class JeffsetUserAccount {
        public JeffsetUserCredentials Credentials { get; set; }
        public string Name { get; set; }
        public JeffsetLicence License { get; set; }
        public bool CheckValidity(bool newAccount = true) {
            if(string.IsNullOrEmpty(Name)) return false;
            if(!Credentials.CheckValidity(newAccount)) return false;
            return true;
        }

        #region Equals
        public override bool Equals(object obj) {
            if(!(obj is JeffsetUserAccount)) return false;
            if(obj == null) return false;
            if(base.Equals(obj)) return true;
            return Credentials.CanAuthWith((obj as JeffsetUserAccount).Credentials);
        }
        public override int GetHashCode() {
            return Name.GetHashCode() ^ Credentials.UserName.GetHashCode() ^ Credentials.Password.GetHashCode();
        }
        #endregion
    }
    public partial class JeffsetManagerCore {
        private const string UserAccountsCatalog = "@UserAccounts/";

        public JeffsetUserAccount CurrentUser { get; private set; }
        public object Avatar { get; set; }
        public bool IsLoggedIn { get { return CurrentUser != null; } }

        public bool IsInDeveloperMode { get { return CurrentUser.License.DeveloperLicense; } }

        #region LogIn

        public enum LogInResult { OK = 0, WrongName = 1, WrongPassword = 2 };

        public async Task<LogInResult> LogInAsync(string userName, string password) {
            BEGIN_OPERATION(OperName.LOGIN);
            try
            {
                var cred = new JeffsetUserCredentials(userName, password);
                if(!cred.CheckValidity(checkPsLength: false))
                    throw new ArgumentException("Account info incorrect.", "userName OR password");
                try
                {
                    string accountPath = $"{UserAccountsCatalog}{userName}/{userName}.acc";
                    var Account = await Cloud.DownloadSerializedInfoAsync<JeffsetUserAccount>(accountPath);
                    if(Account.Credentials.CanAuthWith(userName, password))
                    {
                        CurrentUser = Account;
                        _SetupCookie(Account.Credentials);
                        return LogInResult.OK;
                    }
                    else
                        return LogInResult.WrongPassword;
                }
                catch(CloudNotFoundException)
                {
                    return LogInResult.WrongName;
                }
            }
            finally { END_OPERATION(); };
        }
        public async Task<LogInResult> LogInAsync(JeffsetUserCredentials auth)
        => await LogInAsync(auth.UserName, auth.Password);

        public void LogOff() {
            if(File.Exists(CookiePath))
                File.Delete(CookiePath);
            CurrentUser = null;
        }
        #endregion

        #region Register
        public enum SignUpResult { OK = 0, UserNameConflict = 2, IncorrectData = 4 };

        private async Task _SetUpUserAccountAsync(JeffsetUserAccount account) {
            string accPath = $"{UserAccountsCatalog}{account.Credentials.UserName}";
            await Cloud.CreateCatalogAsync(accPath);
            await Cloud.UploadSerializedInfoAsync(account, accPath + $"/{account.Credentials.UserName}.acc");
        }

        public async Task<SignUpResult> RegisterAccountAsync(JeffsetUserAccount wishToBeRegistered, JeffsetLicence license, Stream avatar) {
            BEGIN_OPERATION(OperName.CREATE_ACCOUNT);
            try
            {
                if(wishToBeRegistered == null)
                    throw new ArgumentNullException("wishToBeRegistered");
                if(!wishToBeRegistered.CheckValidity(true))
                    throw new ArgumentException("Account info incorrect.", "wishToBeRegistered");
                if(avatar == null)
                    throw new ArgumentNullException("avatar");

                string uname = wishToBeRegistered.Credentials.UserName;
                string accPath = $"{UserAccountsCatalog}{uname}/{uname}.acc";
                if(await Cloud.CheckExistsAsync(accPath))
                    return SignUpResult.UserNameConflict;
                await _SetUpUserAccountAsync(wishToBeRegistered);
                string avatarPath = UserAccountsCatalog + wishToBeRegistered.Credentials.UserName + "/banner.gif";
                avatar.Position = 0;
                await Cloud.UploadAsync(avatar, avatarPath);
            }
            finally { END_OPERATION(); }
            await SendRequestAsync(JDRequest.CreateAccountVerification(wishToBeRegistered, license));
            return SignUpResult.OK;
        }
        #endregion

        #region Avatar
        public async Task<Stream> DownloadAvatarAsync(Action<Stream> onImageReady)
        => await DownloadAvatarAsync(CurrentUser, onImageReady);
        public async Task<Stream> DownloadAvatarAsync(JeffsetUserAccount acc, Action<Stream> onImageReady) {
            if(acc == null)
                throw new JMNotAuthorizedException("DownloadAvatar");
            string path = UserAccountsCatalog + acc.Credentials.UserName + "/banner.gif";
            return await LoadImage(path, onImageReady);
        }
        #endregion

        #region CookieLogin

        private const string CookiePath = ".\\config.bin";
        public enum CookieLogInResult { OK = 0, CredentialsChanged = 1, CookieMissing = 3 };


        private void _SetupCookie(JeffsetUserCredentials cr) {
            using(var s = File.OpenWrite(CookiePath))
                new BinaryFormatter().Serialize(s, cr);
        }
        public async Task<CookieLogInResult> CookieLoginAsync() {
            if(!File.Exists(CookiePath))
            {
                return CookieLogInResult.CookieMissing;
            }
            JeffsetUserCredentials cr;
            using(var s = File.OpenRead(CookiePath))
                cr = new BinaryFormatter().Deserialize(s) as JeffsetUserCredentials;
            return await LogInAsync(cr) == LogInResult.OK ? CookieLogInResult.OK : CookieLogInResult.CredentialsChanged;
        }

        #endregion

        #region ChangeAccountInfo

        public async Task ChangeAccountSettingsAsync()
        => await ChangeAccountSettingsAsync(CurrentUser);

        public async Task ChangeAccountSettingsAsync(JeffsetUserAccount acc) {
            BEGIN_OPERATION(OperName.CHANGE_ACCOUNT_INFO);
            try { await _SetUpUserAccountAsync(acc); } finally { END_OPERATION(); }
        }
        #endregion

        #region WipeAccount
        public async Task WipeAccountAsync(JeffsetUserAccount acc) {
            BEGIN_OPERATION(OperName.WIPE_ACCOUNT);
            try { await Cloud.DeleteAsync(UserAccountsCatalog + acc.Credentials.UserName); } catch(CloudNotFoundException) { } finally { END_OPERATION(); }
        }
        #endregion

        #region QueryAllAccounts
        public async Task<IEnumerable<JeffsetUserAccount>> QueryAllAccountsAsync(Predicate<JeffsetUserAccount> pred = null) {

            List<JeffsetUserAccount> result = new List<JeffsetUserAccount>();
            CloudEntityInfo[] accs;
            BEGIN_OPERATION(OperName.QUERY_ALL_ACCOUNTS);
            try
            {
                accs = await Cloud.GetInfoAsync(UserAccountsCatalog);
            }
            finally
            {
                END_OPERATION();
            }
            foreach(var acc in accs)
            {
                BEGIN_OPERATION(OperName.QUERY_ALL_ACCOUNTS + $" [{acc}]");
                try
                {
                    string accountPath = $"{UserAccountsCatalog}{acc}/{acc}.acc";
                    var Account = await Cloud.DownloadSerializedInfoAsync<JeffsetUserAccount>(accountPath);
                    result.Add(Account);
                }
                finally
                {
                    END_OPERATION();
                }
            }
            return result;
        }
        #endregion
    }
}
