﻿using CloudOperations;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace ManagerCore
{
    public class JMCloudImage
    {
        public string CloudPath { get; private set; } = "";
        public Stream ImageStream { get; private set; } = null;
        public void UpdateImage(Action<Stream> callback)
        {
            if (string.IsNullOrEmpty(CloudPath))
                throw new InvalidOperationException("LoadImage with empty path!");
#pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
            JM.Core.LoadImage(CloudPath, (s) =>
            {
                ImageStream = s;
                if (s == null) return;
                s.Position = 0; callback(s);
            });
#pragma warning restore CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
        }
        public JMCloudImage(string cloudPath)
        {
            CloudPath = cloudPath;
        }
        public JMCloudImage(Stream str)
        {
            ImageStream = str;
        }
    }
    public partial class JeffsetManagerCore
    {
        public string ImgLocalChache { get { return @".\Chache\"; } }
        public async Task<Stream> LoadImage(string cloudPath, Action<Stream> onImageReady)
        {
            if (!Directory.Exists(ImgLocalChache))
                Directory.CreateDirectory(ImgLocalChache);
            string chacheEntry = ImgLocalChache + "chache" + cloudPath.GetHashCode();
            bool presentInDiskChache = File.Exists(chacheEntry);
            Stream resultStream = await Task<Stream>.Factory.StartNew(() =>
              {
                  // Если присутствует в дисковом кеше
                  if (presentInDiskChache)
                  {
                      // Прикидываемся шлангом и возвращаем "пока что" старую версию }:)
                      // Пускай повисит, пока мы тут разберемся с хэшами и прочимии облаками
                      using (var old = File.OpenRead(chacheEntry))
                          onImageReady(old);
                      Debug.WriteLine("%JM ImageChache -> ChachedReturned: " + cloudPath);

                      // Считываем хэш-сумму MD5
                      try
                      {
                          string MD5 = File.ReadAllText(chacheEntry + ".md5");
                          // И проверяем актуальность запросом к облаку
                          Stream ms = Cloud.Download(cloudPath, ref MD5);
                          // Значит хэш-суммы неидентичны, и была загружена новая версия
                          // Нужно обновить вернуть новую версию и обновить кэш.
                          if (ms != null)
                          {
                              using (var chache = File.OpenWrite(chacheEntry))
                                  ms.CopyTo(chache);
                              ms.Position = 0;
                              File.WriteAllText(chacheEntry + ".md5", MD5);
                              Debug.WriteLine("%JM ImageChache -> Chache is old. New downloaded: " + cloudPath);
                              onImageReady(ms);
                              return ms;
                          }
                          else // Если кэш все еще актуален, то в обратном вызове мы уже вернули его, а тут нолика достаточно
                              return null;
                      }
                      catch (CloudNotFoundException)
                      {
                          File.Delete(chacheEntry);
                          File.Delete(chacheEntry + ".md5");
                          Debug.WriteLine("%JM ImageChache -> Chache is obsoleted and removed: " + cloudPath);
                          onImageReady(null);
                          return null;
                      }
                  }
                  else // Дисковый кеш пуст, однозначно грузим из облака:
                  {
                      try
                      {
                          MemoryStream ms = null;
                          string MD5 = "";
                          ms = Cloud.Download(cloudPath, ref MD5);
                          using (var chache = File.Create(chacheEntry))
                              ms.CopyTo(chache);
                          ms.Position = 0;
                          File.WriteAllText(chacheEntry + ".md5", MD5);
                          Debug.WriteLine("-IMG-> New downloaded: " + cloudPath);
                          onImageReady(ms);
                          return ms;
                      }
                      catch (CloudNotFoundException)
                      {
                          Debug.WriteLine("-IMG-> Just not found: " + cloudPath);
                          onImageReady(null);
                          return null;
                      }
                  }
              });
            return resultStream;
        }
    }
}
