﻿using CloudOperations;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ManagerCore {
    public enum JeffsetManagerState {
        Ready = 4,
        Busy = 8
    }

    public partial class JeffsetManagerCore {

        #region CurrentOperation
        //private bool _waitingFlag = false;
        public event ManagerOperationEventHandler OperationReport;

        private void BEGIN_OPERATION(string name) {
            Debug.Assert(State == JeffsetManagerState.Ready);
            State = JeffsetManagerState.Busy;
            if(OperationReport != null)
                OperationReport(JMOpRepState.Started, name);
        }
        private void END_OPERATION() {
            Debug.Assert(State == JeffsetManagerState.Busy);
            State = JeffsetManagerState.Ready;
            if(OperationReport != null)
                OperationReport(JMOpRepState.Completed, "");
        }

        public void WaitForAllOps() {
            //if (!IsBusy) return;
            //_waitingFlag = true;
            //_CurrentOperation.EndInvoke(_async);
            //if (OperationReport != null)
            //    OperationReport(JMOpRepState.Completed);
            //if (_operationQueue.Count > 0)
            //    PerformOperation(_operationQueue.Dequeue());
            //_waitingFlag = false;
        }
        #endregion

        private CloudClient Cloud =
#if !DEBUG
            new CloudClient(workingPath: "/SystemStorage/JMMGR/");
#else
            new CloudClient(workingPath: "/SystemStorage/JMMGR/");
#endif

        public event CloudReportProgressEHandler ReportProgress {
            add {
                Cloud.ReportProgress += value;
            }
            remove {
                Cloud.ReportProgress -= value;
            }
        }

        #region ManagerState
        public event ManagerStateChangedEventHandler StateChanged;
        private JeffsetManagerState _state = JeffsetManagerState.Ready;
        public JeffsetManagerState State {
            get { return _state; }
            set {
                var old = _state;
                _state = value;
                var temp = StateChanged;
                if(temp != null) {
                    temp(old, value);
                }
            }
        }
        public bool IsBusy { get { return State == JeffsetManagerState.Busy; } }
        #endregion

        internal JeffsetManagerCore() {
        }

        #region SelfUpdate

        private const string _managerUpdPack = "@Manager/package.exe";
        private const string _managerInfo = "@Manager/manager.jeff";

        [Serializable]
        private class _JManagerInfo {
            public Version CloudVersion { get; private set; }

            public static Version DeployedVersion {
                get {
                    var attr = Assembly.GetEntryAssembly().GetCustomAttributes(true);
                    foreach(var att in attr) {
                        if(att is AssemblyFileVersionAttribute) {
                            var ver = att as AssemblyFileVersionAttribute;
                            return new Version(ver.Version);
                        }
                    }
                    return new Version();
                }
            }

            public _JManagerInfo() {
                CloudVersion = DeployedVersion;
            }
        }

        public Version ManagerVersion { get { return _JManagerInfo.DeployedVersion; } }
        public bool IsNeedUpdate() {
            try {
                var infoTask = Cloud.DownloadSerializedInfoAsync<_JManagerInfo>(_managerInfo);
                infoTask.Wait();
                var info = infoTask.Result;
                if(_JManagerInfo.DeployedVersion < info.CloudVersion)
                    return true;
                else
                    return false;
            } catch(CloudNotFoundException) {
                return false;
            }
        }

        public void SelfUpdate() {
            const string pack = ".\\package.exe";
            const string manager = ".\\JeffsetManager.exe";
            const string backupPath = ".\\Backup";

            try {
                Cloud.Download(_managerUpdPack, pack);
                if(Directory.Exists(backupPath))
                    Directory.Delete(backupPath, true);
                Directory.CreateDirectory(backupPath);
                File.Move(".\\JeffsetManager.exe", backupPath + "\\JeffsetManager.exe");
                File.Move(".\\ManagerCore.dll", backupPath + "\\ManagerCore.dll");
                File.Move(".\\Rar.exe", backupPath + "\\Rar.exe");
                File.Move(".\\WinCon.SFX", backupPath + "\\WinCon.SFX");

                var update = Process.Start(pack);
                update.WaitForExit();

                File.Delete(pack);
                Process.Start(manager, "-updated");
            } catch(Exception e) {
                Debug.Write(e);
                try {
                    File.Move(backupPath + "\\JeffsetManager.exe", ".\\JeffsetManager.exe");
                    File.Move(backupPath + "\\ManagerCore.dll", ".\\ManagerCore.dll");
                    File.Move(backupPath + "\\Rar.exe", ".\\Rar.exe");
                    File.Move(backupPath + "\\WinCon.SFX", ".\\WinCon.SFX");
                } catch(IOException) { }
                Process.Start(manager, "-updatefailed");
            }
        }
        public async Task PublishThisManagerVersionAsync() {
            BEGIN_OPERATION(OperName.MANAGER_UPLOAD);
            try {
                //string archiever = @"C:\Program Files\WinRar\Rar.exe";
                string archiever = @".\Rar.exe";
                string args =
                @"a -m5 -ma5 -md256m -sfxWinCon.SFX package.rar JeffsetManager.exe ManagerCore.dll Rar.exe WinCon.SFX";
                var Archiever = Process.Start(archiever, args);
                Archiever.WaitForExit();
                if(Archiever.ExitCode != 0)
                    throw new InvalidOperationException(Archiever.ExitCode.ToString());
                await Cloud.UploadAsync("package.exe", _managerUpdPack);
                File.Delete("package.exe");
                await Cloud.UploadSerializedInfoAsync(new _JManagerInfo(), _managerInfo);
            } finally {
                END_OPERATION();
            }
        }

        #endregion
    }
}
